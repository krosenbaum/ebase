README for Electric Base
==========================

This is a Parts Database GUI for KiCAD. It uses MariaDB as backend.

(c) Konrad Rosenbaum <konrad@silmor.de>, 2024
protected under the GNU GPL v.3 or at your option any newer
see doc/gpl3.html for details

see doc/index.html for documentation
