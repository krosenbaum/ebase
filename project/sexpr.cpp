// Electric Base App: S-Expression handling a'la KiCAD
// (c) Konrad Rosenbaum, 2024
// protected under GNU GPL v.3 or at your option any newer

#include <QMetaType>
#include <QFile>

#include "sexpr.h"
using namespace EBase::Project::Private;


// ////////////////////////////
// S-Expression Parser

static QVariantList sexprRec(QStringList&collect)
{
    QVariantList ret;
    while(collect.size()>0){
        const auto token=collect.takeFirst();
        if(token=="(")ret.append(QVariant::fromValue(sexprRec(collect)));
        else if(token==")")return ret;
        else ret.append(token);
    }
    return ret;
}

SExpression::SExpression(const QString&s)
{
    //status
    enum Mode {Space,Token,String,HexString,OctString};
    bool backslash=false;
    Mode mode=Space;
    int parbal=0,paropen=0,parclose=0;
    int line=1,column=0,bsdigits=0,bsval=0,tokencol=-1,tokenline=-1;
    QString item;
    QStringList collect;
    //parse
    for(auto c:s){
        //count lines
        if(c=='\r')continue;
        if(c=='\n'){line++;column=0;}
        column++;
        //parse
        if(mode==Space){
            if(c.isSpace())continue;
            if(c=='\"'){
                mode=String;
                item.clear();
                tokencol=column;tokenline=line;
                continue;
            }
            if(c=='('){
                collect.append("(");
                paropen++;parbal++;
                continue;
            }
            if(c==')'){
                collect.append(")");
                parclose++;parbal--;
                continue;
            }
            mode=Token;
            tokencol=column;tokenline=line;
            item=c;
            continue;
        }
        if(mode==Token){
            if(c.isSpace()){
                if(!item.isEmpty()){
                    collect.append(item);
                    item.clear();
                }
                mode=Space;
                tokencol=tokenline=-1;
                continue;
            }
            if(c=='('){
                if(!item.isEmpty()){
                    collect.append(item);
                    item.clear();
                }
                collect.append("(");
                paropen++;parbal++;
                mode=Space;
                tokencol=tokenline=-1;
                continue;
            }
            if(c==')'){
                if(!item.isEmpty()){
                    collect.append(item);
                    item.clear();
                }
                collect.append(")");
                parclose++;parbal--;
                mode=Space;
                tokencol=tokenline=-1;
                continue;
            }
            item+=c;
            continue;
        }
        if(mode==String){
            if(backslash){
                switch(c.unicode()){
                    case '\"':
                    case '\'':
                    case ' ':
                        item+=c;
                        break;
                    case 'n':item+='\n';break;
                    case 'r':item+='\r';break;
                    case 't':item+='\t';break;
                    case '\\':item+='\\';break;
                    case 'a':item+='\x07';break;
                    case 'b':item+='\x08';break;
                    case 'v':item+='\x0b';break;
                    case 'f':item+='\x0c';break;
                    case 'x':
                        mode=HexString;
                        bsdigits=2;bsval=0;
                        break;
                    default:
                        mode=OctString;
                        bsdigits=2;bsval=c.unicode()-'0';
                        break;
                }
                backslash=false;
                continue;
            }
            if(c=='\\'){
                backslash=true;
                continue;
            }
            if(c=='\"'){
                collect.append(item);
                item.clear();
                mode=Space;
                tokencol=tokenline=-1;
                continue;
            }
            if(c=='\n'){
                merr.append(QString("unterminated string on line %1 column %2").arg(tokenline).arg(tokencol));
                collect.append(item);
                item.clear();
                mode=Space;
                tokencol=tokenline=-1;
                continue;
            }
            item+=c;
            continue;
        }
        if(mode==HexString){
            bsval<<=4;
            if(c>='0' && c<='9')bsval+=c.unicode()-'0';
            else if(c>='a' && c<='z')bsval+=c.unicode()-'a'+10;
            else if(c>='A' && c<='Z')bsval+=c.unicode()-'A'+10;
            bsdigits--;
            if(bsdigits==0){
                mode=String;
                item+=(char)bsval;
            }
            continue;
        }
        if(mode==OctString){
            bsval<<=3;
            bsval+=c.unicode()-'0';
            bsdigits--;
            if(bsdigits==0){
                mode=String;
                item+=(char)bsval;
            }
            continue;
        }
    }
    if(!item.isEmpty()){collect.append(item);}
    //end-checks
    if(parbal!=0){
        merr.append(QString("Parenthesis mismatch - %1 open, %2 close, %3 diff").arg(paropen).arg(parclose).arg(parbal));
        return;
    }
    if(collect.size()==0){
        merr.append(QString("No data found."));
        return;
    }
    if(collect.takeFirst()!="(" || collect.last()!=")"){
        merr.append(QString("S-expr file is expected to start with ( and end with )"));
        return;
    }
    //recursively apply
    mexpr=sexprRec(collect);
}

QList<QVariantList> SExpression::query(const QVariantList&expr, QStringList path)
{
    //no path: everything matches
    if(path.size()==0){
        // qDebug()<<"empty query, return all";
        return QList<QVariantList>()<<expr;
    }
    //compare current
    const QString current=path.takeFirst();
    // qDebug()<<"query"<<current<<expr.size()<<qMetaTypeId<QVariantList>();
    if(!current.isEmpty() && current!=expr.value(0).toString())
        return QList<QVariantList>();
    if(path.isEmpty())
        return QList<QVariantList>()<<expr;
    //go through all subs
    QList<QVariantList> ret;
    for(const auto&sub:expr){
        if(sub.typeId()!=qMetaTypeId<QVariantList>())continue;
        QVariantList sublist=sub.value<QVariantList>();
        for(QVariantList vres:query(sublist,path))
            ret.append(vres);
    }
    //done
    return ret;
}

SExpression SExpression::fromFile(QString fn)
{
    QFile fd(fn);
    if(!fd.open(QIODevice::ReadOnly))
        return SExpression().addError(QString("unable to open file %1: %2").arg(fn).arg(fd.errorString()));
    return SExpression(QString::fromUtf8(fd.readAll()));
}
