// Electric Base App: S-Expression handling a'la KiCAD
// (c) Konrad Rosenbaum, 2024
// protected under GNU GPL v.3 or at your option any newer

#pragma once

#include <QStringList>
#include <QVariantList>

namespace EBase::Project::Private {

///S-Expression Parser.
///This is the parser used to dive into KiCAD symbol and footprint files.
///It roughly follows the rules in https://dev-docs.kicad.org/en/components/sexpr/ .
///The S-Expr is represented by a QVariantList that contains QString for simple tokens, numbers and strings;
///and contains QVariantList items for (sub expressions ...) in parentheses.
class SExpression
{
    ///full expression list
    QVariantList mexpr;
    ///\internal list of all parser errors
    QStringList merr;

    ///\internal helper to add parser errors
    SExpression& addError(QString e){merr.append(e);return *this;}
public:
    ///creates empty object
    SExpression()=default;
    ///copy constructor
    SExpression(const SExpression&)=default;
    ///move constructor
    SExpression(SExpression&&)=default;
    ///creates object from string data
    SExpression(const QString&);

    ///copy assignment
    SExpression& operator=(const SExpression&)=default;
    ///move assignment
    SExpression& operator=(SExpression&&)=default;

    ///comparison
    bool operator==(const SExpression&)const = default;

    ///loads the expression from filename, the file is assumed to be UTF-8 encoded.
    static SExpression fromFile(QString filename);

    ///true if there were no parser errors
    bool isValid()const{return merr.isEmpty();}
    ///returns the entire tree
    QVariantList data()const{return mexpr;}
    ///returns all errors that were found
    QStringList errorStrings()const{return merr;}

    ///verifies that a path exists at least once in the s-expr, see \ref query for details
    bool verifyExist(QStringList path){return query(mexpr,path).size()>0;}
    ///Finds all sub-expressions referenced by path. Starting at the top level of the s-expr tree.
    QList<QVariantList> getAll(QStringList path){return query(mexpr,path);}

    ///Finds all sub-expressions referenced by path inside the s_expr list.
    ///\param s_expr - the expression tree to test against path
    ///\param path - list of strings to compare to each level of the s-expr tree
    ///\note
    ///An empty path (size 0) returns the full s-expr.
    ///The first string must match the full s-expr, each following string goes down one level of sub-expressions.
    ///Each string is compared to the first token in the current expression, then recursing into sub-expressions.
    ///An empty string matches any sub-expression at that level, even simple tokens (if the empty string is the last component of path).
    ///\returns
    ///Returns all sub-expressions that match the path.
    ///For example if the tree is (a (b 1) (c 2) (b 3)) and path is {"a","b"}, then query returns (b 1) and (b 3).
    static QList<QVariantList> query(const QVariantList&s_expr,QStringList path);
};

//END of namespace
}
