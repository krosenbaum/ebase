// Electric Base App: Project Classes
// (c) Konrad Rosenbaum, 2024
// protected under GNU GPL v.3 or at your option any newer


#pragma once

#include "project.h"

#include <QApplication>


namespace EBase{ namespace Project {
///\internal \brief private project implementation details, not available outside project folder
namespace Private {

///notifier for changes in global settings that affect EBase::Project::Project instances; singleton
class ProjectSettingsChange : public QObject
{
    Q_OBJECT
    static ProjectSettingsChange*inst;
    ProjectSettingsChange(){
        connect(qApp,&QApplication::aboutToQuit,this,&QObject::deleteLater);
        qDebug()<<"created Project notifier.";
    }
    ~ProjectSettingsChange(){
        inst=nullptr;
        qDebug()<<"cleaning up Project notifier.";
    }
public:
    ///returns the singleton instance
    static ProjectSettingsChange* instance(){
        if(inst==nullptr)inst=new ProjectSettingsChange;
        return inst;
    }
signals:
    ///the auto-save interval (in Minutes) has changed, \see EBase::Project::Project::autoSaveMinutes()
    void autoSaveChanged(int);
};


// ////////////////////////////
// string constants

//project file patterns
static const QString patEBase{"*.ebase"};
static const QString dotEBase{ ".ebase"};
static const QString extEBase{  "ebase"};

static const QString dotEBaseLock{".ebase-lock"};
//note: must not be .ebase.lock, since this name is used by QSettings for concurrency control

//kicad library file patterns
static const QString patKicadDbl{"*.kicad_dbl"};
static const QString dotKicadDbl{ ".kicad_dbl"};
static const QString extKicadDbl{  "kicad_dbl"};


//kicad library JSON keys

//  version
static const QString libMeta{"meta"};
static const QString libMetaVersion{"version"};
static const int     libDefaultVersion{1};

//  basics
static const QString libName{"name"};
static const QString libDescription{"description"};

//  DB source spec
static const QString libSource{"source"};
static const QString libSourceDbType{"type"};
static const QString libSourceOdbcType{"odbc"};
static const QString libSourceDsn{"dsn"};
static const QString libSourceUser{"username"};
static const QString libSourcePassword{"password"};
static const QString libSourceConnString{"connection_string"};
static const QString libSourceTimeout{"timeout_seconds"};
static const qreal   libSourceDefaultTimeout{2.0};

//  library tables
static const QString libLibrariesKey{"libraries"};
static const QString libLibName{"name"};
static const QString libLibTable{"table"};
static const QString libLibKeyCol{"key"};
static const QString libLibSymbolCol{"symbols"};
static const QString libLibFootprintCol{"footprints"};

static const QString libLibFieldArray{"fields"};
static const QString libLibFieldColumn{"column"};
static const QString libLibFieldName{"name"};
static const QString libLibFieldInherit{"inherit_properties"};
static const bool    libLibFieldInheritDefault{false};
static const QString libLibFieldVisibleOnAdd{"visible_on_add"};
static const bool    libLibFieldVisibleOnAddDefault{false};
static const QString libLibFieldVisibleChooser{"visible_in_chooser"};
static const bool    libLibFieldVisibleChooserDefault{false};
static const QString libLibFieldShowName{"show_name"};
static const bool    libLibFieldShowNameDefault{false};

static const QString libLibPropertyMap{"properties"};
static const QString libLibPropDescription{"description"};
static const QString libLibPropFPFilters{"footprint_filters"};//reserved
static const QString libLibPropKeywords{"keywords"};
static const QString libLibPropExcludeBOM{"exclude_from_bom"};
static const QString libLibPropExcludePCB{"exclude_from_board"};



//global internal settings
static const QString autoSaveKey{"project/autoSaveMins"};
static const int autoSaveMin{1};
static const int autoSaveMax{99};
static const int autoSaveDefault{5};


//EBase project INI file keys

static const QString projectVersionKey{"EBase/ProjectVersion"};

//search pathes
static const QString kicadSymbolSearchPath{"kicad/symbolpath"};
static const QString kicadFootprintSearchPath{"kicad/footprintpath"};

// DB alternate settings
static const QString dbAltUse{"dbalt/use"};
static const bool    dbAltUseDefault{false};
static const QString dbAltDbName{"dbalt/name"};
static const QString dbAltDriver{"dbalt/driver"};
static const QString dbAltHostname{"dbalt/host"};
static const QString dbAltPort{"dbalt/port"};
static const QString dbAltUsername{"dbalt/user"};
static const QString dbAltPassword{"dbalt/passwd"};
static const QString dbAltOptions{"dbalt/options"};

//END of namespace
}}}
