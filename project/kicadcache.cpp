// Electric Base App: KiCAD Symbol/Footprint Cache
// (c) Konrad Rosenbaum, 2024
// protected under GNU GPL v.3 or at your option any newer

#include "kicadcache.h"
#include "kicadcache_p.h"
#include "project.h"
#include "sexpr.h"

#include <QApplication>
#include <QDebug>
#include <QDir>
#include <QFileInfo>
#include <QPointer>
#include <QProgressDialog>

using namespace EBase::Project;
using namespace EBase::Project::Private;

// ////////////////////////////
// Cache Interface

QStringList KicadCache::libraryNames(LibraryType lt) const
{
    switch(lt){
        case SymbolLibrary:return msymbols.keys();
        case FootprintLibrary:return mfootprints.keys();
        default: return QStringList();
    }
}

QStringList EBase::Project::KicadCache::libraryItems(LibraryType lt, QString name) const
{
    if(lt!=SymbolLibrary && lt!=FootprintLibrary)return QStringList();
    auto&cache = lt==SymbolLibrary ? msymbols : mfootprints;
    QStringList items;
    for(auto cline:cache){
        const auto lname=cline->libraryName();
        if(name.isEmpty() || name==lname){
            for(const auto&itm:cline->libraryContents())
                items.append(lname+":"+itm);
        }
    }
    //note: the above loop may seem inefficient, but there may be multiple libs of the same name and they are never sorted
    return items;
}

void KicadCache::loadLibraries(EBase::Project::Project*pro)
{
    if(pro==nullptr)return;
    loadLibrary(LibraryType::SymbolLibrary,pro->symbolSearchPath());
    loadLibrary(LibraryType::FootprintLibrary,pro->footprintSearchPath());
}

void KicadCache::loadLibrary(LibraryType lt, QStringList pathes)
{
    if(lt==SymbolLibrary){
        msymbols.clear();
        for(const auto&p:pathes)loadSymbols(p);
    }
    if(lt==FootprintLibrary){
        mfootprints.clear();
        for(const auto&p:pathes)loadFootprints(p);
    }
}

void KicadCache::loadSymbols(QString path)
{
    QFileInfo fi(path);
    if(!fi.exists()){
        qDebug()<<"loadSymbols:"<<path<<"does not exist. Skipping.";
        return;
    }
    if(fi.isDir()){
        qDebug()<<"loadSymbols: recursing into"<<path;
        //recurse into dir
        QDir d(path);
        for(auto sub:d.entryInfoList(QDir::Dirs|QDir::Files|QDir::NoDotAndDotDot))
            loadSymbols(sub.absoluteFilePath());
        return;
    }
    if(fi.isFile() && path.endsWith(kicadSymbolExtensionDot)){
        if(haveSymbols(path)){
            qDebug()<<"loadSymbols: skipping"<<path<<"- already loaded.";
            return;
        }
        qDebug()<<"loadSymbols: attempting to load"<<path;
        //parse & store
        auto cache=KicadCacheStore::instance().addSymbolPath(path);
        if(cache->isValid())
            msymbols.insert(cache->libraryName(),cache);
        else
            qDebug()<<"    ...file is invalid.";
        //done
        return;
    }
    //nothing matches
    qDebug()<<"loadSymbols: unable to handle"<<path;
}

void KicadCache::loadFootprints(QString path)
{
    QFileInfo fi(path);
    if(!fi.exists()){
        qDebug()<<"loadFootprints:"<<path<<"does not exist. Skipping.";
        return;
    }
    if(!fi.isDir()){
        qDebug()<<"loadFootprints:"<<path<<"is not a directory. Skipping.";
        return;
    }
    //ends in ".pretty" -> parse, otherwise recurse
    if(path.endsWith(kicadFootprintDirExtensionDot)){
        if(haveFootprints(path)){
            qDebug()<<"loadFootprints: skipping"<<path<<"- already loaded.";
            return;
        }
        qDebug()<<"loadFootprints: attempting to load"<<path;
        //parse and store
        auto cache=KicadCacheStore::instance().addFootprintPath(path);
        if(cache->isValid())
            mfootprints.insert(cache->libraryName(),cache);
        else
            qDebug()<<"    ...directory is invalid.";
    }else{
        qDebug()<<"loadFootprints: recursing into"<<path;
        QDir d(path);
        for(auto sub:d.entryInfoList(QDir::Dirs|QDir::NoDotAndDotDot))
            loadFootprints(sub.absoluteFilePath());
    }
}

bool KicadCache::haveSymbols(QString fname) const
{
    for(const auto&c:msymbols)
        if(c->path()==fname)
            return true;
    return false;
}

bool KicadCache::haveFootprints(QString dname) const
{
    for(const auto&c:mfootprints)
        if(c->path()==dname)
            return true;
    return false;
}

KicadPathInfo KicadCache::pathInfo(KicadCache::LibraryType lt, QString p) const
{
    if(lt!=SymbolLibrary && lt!=FootprintLibrary)return KicadPathInfo();
    auto&cache= lt==SymbolLibrary ? msymbols : mfootprints;
    QFileInfo fi(p);
    const QString pathroot=fi.absoluteFilePath();
    bool isleaf=false;
    QList<std::shared_ptr<Private::CacheLine>>lines;
    for(const auto&c:cache){
        if(c->path()==pathroot)isleaf=true;
        if(c->path().startsWith(pathroot))
            lines.append(c);
    }
    KicadPathInfo::PathType pinf=KicadPathInfo::InvalidPath;
    if(lt==SymbolLibrary)
        pinf= isleaf ? KicadPathInfo::SymbolLibrary : KicadPathInfo::SymbolParentDir;
    else
        pinf= isleaf ? KicadPathInfo::FootprintLibrary : KicadPathInfo::FootprintParentDir;
    return KicadPathInfo(p,lt,pinf,lines);
}




// ////////////////////////////
// Cache Line

CacheLine::CacheLine(QString libpath, KicadCache::LibraryType _type)
:mpath(libpath),mtype(_type)
{
    if(_type==KicadCache::NoLibrary)return;
    //check path exists and is good
    QFileInfo fi(libpath);
    if(!fi.exists()){
        mtype=KicadCache::NoLibrary;
        mpath.clear();
        return;
    }
    if(_type==KicadCache::SymbolLibrary){
        //check
        if(!fi.isFile() || !fi.isReadable() || !libpath.endsWith(kicadSymbolExtensionDot)){
            mtype=KicadCache::NoLibrary;
            mpath.clear();
            return;
        }
        //extract name
        mname=fi.fileName();
        mname=mname.left(mname.size()-kicadSymbolExtensionDot.size());
    }else{
        //check
        if(!fi.isDir() || !libpath.endsWith(kicadFootprintDirExtensionDot)){
            mtype=KicadCache::NoLibrary;
            mpath.clear();
            return;
        }
        //extract name
        mname=fi.fileName();
        mname=mname.left(mname.size()-kicadFootprintDirExtensionDot.size());
    }
}

CacheLine::~CacheLine()
{
    //nothing to do.
}

void EBase::Project::Private::CacheLine::loadContents()
{
    mcontent.clear();
    if(mtype==KicadCache::SymbolLibrary){
        //TODO!!
        SExpression exp=SExpression::fromFile(mpath);
        if(!exp.isValid()){
            qDebug()<<"Errors while parsing symbol file"<<mpath<<":"<<exp.errorStrings();
            return;
        }
        //verify it is a lib
        if(!exp.verifyExist(kicadSymbolLibPath)){
            qDebug()<<"Not a symbol lib"<<mpath<<"...skipping.";
            return;
        }
        //get all symbols
        auto syms=exp.getAll(kicadSymbolLibSymbolsPath);
        for(const auto &sym:syms){
            QString name=sym.value(1).toString();
            if(name.isEmpty())continue;
            mcontent.append(name);
            //check properties
            auto props=SExpression::query(sym,QStringList()<<kicadSymbolLibPropsToken);
            QMap<QString,QString>propmap;
            for(const auto&p:props)
                propmap.insert(p.value(1).toString(),p.value(2).toString());
            msymdata.insert(name,propmap);
        }
    }else
    if(mtype==KicadCache::FootprintLibrary){
        QDir d(mpath);
        for(const auto&f:d.entryInfoList(QStringList()<<kicadFootprintExtensionPattern,QDir::Files|QDir::Readable,QDir::Name)){
            QString fn=f.fileName();
            mcontent.append(fn.left(fn.size()-kicadFootprintExtensionDot.size()));
        }
    }
}





// ////////////////////////////
// Path Info Class

QString KicadPathInfo::pathTypeString() const
{
    switch(mptype){
        case InvalidPath:       return tr("Invalid","path type");
        case SymbolLibrary:     return tr("Symbol Library","path type");
        case SymbolParentDir:   return tr("Parent of Symbol Libraries","path type");
        case FootprintLibrary:  return tr("Footprint Library","path type");
        case FootprintParentDir:return tr("Parent of Footprint Libraries","path type");
    }
    //should be unreachable
    Q_UNREACHABLE();
    return QString();
}

QStringList KicadPathInfo::libraryNames() const
{
    QStringList ret;
    for(const auto &c:mcache)
        ret.append(c->libraryName());
    return ret;
}

QStringList KicadPathInfo::libraryPathes() const
{
    QStringList ret;
    for(const auto &c:mcache)
        ret.append(c->path());
    return ret;
}

QStringList KicadPathInfo::itemNames(QString lib) const
{
    int i=0;
    QProgressDialog pd(tr("Loading KiCAD Items"),tr("Close"),0,mcache.size(),nullptr);
    QStringList ret;
    for(const auto &c:mcache){
        pd.setValue(i++);
        qApp->processEvents();
        if(lib.isEmpty() || lib==c->libraryName())
            ret.append(c->libraryContents());
    }
    return ret;
}

int KicadPathInfo::numItems(QString lib) const
{
    int ret=0,i=0;
    QProgressDialog pd(tr("Loading KiCAD Items"),tr("Close"),0,mcache.size(),nullptr);
    for(const auto &c:mcache){
        pd.setValue(i++);
        qApp->processEvents();
        if(lib.isEmpty() || lib==c->libraryName())
            ret+=c->libraryContents().size();
    }
    return ret;
}







// ////////////////////////////
// Storage Class

KicadCacheStore::KicadCacheStore()
{
    connect(qApp,&QApplication::aboutToQuit,this,&QObject::deleteLater);
    qDebug()<<"Creating KiCAD Cache Store.";
}

KicadCacheStore::~KicadCacheStore()
{
    qDebug()<<"Deleting KiCAD Cache Store.";
}


//static
KicadCacheStore& KicadCacheStore::instance()
{
    static QPointer<EBase::Project::Private::KicadCacheStore> inst;
    if(inst.isNull())inst=new EBase::Project::Private::KicadCacheStore;
    return *inst;
}

std::shared_ptr<CacheLine> KicadCacheStore::addSymbolPath(QString path)
{
    const QString apath=QFileInfo(path).absoluteFilePath();
    //check whether it already exists
    if(msymbols.contains(apath))return msymbols[apath];
    //create cache and return
    std::shared_ptr<CacheLine> cache{new CacheLine(apath,KicadCache::SymbolLibrary)};
    if(cache->isValid())
        msymbols.insert(apath,cache);
    return cache;
}

std::shared_ptr<CacheLine> KicadCacheStore::addFootprintPath(QString path)
{
    const QString apath=QFileInfo(path).absoluteFilePath();
    //check whether it already exists
    if(mfootprints.contains(apath))return mfootprints[apath];
    //create cache and return
    std::shared_ptr<CacheLine> cache{new CacheLine(apath,KicadCache::FootprintLibrary)};
    if(cache->isValid())
        mfootprints.insert(apath,cache);
    return cache;
}
