// Electric Base App: System Info
// (c) Konrad Rosenbaum, 2024
// protected under GNU GPL v.3 or at your option any newer

#include "project_sysinfo.h"

#include <QHostInfo>
#include <QCoreApplication>

namespace EBase { namespace Project {

QString userName()
{
    QString un=qgetenv("USER");
    if(un.isEmpty())un=qgetenv("USERNAME");
    return un;
}

QString hostName()
{
    return QHostInfo::localHostName();
}

qint64 processId()
{
    return QCoreApplication::applicationPid();
}


//END of namespaces
}}
