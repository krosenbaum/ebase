// Electric Base App: System Info
// (c) Konrad Rosenbaum, 2024
// protected under GNU GPL v.3 or at your option any newer


#pragma once

#include <QString>

namespace EBase { namespace Project {

QString userName();
QString hostName();
qint64 processId();


//END of namespaces
}}
