// Electric Base App: KiCAD Symbol/Footprint Cache
// (c) Konrad Rosenbaum, 2024
// protected under GNU GPL v.3 or at your option any newer

#pragma once

#include <QObject>
#include <QMap>
#include <QMultiMap>
#include <QCoreApplication>

#include <memory>

namespace EBase::Project {

class Project;

namespace Private { class CacheLine; }

class KicadCache;
class KicadPathInfo;

///Cache Interface for KiCAD symbol and footprint libraries.
///These cache interfaces are temporary and multiple instances can exist at the same time for different purposes.
///The actual cache data is stored in the background and does not need to be reloaded for every cache interface instance.
class KicadCache : public QObject
{
    Q_OBJECT
    KicadCache(const KicadCache&)=delete;
public:
    ///Types of known libraries.
    enum LibraryType {
        ///Error value: not a library.
        NoLibrary,
        ///Symbol library.
        SymbolLibrary,
        ///Footprint library.
        FootprintLibrary
    };

    ///Create new cache interface.
    explicit KicadCache(QObject*parent=nullptr):QObject(parent){}

    ///Returns the names of known libraries of a specific type.
    QStringList libraryNames(LibraryType)const;
    ///Returns the items (symbols or footprints) of a specific library of a type.
    ///\param tp type of library to search
    ///\param nm name of the library to search, if empty then contents of all known libraries are returned
    ///\returns Returns the names of matching items in "library-name:item-name" format.
    QStringList libraryItems(LibraryType tp,QString nm)const;

    ///Returns an info object for a specific library type and name.
    KicadPathInfo pathInfo(LibraryType,QString)const;

public slots:
    ///Loads libraries of the given type, the list may contain multiple pathes that may be parent directories.
    ///If parent directories are used then they are searched recursively.
    void loadLibrary(LibraryType,QStringList);
    ///Loads all libraries referenced in the project's searchPathes.
    void loadLibraries(Project*);
    ///Loads a symbol library. The path may be a parent path that is searched recursively.
    void loadSymbols(QString);
    ///Loads a footprint library. The path may be a parent path that is searched recursively.
    void loadFootprints(QString);

private:
    ///maps library names to item caches
    QMultiMap<QString,std::shared_ptr<Private::CacheLine>> msymbols,mfootprints;
    ///\internal true if the symbol file name is already loaded
    bool haveSymbols(QString fname)const;
    ///\internal true if the footprint directory is already loaded
    bool haveFootprints(QString dname)const;
};

///Represents generic infos about a specific library path or parent path.
class KicadPathInfo
{
    Q_DECLARE_TR_FUNCTIONS(KicadPathInfo)
public:
    ///Constructs empty path info object.
    KicadPathInfo()=default;
    ///copy constructor
    KicadPathInfo(const KicadPathInfo&)=default;
    ///move constructor
    KicadPathInfo(KicadPathInfo&&)=default;

    ///copy assignment
    KicadPathInfo& operator=(const KicadPathInfo&)=default;
    ///move assignment
    KicadPathInfo& operator=(KicadPathInfo&&)=default;

    ///Type of path represented by this info object.
    enum PathType {
        ///Not a valid info object or path does not contain anything of value.
        InvalidPath,
        ///Represents a single symbol library file.
        SymbolLibrary,
        ///Represents a directory containing many symbol files.
        SymbolParentDir,
        ///Represents a single footprint directory (*.pretty).
        FootprintLibrary,
        ///Represents a directory containing many footprint libraries.
        FootprintParentDir
    };

    ///the path represented by this info object
    QString pathName()const{return mpath;}
    ///Brief KicadCache classification of the library type.
    KicadCache::LibraryType libraryType()const{return mltype;}
    ///More detailed info classification of the library type.
    PathType pathType()const{return mptype;}
    ///Type description as a human readable string.
    QString pathTypeString()const;

    ///Names of all libraries in this path.
    QStringList libraryNames()const;
    ///Full pathes of all libraries below this path.
    QStringList libraryPathes()const;
    ///Number of libraries represented by this info object.
    int numLibraries()const{return mcache.size();}

    ///Names of all sub-items (symbols/footprints) in all libraries represented by this object.
    ///\note This is not very useful for normal work since it does not contain the library name part of the item.
    ///Use the KicadCache class for this.
    ///
    ///\note This function can take some time to load. A progress dialog will be shown if necessary.
    QStringList itemNames(QString lib=QString())const;
    ///Number of items in all libraries belonging to the info object for statistics.
    ///\note This function can take some time to load. A progress dialog will be shown if necessary.
    int numItems(QString lib=QString())const;

private:
    QString mpath;
    KicadCache::LibraryType mltype=KicadCache::NoLibrary;
    PathType mptype=InvalidPath;
    QList<std::shared_ptr<Private::CacheLine>> mcache;

    friend class KicadCache;
    ///used by KicadCache to provide info
    KicadPathInfo(const QString&p,KicadCache::LibraryType tp,PathType pp,const QList<std::shared_ptr<Private::CacheLine>>&c)
    :mpath(p),mltype(tp),mptype(pp),mcache(c){}
};


//END of namespace
}
