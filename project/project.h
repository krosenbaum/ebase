// Electric Base App: Project Classes
// (c) Konrad Rosenbaum, 2024
// protected under GNU GPL v.3 or at your option any newer

#pragma once

#include <QJsonDocument>
#include <QJsonObject>
#include <QMap>
#include <QObject>
#include <QStringList>
#include <QUuid>

#include "db/dbinfo.h"

namespace EBase {

namespace ODBC {
    class ODBCconnectionSettings;
}

///Project classes namespace.
namespace Project {

class Project;

///Represents the ODBC settings of a project. This is an explicitly shared class.
class OdbcSettings
{
    Project&mprj;
    OdbcSettings()=delete;
    explicit OdbcSettings(Project&p):mprj(p){}
    friend class Project;
public:
    ///create new reference to project ODBC settings
    OdbcSettings(const OdbcSettings&)=default;

    ///returns true if this represents the settings of a valid project
    bool isValid()const;

    ///returns the DSN
    QString dsn()const;
    ///returns user name to use with DSN
    QString username()const;
    ///returns password to use with DSN
    QString password()const;
    ///returns connection string to use instead of DSN
    QString connectionString()const;
    ///returns timeout in seconds
    qreal timeout()const;

    ///returns true if this DB uses a connection string
    bool useConnectionString()const;
    ///returns true if this DB uses a DSN (plus optional username and password)
    bool useDSN()const;

    ///set this project to use a DSN (with optional username and password)
    void setDSN(QString dsn,QString user=QString(),QString password=QString());
    ///set this project to use a connection string
    void setConnectionString(QString cs);
    ///set the timeout in seconds
    void setTimeout(qreal);

    ///return as ODBC driver settings
    EBase::ODBC::ODBCconnectionSettings toDriverSettings()const;
};

///Settings for alternate DB access. Direct reference to the Project.
class DbSettings
{
    Project&mprj;
    DbSettings()=delete;
    ///used by Project to create the instance
    explicit DbSettings(Project&p):mprj(p){}
    friend class Project;
    ///helper to find settings file
    QString projectFileName()const;
public:
    ///creates a new reference to the same Project's settings
    DbSettings(const DbSettings&)=default;

    ///returns true if this references settings of a valid project
    bool isValid()const;

    ///true if an alternate DB connection is used in EBase
    bool useAlternate()const;
    ///true if the KiCAD DB connection is used directly
    bool useKicadOdbc()const{return !useAlternate();}

    ///returns the name of the database/schema
    QString databaseName()const;
    ///returns the name of the driver in Qt speak (references a QtSql plugin)
    QString driverName()const;
    ///returns the type of the DB driver, if it is known
    DB::DBInfo::DBType driverType()const;
    ///returns the host name for the DB server if networking is used
    QString hostName()const;
    ///returns the TCP/UDP port of the DB server if networking is used
    int portNumber()const;
    ///returns the user name for DB authentication
    QString username()const;
    ///returns the password for DB authentication
    QString password()const;
    ///returns the string of connection options
    QString connectOptions()const;

    ///sets whether to use an alternate DB connection (true) or the KiCAD definition (false)
    void setUseAlternate(bool);
    ///sets whether to use the KiCAD DB definition (true) or an alternate DB connection (false)
    void setUseKicadOdbc(bool u){setUseAlternate(!u);}

    ///sets the QtSql plugin to be used for the connection
    bool setDriver(QString);
    ///sets the QtSql plugin to be used by known DB type
    bool setDriver(DB::DBInfo::DBType);
    ///sets the name of the database
    void setDatabaseName(QString);
    ///sets the host and port of the DB server
    void setHost(QString host,int port=-1);
    ///sets user name and password for the connection
    void setLogin(QString user,QString password);
    ///sets the connect options string
    void setConnectOptions(QString);
};


///Represents a reference to a library definition in the project. This is an explicitly shared class.
///Each KiCAD DB library project can contain multiple libraries, usually pointing to different tables or views.
///Each such library is represented by an instance of LibraryDefinition, you can use \ref Project::library to get one.
///Do not store LibraryDefinition instances for any amount of time, since they become corrupted if their Project is deleted. Get a fresh one for every access.
class LibraryDefinition
{
    Project&mprj;
    QUuid mid;

    LibraryDefinition()=delete;
    LibraryDefinition& operator=(const LibraryDefinition&)=delete;
    ///used by Project to instantiate a definition
    LibraryDefinition(Project&p,QUuid i):mprj(p),mid(i){}
    friend class Project;
public:
    ///creates a new reference to the same definition
    LibraryDefinition(const LibraryDefinition&)=default;

    ///returns true if this is a valid definition in a valid Project
    bool isValid()const;

    ///returns true if both objects refer to the same definition
    bool operator==(const LibraryDefinition&o)const{if(mid.isNull())return false;return mid==o.mid;}

    ///returns the internal UUID of the library
    QUuid internalID()const{return mid;}

    ///helper to determine whether the string is a valid library name for \ref setName
    static bool nameIsValid(const QString&);

    ///returns the name of the library
    QString name()const;
    ///sets a new name for the library - the name must follow KLC guidelines (verified with \ref nameIsValid)
    bool setName(QString);

    ///returns the referenced table or view
    QString tableName()const;
    ///returns the column with the (string) key for the part
    QString keyColumn()const;
    ///returns the column with the symbol reference for the part
    QString symbolColumn()const;
    ///returns the column with the footprint reference for the part
    QString footprintColumn()const;

    ///sets the table or view referenced by this library
    void setTableName(QString);
    ///sets the key column
    void setKeyColumn(QString);
    ///sets the column with the KiCAD symbol reference
    void setSymbolColumn(QString);
    ///sets the column with the KiCAD footprint reference
    void setFootprintColumn(QString);

    ///returns the names of all additional symbol/footprint fields that are taken from the database
    QStringList fieldNames()const;
    ///true if a field with that name is already defined for this library
    bool hasField(QString)const;

    ///returns the column with the contents for this field
    QString fieldColumn(QString)const;
    ///returns whether the field is visible in the schematic when adding the symbol
    bool fieldVisibleOnAdd(QString)const;
    ///returns whether the field is visible in the symbol chooser as a column
    bool fieldVisibleInChooser(QString)const;
    ///returns true if the name of the field is visible in the schematic
    bool fieldShowName(QString)const;
    ///if true: if the field already exists in the base symbol then only the contents are taken from the DB and everything else from the symbol
    bool fieldInheritProperties(QString)const;

    ///adds a new field to the library, the field must not yet exist
    ///\param name the name of the field, must be non-empty
    ///\param column the name if the column that contains the contents of the field, must be non-empty
    ///\returns true on success or false if the field exists or a parameter is invalid
    bool addField(QString name,QString column);
    ///changes the column of a field
    ///\param name the name of the field, must exist already
    ///\param column the name if the column that contains the contents of the field, must be non-empty
    ///\returns true on success or false if a parameter is invalid
    bool setFieldColumn(QString name,QString column);
    ///sets visibility of the field value
    bool setFieldVisibility(QString name,bool visibleOnAdd,bool visibleInChooser);
    ///sets visibility of the field name in the schematic
    bool setFieldShowName(QString name,bool showname);
    ///sets whether to inherit field properties from the base symbol
    bool setFieldInheritProperties(QString name,bool inherit);
    ///removes the field from the library definition
    bool removeField(QString name);

    ///returns the column that contains the description of the part, or empty if this is not in the DB
    QString descriptionColumn()const;
    ///returns the column that contains keywords for the part, or empty if this is not in the DB
    QString keywordsColumn()const;
    ///returns the column that contains whether the part is part of the BOM (must be a numeric column)
    QString excludeBomColumn()const;
    ///returns the column that contains whether the part is part of the board (must be a numeric column)
    QString excludeBoardColumn()const;

    ///sets the column for descriptions (deletes the property if an empty string is used)
    void setDescriptionColumn(QString);
    ///sets the column for keywords (deletes the property if an empty string is used)
    void setKeywordsColumn(QString);
    ///sets the column for BOM exclusion (deletes the property if an empty string is used)
    void setExcludeBomColumn(QString);
    ///sets the column for board exclusion (deletes the property if an empty string is used)
    void setExcludeBoardColumn(QString);
private:
    ///helper: find a field by name for reading
    QJsonObject findField(QString)const;
};

///Represents a Database Library project.
///A project always consists of two files: a KiCAD library (*.kicad_dbl) and an EBase project file (*.ebase)
///with additional settings. Both files must be in the same location and have the same base name, but different
///extensions.
class Project : public QObject
{
    Q_OBJECT

    Project()=delete;
    Project(const Project&)=delete;
public:
    ///returns file extension of EBase project files
    static QString projectExtension();
    ///returns file pattern of EBase project files
    static QString projectPattern();
    ///returns file extension of KiCAD DB libraries
    static QString kicadExtension();
    ///returns file pattern of KiCAD DB libraries
    static QString kicadPattern();
    ///returns the full pattern string for a QFileDialog
    static QString fileDialogPattern();

    ///returns the current setting for auto-save timer
    static int autoSaveMinutes();
    ///sets the auto-save timer
    static void setAutoSaveMinutes(int);

    ///How to open the project.
    enum class OpenMode {
        ///Open existing project (only succeeds if at least one of the files exists). Fails if there is a lock.
        OpenExisting,
        ///Open existing project and steal lock if necessary.
        OpenExistingForce,
        ///Create new project (fails if either file already exists).
        CreateNew,
        ///Re-Create the project - ruthlessly overwrites existing files.
        ReCreateNew,
    };
    ///Create project from file.
    ///\param filename name (or root) of the file to be opened, can be the *.ebase or the *.kicad_dbl - the other one will be determined automatically
    ///\param mode whether to open or create the file
    ///\param _parent object (usually main window) that owns the project
    Project(QString filename,OpenMode mode=OpenMode::OpenExisting,QObject*_parent=nullptr);
    ///save and close the project
    ~Project();

    ///returns true if this project represents valid files
    bool isValid()const{return !msetfile.isEmpty() && !mlibfile.isEmpty();}

    ///file name of the project (*.ebase)
    QString projectFileName()const{return msetfile;}
    ///file name of the KiCAD library (*.kicad_dbl)
    QString libraryFileName()const{return mlibfile;}
    ///project description
    QString description()const;
    ///project name
    QString name()const;

    ///search pathes for symbol files
    QStringList symbolSearchPath()const;
    ///search pathes for footprint directories
    QStringList footprintSearchPath()const;

    ///returns a reference to the KiCAD ODBC settings
    OdbcSettings odbcSettings(){return OdbcSettings(*this);}

    ///returns a reference to the alternate DB connection settings
    DbSettings dbSettings(){return DbSettings(*this);}

    ///Returns the names of all defined libraries, it is legal to repeat names.
    QStringList libraryNames()const;
    ///Returns the internal UUIDs of libraries.
    ///Those UUIDs uniquely identify each library definition in this session.
    ///UUIDs are generated and stable per session, but are discarded when closing the project.
    QList<QUuid> libraryUids(){return mlibids;}
    ///Return the name of a library.
    QString libraryName(QUuid id);
    ///returns how many libraries are defined in this project.
    int numLibraries()const{return mlibids.size();}
    ///returns a reference to a library definition by its UUID. The definition can then be changed.
    LibraryDefinition library(QUuid id);
    ///Creates a new (empty) library and returns a reference to its definition, use the definition to adjust it.
    LibraryDefinition addLibrary();
    ///Deletes a library from the project. All existing references to it become invalid.
    void removeLibrary(QUuid id);

    ///returns lock information.
    ///If it is a valid project session it is the info stored in its lock.
    ///If it is invalid then this returns the info stored by the process that has it open.
    QString lockInfo()const{return mlockinfo;}

public slots:
    ///saves any unsaved data to the library file (project settings are auto-saved)
    ///\param force if true: saves, even if there are no changes; if false: only saves if there are changes
    void save(bool force=false);

    ///change the project description
    void setDescription(QString);
    ///change the project name
    void setName(QString);

    ///set search pathes for symbols
    void setSymbolSearchPath(const QStringList&);
    ///set search pathes for footprints
    void setFootprintSearchPath(const QStringList&);

private:
    ///project/settings file name, empty if no project
    QString msetfile;
    ///KiCAD library file name, empty if no project
    QString mlibfile;
    ///JSON document for library file
    QJsonDocument mlibjdoc;
    ///Library Cache: order
    QList<QUuid>mlibids;
    ///Library Cache: data
    QMap<QUuid,QJsonObject>mlibobj;
    ///true if there are unsaved changes
    bool mdirty=false;
    ///contains lock file infos
    QString mlockinfo;

    ///check that the library doc has all the mandatory elements (used by constructor)
    void checkLibDoc();
    ///transfer libraries from JSON to cache (used by constructor)
    void loadLibCache();
    ///transfer libraries from cache to JSON doc (used by save())
    void pushLibCache();

    friend class OdbcSettings;
    friend class DbSettings;
    friend class LibraryDefinition;
};


//speed ups...
inline bool OdbcSettings::isValid()const{return mprj.isValid();}
inline bool DbSettings::isValid()const{return mprj.isValid();}
inline bool LibraryDefinition::isValid()const{return mprj.isValid() && !mid.isNull() && mprj.mlibids.contains(mid);}
inline QString DbSettings::projectFileName()const{return mprj.msetfile;}

//END of namespace
}}
