// Electric Base App: Project Classes
// (c) Konrad Rosenbaum, 2024
// protected under GNU GPL v.3 or at your option any newer

#include "project.h"
#include "project_p.h"
#include "project_sysinfo.h"
#include "db/odbcdrv.h"

#include <QFileInfo>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonParseError>
#include <QSettings>
#include <QTimer>


using namespace EBase::Project::Private;

ProjectSettingsChange* ProjectSettingsChange::inst = nullptr;


///\internal helper macro to define project file API variable
#define PRJSET(set) QSettings set(projectFileName(),QSettings::IniFormat)





// ////////////////////////////
// Main Project class

QString EBase::Project::Project::projectExtension(){return extEBase;}
QString EBase::Project::Project::projectPattern(){return patEBase;}
QString EBase::Project::Project::kicadExtension(){return extKicadDbl;}
QString EBase::Project::Project::kicadPattern(){return patKicadDbl;}

QString EBase::Project::Project::fileDialogPattern()
{
    return tr("Project or DB Library (%1 %2);;Project (%1);;DB Library (%2);;Any File (*.*)")
        .arg(patEBase).arg(patKicadDbl);
}



int EBase::Project::Project::autoSaveMinutes()
{
    return QSettings().value(autoSaveKey,autoSaveDefault).toInt();
}

void EBase::Project::Project::setAutoSaveMinutes(int m)
{
    if(m>=autoSaveMin && m<=autoSaveMax){
        QSettings().setValue(autoSaveKey,m);
        emit ProjectSettingsChange::instance()->autoSaveChanged(m);
    }
}



EBase::Project::Project::Project(QString filename, OpenMode mode, QObject* _parent)
:QObject(_parent)
{
    QString setfile,libfile;
    //what kind is it?
    if(filename.endsWith(dotEBase)){
        setfile=filename;
        libfile=filename.left(filename.size()-dotEBase.size())+dotKicadDbl;
    }else if(filename.endsWith(dotKicadDbl)){
        libfile=filename;
        setfile=filename.left(filename.size()-dotKicadDbl.size())+dotEBase;
    }else{
        libfile=filename+dotKicadDbl;
        setfile=filename+dotEBase;
    }
    //check lock exists
    //NOTE: there is a slight race condition between this and the creation of a new lock below. We'll take the risk for now.
    const QString lockfile=setfile.left(setfile.size()-dotEBase.size())+dotEBaseLock;
    if(QFileInfo::exists(lockfile)){
        qDebug()<<"Project"<<filename<<"is locked by another process.";
        QFile fd(lockfile);
        fd.open(QIODevice::ReadOnly);
        mlockinfo=QString::fromUtf8(fd.readAll()).trimmed();
        fd.close();
        if(mlockinfo.isEmpty())mlockinfo=tr("lock file exists");
        qDebug().noquote()<<"Lock info:\n"<<mlockinfo;
        if(mode!=OpenMode::OpenExistingForce)
            return;
        else
            qDebug()<<"Heads up: I'm going to break that lock.";
    }
    //check file exists
    QFileInfo fi(setfile);
    if(mode==OpenMode::OpenExisting || mode==OpenMode::OpenExistingForce){
        QFileInfo sfi(setfile);
        QFileInfo lfi(libfile);
        const bool sfx=sfi.exists() && sfi.isFile() && sfi.isReadable() && sfi.isWritable();
        const bool lfx=lfi.exists() && lfi.isFile() && lfi.isReadable() && lfi.isWritable();
        //check file really exists, so it can be opened
        if(!sfx && !lfx){
            qDebug()<<"Ooops! Cannot open project:"<<filename<<"- No accessible file.";
            qDebug()<<"\t"<<setfile<<"does not exist or is not accessible";
            qDebug()<<"\t"<<libfile<<"does not exist or is not accessible";
            return;
        }
    }else if(mode==OpenMode::CreateNew){
        QFileInfo sfi(setfile);
        QFileInfo lfi(libfile);
        const bool sfx=sfi.exists();
        const bool lfx=lfi.exists();
        if(sfx || lfx){
            qDebug()<<"Ooops! Cannot create project:"<<filename<<"- At least one file already exists.";
            qDebug()<<"\t"<<setfile<<(sfx?"already exists, cannot create":"does not exist, okay");
            qDebug()<<"\t"<<libfile<<(lfx?"already exists, cannot create":"does not exist, okay");
            return;
        }
    }else if(mode==OpenMode::ReCreateNew){
        if(QFileInfo(setfile).exists()){
            qDebug()<<"Ruthlessly overriding"<<setfile;
            QFile::remove(setfile);
        }
        if(QFileInfo(libfile).exists()){
            qDebug()<<"Ruthlessly overriding"<<libfile;
            QFile::remove(libfile);
        }
    }else{
        qDebug()<<"Weird. Never seen Project::OpenMode"<<(int)mode;
        qDebug()<<"Giving up. Please report this as a bug.";
        return;
    }

    //create lock
    QFile lockfd(lockfile);
    if(lockfd.open(QIODevice::WriteOnly|QIODevice::Truncate|QIODevice::Text)){
        mlockinfo=tr("Open Time: %1\nUser: %2\nHost: %3\nProcess ID: %4")
            .arg(QDateTime::currentDateTime().toString())
            .arg(EBase::Project::userName())
            .arg(EBase::Project::hostName())
            .arg(EBase::Project::processId());
        lockfd.write(mlockinfo.toUtf8());
        lockfd.close();
        qDebug()<<"Locked project  "<<setfile;
        qDebug()<<"  With lock file"<<lockfile;
        qDebug().noquote()<<"  Lock Info:\n\t"<<QString(mlockinfo).replace("\n","\n\t");
    }else{
        qDebug()<<"Ooops. Unable to create new lock file. Giving up.";
        mlockinfo=tr("Unable to lock project.");
        return;
    }

    //done, init and remember
    msetfile=setfile;
    mlibfile=libfile;
    qDebug()<<"Opened project"<<msetfile<<"DB"<<mlibfile;

    //create missing
    if(!QFileInfo::exists(msetfile)){
        qDebug()<<"creating missing project file"<<msetfile;
        PRJSET(s);
        s.setValue(projectVersionKey,1);
    }
    if(!QFileInfo::exists(mlibfile)){
        qDebug()<<"creating missing project file"<<mlibfile;
        QFile fd(mlibfile);
        fd.open(QIODevice::WriteOnly|QIODevice::NewOnly);
        fd.write("{}");
        fd.close();
    }

    //load JSON
    QFile fd(mlibfile);
    if(fd.open(QIODevice::ReadOnly)){
        QJsonParseError err;
        mlibjdoc=QJsonDocument::fromJson(fd.readAll(),&err);
        fd.close();
        if(mlibjdoc.isNull())
            qDebug()<<"Error while parsing library file"<<err.errorString();
    }
    if(!mlibjdoc.isObject()){
        qDebug()<<"Library JSON is not an object. Resetting.";
        mlibjdoc=QJsonDocument(QJsonObject());
    }
    checkLibDoc();
    loadLibCache();

    //auto-save timer
    if(isValid()){
        auto*tmr=new QTimer(this);
        connect(tmr,&QTimer::timeout,this,std::bind(&Project::save,this,false));
        connect(ProjectSettingsChange::instance(),&ProjectSettingsChange::autoSaveChanged,tmr,qOverload<int>(&QTimer::start));
        tmr->setSingleShot(false);
        tmr->setTimerType(Qt::VeryCoarseTimer);
        int ast=QSettings().value(autoSaveKey,autoSaveDefault).toInt();
        if(ast<autoSaveMin)ast=autoSaveMin;
        if(ast>autoSaveMax)ast=autoSaveMax;
        tmr->start(ast*60000);
    }
}

EBase::Project::Project::~Project()
{
    if(!isValid()){
        qDebug()<<"releasing invalid project";
        return;
    }
    qDebug()<<"Closing project"<<msetfile;
    save();
    const QString lockfile=msetfile.left(msetfile.size()-dotEBase.size())+dotEBaseLock;
    if(QFile::remove(lockfile))
        qDebug()<<"Removed lock file"<<lockfile;
    else
        qDebug()<<"Hmm. Unable to remove lock file"<<lockfile<<"...you may have to break it later.";
    qDebug()<<"Closed Project"<<msetfile;
}

QString EBase::Project::Project::description() const
{
    if(!isValid())return QString();
    return mlibjdoc.object().value(libDescription).toString();
}

void EBase::Project::Project::setDescription(QString d)
{
    if(!isValid())return;
    auto doc=mlibjdoc.object();
    doc.insert(libDescription,d);
    mlibjdoc.setObject(doc);
    mdirty=true;
}

QString EBase::Project::Project::name() const
{
    if(!isValid())return QString();
    return mlibjdoc.object().value(libName).toString();
}

void EBase::Project::Project::setName(QString n)
{
    if(!isValid())return;
    auto obj=mlibjdoc.object();
    obj.insert(libName,n);
    mlibjdoc.setObject(obj);
    mdirty=true;
}

QStringList EBase::Project::Project::symbolSearchPath() const
{
    if(!isValid())return QStringList();
    PRJSET(prj);
    return prj.value(kicadSymbolSearchPath).toStringList();
}

void EBase::Project::Project::setSymbolSearchPath(const QStringList&s)
{
    if(!isValid())return;
    PRJSET(prj);
    prj.setValue(kicadSymbolSearchPath,s);
}

QStringList EBase::Project::Project::footprintSearchPath() const
{
    if(!isValid())return QStringList();
    PRJSET(prj);
    return prj.value(kicadFootprintSearchPath).toStringList();
}

void EBase::Project::Project::setFootprintSearchPath(const QStringList&s)
{
    if(!isValid())return;
    PRJSET(prj);
    prj.setValue(kicadFootprintSearchPath,s);
}

void EBase::Project::Project::save(bool force)
{
    if(!isValid())return;
    if(!mdirty && !force)return;
    //sync library cache
    pushLibCache();
    //save library
    QFile lfd(mlibfile);
    if(lfd.open(QIODevice::WriteOnly|QIODevice::Truncate)){
        qDebug()<<"Writing library"<<mlibfile;
        lfd.write(mlibjdoc.toJson());
        lfd.close();
        mdirty=false;
    }else{
        qDebug()<<"Unable to write library file"<<mlibfile<<":"<<lfd.errorString();
    }
}

void EBase::Project::Project::checkLibDoc()
{
    QJsonObject doc=mlibjdoc.object();
    if(!doc.contains(libMeta) || !doc.value(libMeta).toObject().contains(libMetaVersion))
        doc.insert(libMeta,QJsonObject{{libMetaVersion,libDefaultVersion}});
    if(!doc.contains(libName))
        doc.insert(libName,QString());
    if(!doc.contains(libDescription))
        doc.insert(libDescription,QString());
    if(!doc.contains(libSource))
        doc.insert(libSource,QJsonObject{
            {libSourceDbType, libSourceOdbcType},
            {libSourceDsn, QString()},
            {libSourceTimeout, libSourceDefaultTimeout}
        });
    if(!doc.contains(libLibrariesKey))
        doc.insert(libLibrariesKey,QJsonArray());
    mlibjdoc.setObject(doc);
}

void EBase::Project::Project::loadLibCache()
{
    mlibids.clear();
    mlibobj.clear();
    //go through JSON
    for(const auto v:mlibjdoc.object().value(libLibrariesKey).toArray()){
        if(!v.isObject())continue;
        //get unique ID in case we shuffle things a bit later
        QUuid id=QUuid::createUuid();
        //store it
        mlibids.append(id);
        mlibobj.insert(id, v.toObject());
    }
}

void EBase::Project::Project::pushLibCache()
{
    if(!isValid())return;
    //create new array
    QJsonArray lar;
    //fill array
    for(const auto &id:mlibids){
        lar.append(mlibobj.value(id));
    }
    //store in doc
    QJsonObject doc=mlibjdoc.object();
    doc.insert(libLibrariesKey,lar);
    mlibjdoc.setObject(doc);
}



QStringList EBase::Project::Project::libraryNames() const
{
    if(!isValid())return QStringList();
    QStringList ret;
    for(const auto &id:mlibids){
        const QJsonObject o=mlibobj.value(id);
        ret.append(o.value(libLibName).toString());
    }
    return ret;
}

EBase::Project::LibraryDefinition EBase::Project::Project::library(QUuid id)
{
    if(!isValid() || !mlibids.contains(id))
        return LibraryDefinition(*this,QUuid());
    return LibraryDefinition(*this,id);
}

QString EBase::Project::Project::libraryName(QUuid id)
{
    if(!isValid() || !mlibids.contains(id))
        return QString();
    //find name
    const QJsonObject o=mlibobj.value(id);
    return o.value(libLibName).toString();
}

EBase::Project::LibraryDefinition EBase::Project::Project::addLibrary()
{
    if(!isValid())
        return LibraryDefinition(*this,QUuid());
    //create base object
    QUuid id=QUuid::createUuid();
    QJsonObject obj{
        {libLibFieldName, id.toString(QUuid::WithoutBraces)}
    };
    //add to cache
    mlibids.append(id);
    mlibobj.insert(id,obj);
    mdirty=true;
    //return
    return LibraryDefinition(*this,id);
}

void EBase::Project::Project::removeLibrary(QUuid id)
{
    if(!isValid() || !mlibids.contains(id))return;
    mlibids.removeAll(id);
    mlibobj.remove(id);
    mdirty=true;
}






// ////////////////////////////
// KiCAD ODBC settings

QString EBase::Project::OdbcSettings::dsn() const
{
    if(!isValid())return QString();
    return mprj.mlibjdoc.object().value(libSource).toObject().value(libSourceDsn).toString();
}

bool EBase::Project::OdbcSettings::useDSN() const
{
    if(!isValid())return false;
    return mprj.mlibjdoc.object().value(libSource).toObject().value(libSourceDsn).toString().isEmpty()!=true;
}

bool EBase::Project::OdbcSettings::useConnectionString() const
{
    if(!isValid())return false;
    return mprj.mlibjdoc.object().value(libSource).toObject().value(libSourceConnString).toString().isEmpty()!=true;
}

QString EBase::Project::OdbcSettings::username() const
{
    if(!isValid())return QString();
    return mprj.mlibjdoc.object().value(libSource).toObject().value(libSourceUser).toString();
}

QString EBase::Project::OdbcSettings::password() const
{
    if(!isValid())return QString();
    return mprj.mlibjdoc.object().value(libSource).toObject().value(libSourcePassword).toString();
}

QString EBase::Project::OdbcSettings::connectionString() const
{
    if(!isValid())return QString();
    return mprj.mlibjdoc.object().value(libSource).toObject().value(libSourceConnString).toString();
}

qreal EBase::Project::OdbcSettings::timeout() const
{
    if(!isValid())return 0;
    return mprj.mlibjdoc.object().value(libSource).toObject().value(libSourceTimeout).toDouble(libSourceDefaultTimeout);
}

void EBase::Project::OdbcSettings::setDSN(QString dsn, QString user, QString passwd)
{
    if(!isValid())return;
    QJsonObject doc=mprj.mlibjdoc.object();
    QJsonObject odbc=doc.value(libSource).toObject();
    odbc.remove(libSourceConnString);
    odbc.insert(libSourceDbType,libSourceOdbcType);
    odbc.insert(libSourceDsn,dsn);
    odbc.insert(libSourceUser,user);
    odbc.insert(libSourcePassword,passwd);
    doc.insert(libSource,odbc);
    mprj.mlibjdoc.setObject(doc);
    mprj.mdirty=true;
}

void EBase::Project::OdbcSettings::setConnectionString(QString cs)
{
    if(!isValid())return;
    QJsonObject doc=mprj.mlibjdoc.object();
    QJsonObject odbc=doc.value(libSource).toObject();
    odbc.insert(libSourceConnString,cs);
    odbc.insert(libSourceDbType,libSourceOdbcType);
    odbc.remove(libSourceDsn);
    odbc.remove(libSourceUser);
    odbc.remove(libSourcePassword);
    doc.insert(libSource,odbc);
    mprj.mlibjdoc.setObject(doc);
    mprj.mdirty=true;
}

void EBase::Project::OdbcSettings::setTimeout(qreal t)
{
    if(!isValid() || t<=0.0)return;
    auto doc=mprj.mlibjdoc.object();
    auto src=doc.value(libSource).toObject();
    src.insert(libSourceTimeout,t);
    doc.insert(libSource,src);
    mprj.mlibjdoc.setObject(doc);
    mprj.mdirty=true;
}

EBase::ODBC::ODBCconnectionSettings EBase::Project::OdbcSettings::toDriverSettings() const
{
    bool udsn=useDSN();
    return EBase::ODBC::ODBCconnectionSettings(
        udsn? EBase::ODBC::ODBCconnectionSettings::ConnectionMode::ByDSN
            : EBase::ODBC::ODBCconnectionSettings::ConnectionMode::ByConnectionString,
        udsn? dsn() : connectionString(),
        username(),
        password()
    );
}






// ////////////////////////////
// DB Settings class

bool EBase::Project::DbSettings::useAlternate() const
{
    if(!isValid())return false;
    PRJSET(prj);
    return prj.value(dbAltUse,dbAltUseDefault).toBool();
}

void EBase::Project::DbSettings::setUseAlternate(bool u)
{
    if(!isValid())return;
    PRJSET(prj);
    prj.setValue(dbAltUse,u);
    mprj.mdirty=true;
}

QString EBase::Project::DbSettings::databaseName() const
{
    if(!isValid())return QString();
    PRJSET(prj);
    return prj.value(dbAltDbName).toString();
}

void EBase::Project::DbSettings::setDatabaseName(QString dn)
{
    if(!isValid())return;
    PRJSET(prj);
    prj.setValue(dbAltDbName,dn);
    mprj.mdirty=true;
}

QString EBase::Project::DbSettings::driverName() const
{
    if(!isValid())return QString();
    PRJSET(prj);
    return prj.value(dbAltDriver).toString();
}

EBase::DB::DBInfo::DBType EBase::Project::DbSettings::driverType() const
{
    return DB::DBInfo::driverToType(driverName());
}

bool EBase::Project::DbSettings::setDriver(DB::DBInfo::DBType dbt)
{
    return setDriver(DB::DBInfo::typeToDriver(dbt));
}

bool EBase::Project::DbSettings::setDriver(QString drv)
{
    if(!isValid())return false;
    //check driver type exists
    if(!DB::DBInfo::driverAvailable(drv))return false;
    //set
    PRJSET(prj);
    prj.setValue(dbAltDriver,drv);
    mprj.mdirty=true;
    return true;
}

QString EBase::Project::DbSettings::hostName() const
{
    if(!isValid())return QString();
    PRJSET(prj);
    return prj.value(dbAltHostname).toString();
}

int EBase::Project::DbSettings::portNumber() const
{
    if(!isValid())return -1;
    PRJSET(prj);
    return prj.value(dbAltHostname,-1).toInt();
}

void EBase::Project::DbSettings::setHost(QString host, int port)
{
    if(!isValid())return;
    PRJSET(prj);
    prj.setValue(dbAltHostname,host);
    prj.setValue(dbAltPort,port);
    mprj.mdirty=true;
}

QString EBase::Project::DbSettings::username() const
{
    if(!isValid())return QString();
    PRJSET(prj);
    return prj.value(dbAltUsername).toString();
}

QString EBase::Project::DbSettings::password() const
{
    if(!isValid())return QString();
    PRJSET(prj);
    return prj.value(dbAltPassword).toString();
}

void EBase::Project::DbSettings::setLogin(QString user, QString passwd)
{
    if(!isValid())return;
    PRJSET(prj);
    prj.setValue(dbAltUsername,user);
    prj.setValue(dbAltPassword,passwd);
    mprj.mdirty=true;
}

QString EBase::Project::DbSettings::connectOptions() const
{
    if(!isValid())return QString();
    PRJSET(prj);
    return prj.value(dbAltOptions).toString();
}

void EBase::Project::DbSettings::setConnectOptions(QString cs)
{
    if(!isValid())return;
    PRJSET(prj);
    prj.setValue(dbAltOptions,cs);
    mprj.mdirty=true;
}





// ////////////////////////////
// Library definition

bool EBase::Project::LibraryDefinition::nameIsValid(const QString&n)
{
    //we check against the very strict KLC guideline, a false is simply a warning
    static const QString klcallow{"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_-.,+"};
    for(QChar c:n)
        if(!klcallow.contains(c))
            return false;
    return true;
}


QString EBase::Project::LibraryDefinition::name() const
{
    if(!isValid())return QString();
    return mprj.mlibobj.value(mid).value(libLibName).toString();
}

bool EBase::Project::LibraryDefinition::setName(QString n)
{
    if(!isValid())return false;
    if(!nameIsValid(n))return false;
    mprj.mlibobj[mid].insert(libLibName,n);
    mprj.mdirty=true;
    return true;
}

QString EBase::Project::LibraryDefinition::tableName()const
{
    if(!isValid())return QString();
    return mprj.mlibobj.value(mid).value(libLibTable).toString();
}

void EBase::Project::LibraryDefinition::setTableName(QString tn)
{
    if(!isValid())return;
    mprj.mlibobj[mid].insert(libLibTable,tn);
    mprj.mdirty=true;
}

QString EBase::Project::LibraryDefinition::keyColumn()const
{
    if(!isValid())return QString();
    return mprj.mlibobj.value(mid).value(libLibKeyCol).toString();
}

void EBase::Project::LibraryDefinition::setKeyColumn(QString kc)
{
    if(!isValid())return;
    mprj.mlibobj[mid].insert(libLibKeyCol,kc);
    mprj.mdirty=true;
}

QString EBase::Project::LibraryDefinition::symbolColumn()const
{
    if(!isValid())return QString();
    return mprj.mlibobj.value(mid).value(libLibSymbolCol).toString();
}

void EBase::Project::LibraryDefinition::setSymbolColumn(QString sc)
{
    if(!isValid())return;
    mprj.mlibobj[mid].insert(libLibSymbolCol,sc);
    mprj.mdirty=true;
}

QString EBase::Project::LibraryDefinition::footprintColumn()const
{
    if(!isValid())return QString();
    return mprj.mlibobj.value(mid).value(libLibFootprintCol).toString();
}

void EBase::Project::LibraryDefinition::setFootprintColumn(QString fc)
{
    if(!isValid())return;
    mprj.mlibobj[mid].insert(libLibFootprintCol,fc);
    mprj.mdirty=true;
}


QJsonObject EBase::Project::LibraryDefinition::findField(QString fn)const
{
    if(!isValid())return QJsonObject();
    QJsonArray far=mprj.mlibobj.value(mid).value(libLibFieldArray).toArray();
    for(const auto&fv:far){
        if(!fv.isObject())continue;
        QJsonObject obj=fv.toObject();
        if(obj.value(libLibFieldName).toString()==fn)
            return obj;
    }
    return QJsonObject();
}

QStringList EBase::Project::LibraryDefinition::fieldNames()const
{
    if(!isValid())return QStringList();
    QStringList ret;
    QJsonArray far=mprj.mlibobj.value(mid).value(libLibFieldArray).toArray();
    for(const auto&fv:far){
        if(!fv.isObject())continue;
        ret.append(fv.toObject().value(libLibFieldName).toString());
    }
    return ret;
}

bool EBase::Project::LibraryDefinition::hasField(QString fn)const
{
    if(!isValid())return false;
    QJsonArray far=mprj.mlibobj.value(mid).value(libLibFieldArray).toArray();
    for(const auto&fv:far){
        if(!fv.isObject())continue;
        //found?
        if(fv.toObject().value(libLibFieldName).toString()==fn)
            return true;
    }
    //not found.
    return false;
}

QString EBase::Project::LibraryDefinition::fieldColumn(QString fn)const
{
    return findField(fn).value(libLibFieldColumn).toString();
}

bool EBase::Project::LibraryDefinition::addField(QString name, QString column)
{
    if(name.isEmpty()||column.isEmpty())return false;
    if(!isValid())return false;
    if(hasField(name))return false;
    QJsonArray far=mprj.mlibobj.value(mid).value(libLibFieldArray).toArray();
    QJsonObject field;
    field.insert(libLibFieldName,name);
    field.insert(libLibFieldColumn,column);
    far.append(field);
    mprj.mlibobj[mid].insert(libLibFieldArray,far);
    mprj.mdirty=true;
    return true;
}

bool EBase::Project::LibraryDefinition::removeField(QString name)
{
    if(!isValid())return false;
    QJsonArray far=mprj.mlibobj.value(mid).value(libLibFieldArray).toArray();
    for(int i=0;i<far.size();i++){
        auto f=far[i].toObject();
        if(f.value(libLibFieldName).toString()==name){
            far.removeAt(i);
            mprj.mlibobj[mid].insert(libLibFieldArray,far);
            mprj.mdirty=true;
            return true;
        }
    }
    return false;
}

bool EBase::Project::LibraryDefinition::setFieldColumn(QString name, QString column)
{
    if(column.isEmpty())return false;
    if(!isValid())return false;
    QJsonArray far=mprj.mlibobj.value(mid).value(libLibFieldArray).toArray();
    for(int i=0;i<far.size();i++){
        auto f=far[i].toObject();
        if(f.value(libLibFieldName).toString()==name){
            f.insert(libLibFieldColumn,column);
            far[i]=f;
            mprj.mlibobj[mid].insert(libLibFieldArray,far);
            mprj.mdirty=true;
            return true;
        }
    }
    return false;
}

bool EBase::Project::LibraryDefinition::fieldVisibleOnAdd(QString fn)const
{
    return findField(fn).value(libLibFieldVisibleOnAdd).toBool(libLibFieldVisibleOnAddDefault);
}

bool EBase::Project::LibraryDefinition::fieldVisibleInChooser(QString fn)const
{
    return findField(fn).value(libLibFieldVisibleChooser).toBool(libLibFieldVisibleChooserDefault);
}

bool EBase::Project::LibraryDefinition::setFieldVisibility(QString name, bool visibleOnAdd, bool visibleInChooser)
{
    if(!isValid())return false;
    QJsonArray far=mprj.mlibobj.value(mid).value(libLibFieldArray).toArray();
    for(int i=0;i<far.size();i++){
        auto f=far[i].toObject();
        if(f.value(libLibFieldName).toString()==name){
            f.insert(libLibFieldVisibleOnAdd,visibleOnAdd);
            f.insert(libLibFieldVisibleChooser,visibleInChooser);
            far[i]=f;
            mprj.mlibobj[mid].insert(libLibFieldArray,far);
            mprj.mdirty=true;
            return true;
        }
    }
    return false;
}

bool EBase::Project::LibraryDefinition::fieldShowName(QString fn)const
{
    return findField(fn).value(libLibFieldShowName).toBool(libLibFieldShowNameDefault);
}

bool EBase::Project::LibraryDefinition::setFieldShowName(QString name, bool showname)
{
    if(!isValid())return false;
    QJsonArray far=mprj.mlibobj.value(mid).value(libLibFieldArray).toArray();
    for(int i=0;i<far.size();i++){
        auto f=far[i].toObject();
        if(f.value(libLibFieldName).toString()==name){
            f.insert(libLibFieldShowName,showname);
            far[i]=f;
            mprj.mlibobj[mid].insert(libLibFieldArray,far);
            mprj.mdirty=true;
            return true;
        }
    }
    return false;
}

bool EBase::Project::LibraryDefinition::fieldInheritProperties(QString fn)const
{
    return findField(fn).value(libLibFieldInherit).toBool(libLibFieldInheritDefault);
}

bool EBase::Project::LibraryDefinition::setFieldInheritProperties(QString name, bool inherit)
{
    if(!isValid())return false;
    QJsonArray far=mprj.mlibobj.value(mid).value(libLibFieldArray).toArray();
    for(int i=0;i<far.size();i++){
        auto f=far[i].toObject();
        if(f.value(libLibFieldName).toString()==name){
            f.insert(libLibFieldInherit,inherit);
            far[i]=f;
            mprj.mlibobj[mid].insert(libLibFieldArray,far);
            mprj.mdirty=true;
            return true;
        }
    }
    return false;
}


QString EBase::Project::LibraryDefinition::descriptionColumn()const
{
    if(!isValid())return QString();
    return mprj.mlibobj.value(mid).value(libLibPropertyMap).toObject().value(libLibPropDescription).toString();
}

void EBase::Project::LibraryDefinition::setDescriptionColumn(QString dc)
{
    if(!isValid())return;
    auto prop=mprj.mlibobj[mid].value(libLibPropertyMap).toObject();
    if(dc.isEmpty())
        prop.remove(libLibPropDescription);
    else
        prop.insert(libLibPropDescription,dc);
    mprj.mlibobj[mid].insert(libLibPropertyMap,prop);
    mprj.mdirty=true;
}

QString EBase::Project::LibraryDefinition::keywordsColumn()const
{
    if(!isValid())return QString();
    return mprj.mlibobj.value(mid).value(libLibPropertyMap).toObject().value(libLibPropKeywords).toString();
}

void EBase::Project::LibraryDefinition::setKeywordsColumn(QString kc)
{
    if(!isValid())return;
    auto prop=mprj.mlibobj[mid].value(libLibPropertyMap).toObject();
    if(kc.isEmpty())
        prop.remove(libLibPropKeywords);
    else
        prop.insert(libLibPropKeywords,kc);
    mprj.mlibobj[mid].insert(libLibPropertyMap,prop);
    mprj.mdirty=true;
}

QString EBase::Project::LibraryDefinition::excludeBomColumn()const
{
    if(!isValid())return QString();
    return mprj.mlibobj.value(mid).value(libLibPropertyMap).toObject().value(libLibPropExcludeBOM).toString();
}

void EBase::Project::LibraryDefinition::setExcludeBomColumn(QString bc)
{
    if(!isValid())return;
    auto prop=mprj.mlibobj[mid].value(libLibPropertyMap).toObject();
    if(bc.isEmpty())
        prop.remove(libLibPropExcludeBOM);
    else
        prop.insert(libLibPropExcludeBOM,bc);
    mprj.mlibobj[mid].insert(libLibPropertyMap,prop);
    mprj.mdirty=true;
}

QString EBase::Project::LibraryDefinition::excludeBoardColumn()const
{
    if(!isValid())return QString();
    return mprj.mlibobj.value(mid).value(libLibPropertyMap).toObject().value(libLibPropExcludePCB).toString();
}

void EBase::Project::LibraryDefinition::setExcludeBoardColumn(QString bc)
{
    if(!isValid())return;
    auto prop=mprj.mlibobj[mid].value(libLibPropertyMap).toObject();
    if(bc.isEmpty())
        prop.remove(libLibPropExcludePCB);
    else
        prop.insert(libLibPropExcludePCB,bc);
    mprj.mlibobj[mid].insert(libLibPropertyMap,prop);
    mprj.mdirty=true;
}
