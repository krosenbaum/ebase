// Electric Base App: KiCAD Symbol/Footprint Cache
// (c) Konrad Rosenbaum, 2024
// protected under GNU GPL v.3 or at your option any newer

#pragma once

#include <QObject>
#include <QVariantList>

#include "kicadcache.h"

namespace EBase::Project::Private {

///Caches a single library of symbols or footprints.
class CacheLine
{
    CacheLine()=delete;
    CacheLine(const CacheLine&)=delete;
public:
    ///create cache line from path
    ///\param libpath must be either a KiCAD symbol file or a footprint directory (*.pretty).
    ///\param type tells the cache whether it is symbols or footprints
    CacheLine(QString libpath,KicadCache::LibraryType type);
    ///drop dead
    ~CacheLine();

    ///true if this represents a real library
    bool isValid()const{return mtype!=KicadCache::NoLibrary;}

    ///returns the full path represented by this cache line
    QString path()const{return mpath;}
    ///returns the type of this library
    KicadCache::LibraryType type()const{return mtype;}
    ///returns the (presumed) name of the library - calculated from the file/directory name
    QString libraryName()const{return mname;}
    ///returns the list of symbols or footprints, without the library name prefix
    QStringList libraryContents()
    {
        if(mcontent.size()==0)loadContents();
        return mcontent;
    }
    ///forces the cache line to re-load its contents (reparse the file)
    void reload(){loadContents();}

    ///for KiCAD symbols: returns additional properties like Datasheet or Value
    QString symbolData(QString symbol,QString dataname)
    {
        if(mcontent.size()==0)loadContents();
        return msymdata.value(symbol).value(dataname);
    }
private:
    QString mpath,mname;
    KicadCache::LibraryType mtype;
    QStringList mcontent;
    QMap<QString,QMap<QString,QString>>msymdata;

    ///\internal helper to reparse the file(s)
    void loadContents();
};

///Internal store for KiCAD library caches.
///This is a singleton for efficiency: there is no point in caching the entire KiCAD library multiple times.
class KicadCacheStore : public QObject
{
    Q_OBJECT
    KicadCacheStore();
    ~KicadCacheStore();
public:
    ///Returns the cache singleton.
    static KicadCacheStore& instance();

    ///Adds a single symbol file.
    ///\returns Returns a pointer to the loaded cache for the symbol file.
    std::shared_ptr<CacheLine> addSymbolPath(QString);
    ///Adds a single footprint dir.
    ///\returns Returns a pointer to the loaded cache for the footprint dir.
    std::shared_ptr<CacheLine> addFootprintPath(QString);

private:
    ///maps file names to cache
    QMap<QString,std::shared_ptr<CacheLine>> msymbols,mfootprints;
};



//constants
// symbol file extension
static const QString kicadSymbolExtension   { "kicad_sym"};
static const QString kicadSymbolExtensionDot{".kicad_sym"};

// symbol file internal key words
static const QString kicadSymbolLibToken{"kicad_symbol_lib"};
static const QString kicadSymbolLibSymbolsToken{"symbol"};
static const QString kicadSymbolLibPropsToken{"property"};

// s-expr search pathes for symbol files
static const QStringList kicadSymbolLibPath{"kicad_symbol_lib"};
static const QStringList kicadSymbolLibSymbolsPath{"kicad_symbol_lib","symbol"};
static const QStringList kicadSymbolLibPropsPath{"kicad_symbol_lib","symbol","property"};

// footprint file extensions
static const QString kicadFootprintExtension          {  "kicad_mod"};
static const QString kicadFootprintExtensionDot       { ".kicad_mod"};
static const QString kicadFootprintExtensionPattern   {"*.kicad_mod"};
static const QString kicadFootprintDirExtension       {  "pretty"};
static const QString kicadFootprintDirExtensionDot    { ".pretty"};
static const QString kicadFootprintDirExtensionPattern{"*.pretty"};

// symbol: important property names
static const QString kicadSymbolPropValue       {"Value"};
static const QString kicadSymbolPropDatasheet   {"Datasheet"};
static const QString kicadSymbolPropFootprint   {"Footprint"};
static const QString kicadSymbolPropReference   {"Reference"};
static const QString kicadSymbolPropDescription {"Description"};

//END of namespace
}
