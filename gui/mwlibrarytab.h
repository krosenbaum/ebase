// Electric Base App: Main Window - DB Tab
// (c) Konrad Rosenbaum, 2024
// protected under GNU GPL v.3 or at your option any newer

#pragma once

#include "mwin.h"
#include <memory>

class QListView;
class QTableView;
class QStandardItemModel;
class QLabel;
class QLineEdit;
class QRadioButton;
class QModelIndex;
class QUuid;

namespace EBase {
namespace Project { class LibraryDefinition; }
namespace GUI {

///KiCAD Library Setup Tab of Main Application Window.
class MWLibraryTab : public MainTab
{
    Q_OBJECT
public:
    ///Instantiates tab.
    MWLibraryTab(MainWindow*,Project::Project*,DB::DBAccessor*);
    ///Frees all resources associated with this window. Saves contents.
    ~MWLibraryTab();

    QString tabTitle() const override;
    QString tabToolTip() const override;
    // void configStatusBar(QStatusBar * ) override;

    void setGeometryFromConfig(QString settingsKey) override;
    void saveGeometryToConfig(QString settingsKey) override;

    enum class ColumnType{String,Bool};
private slots:
    void addLibrary();
    void removeLibrary();
    void libraryRowSelected(const QModelIndex&);
    void selectLibrary(const QUuid&);

    void refillLibraryModel();

    void renameLibrary();
    void selectTable();
    void selectColumn(QLineEdit*,ColumnType ct=ColumnType::String,QRadioButton*rbon=nullptr);
private:
    QListView*mliblist;
    QTableView*mfieldtable;
    QStandardItemModel*mlibmodel,*mfieldmodel;
    QLineEdit *mlibname,*mtablename,*mkeycol,*msymcol,*mfprintcol,*mdescrcol,*mkeywordcol,*mexclbomcol,*mexclboardcol;
    QRadioButton*mdescron,*mdescroff,*mkeywordon,*mkeywordoff,*mexclbomon,*mexclbomoff,*mexclboardon,*mexclboardoff;
    QWidget*mright;
    std::unique_ptr<Project::LibraryDefinition>mcurrent;
};

//END of namespace
}}
