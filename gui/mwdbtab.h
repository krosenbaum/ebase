// Electric Base App: Main Window - DB Tab
// (c) Konrad Rosenbaum, 2024
// protected under GNU GPL v.3 or at your option any newer

#pragma once

#include "mwin.h"

class QTreeView;
class QStandardItemModel;
class QTableView;

namespace EBase {
namespace GUI {

///DB Design Tab of Main Application Window.
class MWDatabaseTab : public MainTab
{
    Q_OBJECT
public:
    ///Instantiates tab.
    MWDatabaseTab(MainWindow*,Project::Project*,DB::DBAccessor*);
    ///Frees all resources associated with this window. Saves contents.
    ~MWDatabaseTab();

    QString tabTitle() const override;
    QString tabToolTip() const override;
    // void configStatusBar(QStatusBar * ) override;
    void setGeometryFromConfig(QString settingsKey) override;
    void saveGeometryToConfig(QString settingsKey) override;
protected:
    void dbReload() override;
    void dbAboutToClose() override;
    void dbClosed() override;
private:
    QTreeView *mdbstruct;
    QStandardItemModel*mdbstructmodel;

};

//END of namespace
}}
