// Electric Base App: Main Window - Basics Tab
// (c) Konrad Rosenbaum, 2024
// protected under GNU GPL v.3 or at your option any newer

#pragma once

#include "mwin.h"

class QCheckBox;
class QComboBox;
class QLabel;
class QLineEdit;
class QSpinBox;


namespace EBase {
namespace GUI {

///Basics Tab of Main Application Window.
class MWBasicsTab : public MainTab
{
    Q_OBJECT
public:
    ///Instantiates tab.
    MWBasicsTab(MainWindow*,Project::Project*,DB::DBAccessor*);
    ///Frees all resources associated with this window. Saves contents.
    ~MWBasicsTab();

    QString tabTitle() const override;
    QString tabToolTip() const override;
    void configStatusBar(QStatusBar * ) override;

private slots:
    ///save ODBC settings
    void saveOdbc();
    ///test ODBC connection
    void testOdbc();
    ///chose a new DSN from a list
    void chooseDSN();

public slots:
    ///update DB status widgets
    void updateDbStatus();
    ///open the database
    void openDB(bool showError=true);
    ///close the database
    void closeDB();

signals:
    ///about to close the DB
    void dbAboutToCloseSig();
    ///actually closed the DB
    void dbClosedSig();
    ///opened a new DB
    void dbOpenedSig();

private:
    QLineEdit *modbcdsn,*modbcusr,*modbcpass,*modbccstr,*madbname,*madbusr,*madbpass,*madbhost,*madbopt;
    QCheckBox *modbcusecs;
    QSpinBox *modbctime,*madbport;
    QLabel *mdbstatus=nullptr,*mdbstatonbar=nullptr,*mdbinfo=nullptr,*mdbdialect=nullptr;
    QComboBox *madbtype;

    //GUI construction, helpers for initGui()
    ///helper: create basics tab
    QWidget*createBaseTab();
    ///helper: fill DB types ComboBox
    void fillDbTypes(QString current);

};

//END of GUI namespaces
}}
