// Electric Base App: Icons
// (c) Konrad Rosenbaum, 2024
// protected under GNU GPL v.3 or at your option any newer

#include "icons.h"

#include <QFileInfo>

using namespace EBase::GUI::Icons;

QString EBase::GUI::Icons::iconFileName(const QString&n)
{
    QString fn=baseDir+n;
    for(const auto&x:extensions)
        if(QFileInfo::exists(fn+x))
            return fn+x;
    return QString();
}

QIcon EBase::GUI::Icons::iconByName(const QString& n, EBase::GUI::Icons::IconVariants v)
{
    QIcon ico;
    QString fn;
    switch(v){
        case IconVariants::Normal:
            ico.addFile(iconFileName(n));
            break;
        case IconVariants::State:{
            fn=iconFileName(n);
            if(fn.isEmpty())fn=iconFileName(n+"-on");
            if(!fn.isEmpty())ico.addFile(fn,QSize(),QIcon::Normal,QIcon::On);
            fn=iconFileName(n+"-off");
            if(!fn.isEmpty())ico.addFile(fn,QSize(),QIcon::Normal,QIcon::Off);
            break;
        }
        case IconVariants::Mode:{
            fn=iconFileName(n);
            if(fn.isEmpty())fn=iconFileName(n+"-normal");
            if(!fn.isEmpty())ico.addFile(fn,QSize(),QIcon::Normal);
            fn=iconFileName(n+"-disabled");
            if(!fn.isEmpty())ico.addFile(fn,QSize(),QIcon::Disabled);
            fn=iconFileName(n+"-active");
            if(!fn.isEmpty())ico.addFile(fn,QSize(),QIcon::Active);
            fn=iconFileName(n+"-selected");
            if(!fn.isEmpty())ico.addFile(fn,QSize(),QIcon::Selected);
            break;
        }
    }
    if(ico.isNull())
        ico.addFile(iconFileName(Names::fallback));
    return ico;
}

QPixmap EBase::GUI::Icons::pixmapByName(const QString&n)
{
    return QPixmap(iconFileName(n));
}
