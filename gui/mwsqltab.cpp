// Electric Base App: Main Window - DB Tab
// (c) Konrad Rosenbaum, 2024
// protected under GNU GPL v.3 or at your option any newer

#include "mwsqltab.h"
#include "icons.h"
#include "db/dbaccess.h"

#include <QBoxLayout>
#include <QIcon>
#include <QLabel>
#include <QPushButton>
#include <QSettings>
#include <QSplitter>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlQueryModel>
#include <QTableView>
#include <QTextEdit>

using namespace EBase::GUI;

MWSqlTab::MWSqlTab(MainWindow*mw, Project::Project*prj, DB::DBAccessor*dba)
:MainTab(mw,prj,dba)
{
    auto split=new QSplitter;
    split->setOrientation(Qt::Vertical);
    setWidget(split);
    //TODO!!
    QWidget*w;
    QVBoxLayout*vl;
    QHBoxLayout*hl;
    QPushButton*pb;

    //SQL query input
    split->addWidget(w=new QWidget);
    w->setLayout(vl=new QVBoxLayout);
    vl->addLayout(hl=new QHBoxLayout,0);
    hl->addWidget(new QLabel("SQL Query:"),0);
    hl->addStretch(1);
    vl->addWidget(msqltext=new QTextEdit,1);
    vl->addLayout(hl=new QHBoxLayout,0);
    hl->addStretch(1);
    hl->addWidget(pb=new QPushButton(Icons::iconByName(Icons::Names::sqlClearQuery),tr("Clear Query")),0);
    connect(pb,&QPushButton::clicked,this,&MWSqlTab::clearQuery);
    hl->addSpacing(20);
    hl->addWidget(pb=new QPushButton(Icons::iconByName(Icons::Names::sqlGo),tr("Go!")),0);
    auto font=pb->font();
    font.setBold(true);
    pb->setFont(font);
    connect(pb,&QPushButton::clicked,this,&MWSqlTab::execQuery);

    //SQL query result display
    split->addWidget(w=new QWidget);
    w->setLayout(vl=new QVBoxLayout);
    vl->addLayout(hl=new QHBoxLayout,0);
    hl->addWidget(new QLabel(tr("Result State:")),0);
    hl->addWidget(mresultstate=new QLabel,1);
    mresultstate->setWordWrap(true);
    // hl->addStretch(1);
    vl->addWidget(mresulttable=new QTableView);
    mresulttable->setModel(mresultmodel=new QSqlQueryModel(this));
    mresulttable->setEditTriggers(QAbstractItemView::NoEditTriggers);
    vl->addLayout(hl=new QHBoxLayout,0);
    hl->addStretch(1);
    hl->addWidget(pb=new QPushButton(Icons::iconByName(Icons::Names::sqlClearResult),tr("Clear Result")),0);
    connect(pb,&QPushButton::clicked,this,&MWSqlTab::clearResult);

    //initial load?
    if(dba->isOpen())dbReload();
    else split->setEnabled(false);
}

MWSqlTab::~MWSqlTab()
{
}

QString MWSqlTab::tabTitle() const
{
    return tr("&SQL");
}

QString MWSqlTab::tabToolTip() const
{
    return tr("Direct access to Database with native SQL.");
}

void MWSqlTab::dbAboutToClose()
{
    clearResult();
    widget()->setEnabled(false);
}

void MWSqlTab::dbReload()
{
    widget()->setEnabled(true);
}

void MWSqlTab::clearQuery()
{
    msqltext->clear();
    clearResult();
}

void MWSqlTab::clearResult()
{
    //note: misclear is a hack to prevent double clear after invalidating the DB - it crashes QtSql
    if(!misclear)mresultmodel->clear();
    misclear=true;
    mresultstate->setText(QString());
}

void MWSqlTab::execQuery()
{
    clearResult();
    const QString qtext=msqltext->toPlainText().trimmed();
    if(qtext.isEmpty()){
        mresultstate->setText(tr("Please enter SQL Query!"));
        return;
    }
    mresultmodel->setQuery(qtext,mdba->db());
    misclear=false;//(needed by clearResult)
    auto err=mresultmodel->lastError();
    if(err.isValid()){
        mresultstate->setText(tr("Error: %1").arg(err.text()));
    }else{
        mresulttable->resizeColumnsToContents();
        const auto&q=mresultmodel->query();
        if(q.isSelect())
            mresultstate->setText(tr("Success, %1 rows returned.").arg(q.size()));
        else
            mresultstate->setText(tr("Success, %1 rows affected.").arg(q.numRowsAffected()));
    }
}

void MWSqlTab::saveGeometryToConfig(QString settingsKey)
{
    MainTab::saveGeometryToConfig(settingsKey);
    QSplitter*s=qobject_cast<QSplitter*>(widget());
    if(s)
        QSettings().setValue(settingsKey+"_split",s->saveState());
}

void MWSqlTab::setGeometryFromConfig(QString settingsKey)
{
    MainTab::setGeometryFromConfig(settingsKey);
    QSplitter*s=qobject_cast<QSplitter*>(widget());
    if(s)
        s->restoreState(QSettings().value(settingsKey+"_split").toByteArray());
}
