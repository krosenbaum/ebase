// Electric Base App: Main Window
// (c) Konrad Rosenbaum, 2024
// protected under GNU GPL v.3 or at your option any newer

#include <QApplication>
#include <QDesktopServices>
#include <QFileDialog>
#include <QMenu>
#include <QMenuBar>
#include <QMessageBox>
#include <QSettings>
#include <QStatusBar>
#include <QTabWidget>

#include "mwin.h"
#include "mwbasetab.h"
#include "mwdbtab.h"
#include "mwlibrarytab.h"
#include "mwformstab.h"
#include "mwsqltab.h"
#include "odbcgui.h"
#include "icons.h"
#include "db/odbcdrv.h"
#include "db/dbaccess.h"
#include "project/project.h"

using namespace EBase;
using namespace EBase::GUI;
using namespace EBase::GUI::Private;

// ////////////////////////////
// Notifier class

MainWindowNotifier* MainWindowNotifier::inst=nullptr;

MainWindowNotifier::MainWindowNotifier()
{
    qDebug()<<"created MainWindow notifier.";
    connect(qApp,&QCoreApplication::aboutToQuit,this,&QObject::deleteLater);
}

MainWindowNotifier::~MainWindowNotifier()
{
    inst=nullptr;
    qDebug()<<"cleaning up MainWindow notifier.";
}

MainWindowNotifier * MainWindowNotifier::instance()
{
    if(inst==nullptr)inst=new MainWindowNotifier;
    return inst;
}



// ////////////////////////////
// Main Window construction

//QSettings keys for geometry info
static const QString geoMainWindowKey{"geo/MainWindow"};
static const QString geoMWBasicsTabKey{"geo/MainWindow_Basics"};
static const QString geoMWLibraryTabKey{"geo/MainWindow_Library"};
static const QString geoMWFormsTabKey{"geo/MainWindow_Forms"};
static const QString geoMWDatabaseTabKey{"geo/MainWindow_DB"};
static const QString geoMWSqlTabKey{"geo/MainWindow_SQL"};

MainWindow::MainWindow(Project::Project*prj)
:mproject(prj),mdba(new DB::DBAccessor(this))
{
    QMainWindow::setAttribute(Qt::WA_DeleteOnClose,true);
    auto*mb=menuBar();
    auto*m=mb->addMenu(tr("&Project"));
    m->addAction(tr("&New Project..."),this,&MainWindow::newProject);
    m->addAction(tr("&Open Project..."),this,&MainWindow::openProject);
    mrecentfiles=m->addMenu(tr("Open &Recent"));
    m->addSeparator();
    m->addAction(tr("&Save"),this,&MainWindow::save);
    m->addSeparator();
    m->addAction(tr("&Close"),this,&MainWindow::close);

    m=mb->addMenu(tr("&ODBC"));
    m->addAction(tr("&Driver Info..."),this,&MainWindow::odbcDrvInfo);
    m->addAction(tr("Data &Sources..."),this,&MainWindow::odbcDsnInfo);
    m->addAction(tr("Data Source &Wizard..."),this,&MainWindow::odbcDsnWizard);

    m=mb->addMenu(tr("&Help"));
    m->addAction(tr("&Help Index"),this,&MainWindow::help);
    m->addSeparator();
    m->addAction(tr("&About EBase"),this,&MainWindow::about);
    m->addAction(tr("About Oxygen &Icons"),this,&MainWindow::aboutOxygen);
    m->addAction(tr("About &Qt"),qApp,&QApplication::aboutQt);
    m->addSeparator();
    m->addAction(tr("&ODBC Version"),this,&MainWindow::odbcVersion);
    m->addAction(tr("Qt &Database Plugins"),this,&MainWindow::dbPluginInfo);

    statusBar()->setSizeGripEnabled(true);

    restoreGeometry(QSettings().value(geoMainWindowKey).toByteArray());
    updateTitle();
    if(mproject)initGui();

    connect(MainWindowNotifier::instance(),&MainWindowNotifier::recentChanged,this,&MainWindow::recentChanged);
    recentChanged();
}

void MainWindow::initGui()
{
    QWidget*w;
    if((w=centralWidget())!=nullptr)
        w->deleteLater();
    if(mproject.isNull() || !mproject->isValid())return;

    QTabWidget*tab=new QTabWidget;
    setCentralWidget(tab);
    tab->setTabPosition(QTabWidget::South);

    //base tab
    mbasetab=new MWBasicsTab(this,mproject,mdba);
    int tn=tab->addTab(mbasetab->widget(),mbasetab->tabTitle());
    tab->setTabToolTip(tn,mbasetab->tabToolTip());
    mbasetab->setGeometryFromConfig(geoMWBasicsTabKey);

    //DB tables/views tab
    mdbtab=new MWDatabaseTab(this,mproject,mdba);
    tn=tab->addTab(mdbtab->widget(),mdbtab->tabTitle());
    tab->setTabToolTip(tn,mdbtab->tabToolTip());
    mdbtab->setGeometryFromConfig(geoMWDatabaseTabKey);

    //library config tab
    mlibtab=new MWLibraryTab(this,mproject,mdba);
    tn=tab->addTab(mlibtab->widget(),mlibtab->tabTitle());
    tab->setTabToolTip(tn,mlibtab->tabToolTip());
    mlibtab->setGeometryFromConfig(geoMWLibraryTabKey);

    //forms using tab
    mformtab=new MWFormsTab(this,mproject,mdba);
    tn=tab->addTab(mformtab->widget(),mformtab->tabTitle());
    tab->setTabToolTip(tn,mformtab->tabToolTip());
    mformtab->setGeometryFromConfig(geoMWDatabaseTabKey);

    //sql query tab
    msqltab=new MWSqlTab(this,mproject,mdba);
    tn=tab->addTab(msqltab->widget(),msqltab->tabTitle());
    tab->setTabToolTip(tn,msqltab->tabToolTip());
    msqltab->setGeometryFromConfig(geoMWSqlTabKey);

    //Status Bar gadgets
    auto *sb=statusBar();
    mbasetab->configStatusBar(sb);
    mdbtab->configStatusBar(sb);
    mlibtab->configStatusBar(sb);
    mformtab->configStatusBar(sb);
    msqltab->configStatusBar(sb);

    //init DB
    mbasetab->openDB(false);
}


MainWindow::~MainWindow()
{
    emit aboutToClose();
    if(mbasetab)mbasetab->closeDB();
    //save();//automatically saved by Project destructor.
    mproject=nullptr;//protect from accidental changes
    QSettings().setValue(geoMainWindowKey,saveGeometry());//remember window size
}

// ////////////////////////////
// New/Open/Save

void MainWindow::newProject()
{
    QFileDialog fd(this,tr("New Project"),QString(),Project::Project::fileDialogPattern());
    fd.setFileMode(QFileDialog::AnyFile);
    fd.setAcceptMode(QFileDialog::AcceptSave);
    fd.setDefaultSuffix(Project::Project::projectExtension());
    if(fd.exec()!=QDialog::Accepted)return;
    const auto fnames=fd.selectedFiles();
    if(fnames.size()<1)return;
    //open project
    openProjectTool(fnames.at(0),true);
}

void MainWindow::openProject()
{
    QFileDialog fd(this,tr("Open Project"),QString(),Project::Project::fileDialogPattern());
    fd.setFileMode(QFileDialog::ExistingFile);
    fd.setAcceptMode(QFileDialog::AcceptOpen);
    fd.setDefaultSuffix(Project::Project::projectExtension());
    if(fd.exec()!=QDialog::Accepted)return;
    const auto fnames=fd.selectedFiles();
    if(fnames.size()<1)return;
    //open project
    openProjectTool(fnames.at(0),false);
}

void MainWindow::openProjectFile(QString fn)
{
    openProjectTool(fn,false);
}

void MainWindow::openProjectTool(QString fname, bool openNew)
{
    //open project
    auto*prj=new Project::Project(fname,
        (openNew ? Project::Project::OpenMode::ReCreateNew : Project::Project::OpenMode::OpenExisting),
        this);
    //if this is open: fail to lock handler
    if(openNew==false && !prj->isValid()){
        const QString linf=prj->lockInfo();
        delete prj;
        if(linf.isEmpty()){
            qDebug()<<"Unable to open project.";
            QMessageBox::warning(this,tr("Warning"),tr("Unable to open project."));
            return;
        }
        if(QMessageBox::question(this,tr("Project Locked"),tr("This project is already locked. Lock Info:\n%1\n\nOverride lock and open anyway?").arg(linf))!=QMessageBox::Yes)return;
        //try again
        prj=new Project::Project(fname,Project::Project::OpenMode::OpenExistingForce,this);
    }
    //check whether it worked
    if(!prj->isValid()){
        const QString linf=prj->lockInfo();
        delete prj;
        if(linf.isEmpty()){
            qDebug()<<"Project not valid. Try again.";
            QMessageBox::warning(this,tr("Warning"),tr("Unable to open project."));
        }else{
            qDebug()<<"Project not valid. Try again. Lock Info:"<<linf;
            QMessageBox::warning(this,tr("Warning"),tr("Unable to open project - already locked:\n%1").arg(linf));
        }
        return;
    }
    //current window or new one?
    if(mproject){
        //already have a project - open new window
        auto nw=new MainWindow(prj);
        prj->setParent(nw);
        nw->show();
        nw->registerRecent();
    }else{
        //no project yet - take it
        mproject=prj;
        updateTitle();
        initGui();
        registerRecent();
    }
}

void MainWindow::save()
{
    if(mproject)mproject->save();
}

void MainWindow::updateTitle()
{
    if(mproject)
        setWindowTitle(tr("Project: %1").arg(mproject->projectFileName()));
    else
        setWindowTitle(tr("Electric Base"));
}

static const QString recentFilesKey{"MainWindow/recents"};
static const int recentFilesLength{10};

void MainWindow::registerRecent()
{
    if(mproject.isNull() || !mproject->isValid())return;
    const auto fn=mproject->projectFileName();
    if(fn.isEmpty())return;
    QSettings set;
    QStringList rc=set.value(recentFilesKey).toStringList();
    if(rc.contains(fn))rc.removeAll(fn);
    while(rc.size()>=recentFilesLength)rc.removeLast();
    rc.prepend(fn);
    set.setValue(recentFilesKey,rc);
    emit MainWindowNotifier::instance()->recentChanged();
}

void MainWindow::recentChanged()
{
    if(mrecentfiles==nullptr)return;
    mrecentfiles->clear();
    for(QString f:QSettings().value(recentFilesKey).toStringList())
        mrecentfiles->addAction(f,this,std::bind(&MainWindow::openProjectFile,this,f));
}




// ////////////////////////////
// ODBC helpers

void MainWindow::odbcDrvInfo()
{
    OdbcDriverList dl(this);
    dl.exec();
}

void MainWindow::odbcDsnInfo()
{
    OdbcDsn od(this);
    od.exec();
}

void MainWindow::odbcDsnWizard()
{
    //TODO
}


// ////////////////////////////
// Help functions

void MainWindow::help()
{
    //candidate directories
    static const QStringList fcand{
        //relative dirs of the executable in normal installed mode
        "../share/doc/ebase/index.html",
        //relative dirs of the executable in build mode
        "doc/index.html","../doc/index.html",
        //Linux and Unix
#if defined(Q_OS_LINUX) || defined(Q_OS_UNIX)
        "/usr/share/doc/ebase/index.html","/usr/local/share/doc/ebase/index.html","/opt/share/doc/ebase/index.html",
#endif
    };
    //application dir for relative pathes
    static const QString appPath=QApplication::applicationDirPath()+"/";
    //find help file
    for(const auto&path:fcand){
        QString hpath;
        if(QFileInfo(path).isAbsolute())hpath=path;
        else hpath=appPath+path;
        if(QFileInfo::exists(hpath)){
            //try to launch browser
            if(!QDesktopServices::openUrl(QUrl::fromLocalFile(appPath+path)))
                QMessageBox::warning(this,tr("Warning"),tr("Unable to launch Browser."));
            return;
        }
    }
    //not found
    QMessageBox::warning(this,tr("Warning"),tr("Unable to find Help files. Sorry."));
}

void MainWindow::about()
{
    QMessageBox::about(this,
        tr("About Electric Base"),
        tr("<h1>About</h1>"
           "<p>The Electric Base - a database construction utility for <a href=\"https://www.kicad.org\">KiCAD</a>.</p>"
           "<p>&copy; Konrad Rosenbaum, 2024.<br/>protected under the GNU GPL v.3 or at your option any newer.</p>"
        )
    );
    //TODO: add parameters for year and current (GIT) version
}

void MainWindow::aboutOxygen()
{
    QMessageBox about(this);
    about.setWindowTitle(tr("About Oxygen Icons"));
    about.setText(tr("<h1>About</h1>"
           "<p>Most icons in Electric Base are from the <a href=\"https://invent.kde.org/frameworks/oxygen-icons\">KDE Oxygen Icon Theme</a>.</p>"
           "<p>&copy; Oxygen Team, 2007.<br/>protected under the GNU LGPL v.3 or at your option any newer.</p>"
        ));
    about.setStandardButtons(QMessageBox::Ok);
    about.setIconPixmap(Icons::pixmapByName(Icons::Names::oxygen));
    about.exec();
}

void MainWindow::odbcVersion()
{
    QMessageBox::information(this,tr("ODBC Info"),
                             tr("This application was built with ODBC version %1.")
                             .arg(ODBC::ODBCInterface::odbcVersion()));
}

void MainWindow::dbPluginInfo()
{
    QString dblist;
    for(const auto&inf:DB::DBInfo::allAvailable()){
        dblist+= tr("<li>%1: %2 (%3)</li>","line for DB plugin info")
            .arg(inf.driver())
            .arg(inf.humanReadableType())
            .arg(inf.isSupported() ? tr("fully supported","DB plugin: 'supported' stringlet") : tr("may be unsupported","DB plugin: 'unsupported' stringlet"))
            ;
    }

    QMessageBox::information(this,tr("DB Plugins"),tr("<html>Available Database Plugins:<br/><ul>%1</ul>").arg(dblist));
}






// ////////////////////////////
// MainWindow Tab Base

MainTab::MainTab(MainWindow* parent_, Project::Project* project, DB::DBAccessor* dbaccess)
:QObject(parent_),mmainwin(parent_),mproject(project),mdba(dbaccess)
{
    connect(parent_,&MainWindow::aboutToClose,this,&MainTab::aboutToClose);
    connect(parent_,&MainWindow::dbAboutToClose,this,&MainTab::dbAboutToClose);
    connect(parent_,&MainWindow::dbClosed,this,&MainTab::dbClosed);
    connect(parent_,&MainWindow::dbReload,this,&MainTab::dbReload);
}

MainTab::~MainTab()
{
    aboutToClose();
}

void MainTab::setGeometryFromConfig(QString settingsKey)
{
    if(settingsKey.isEmpty())return;
    if(mwidget)mwidget->restoreGeometry(QSettings().value(settingsKey).toByteArray());
    mgeokey=settingsKey;
}

void MainTab::saveGeometryToConfig(QString settingsKey)
{
    if(mwidget)
        QSettings().setValue(settingsKey,mwidget->saveGeometry());
}

void MainTab::aboutToClose()
{
    if(!mgeokey.isEmpty() && mwidget){
        saveGeometryToConfig(mgeokey);
        mgeokey.clear();
    }
}
