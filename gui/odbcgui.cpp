// Electric Base App: ODBC GUI
// (c) Konrad Rosenbaum, 2024
// protected under GNU GPL v.3 or at your option any newer

#include <QBoxLayout>
#include <QHeaderView>
#include <QIcon>
#include <QLabel>
#include <QMessageBox>
#include <QPixmap>
#include <QPushButton>
#include <QSettings>
#include <QStandardItemModel>
#include <QTabWidget>
#include <QTableView>
#include <QTextBrowser>

#include <functional>

#include "odbcgui.h"
#include "icons.h"

#include "db/odbcdrv.h"

using namespace EBase;
using namespace EBase::GUI;
using namespace EBase::ODBC;

// ////////////////////////////
// DSN List Window

OdbcDsn::OdbcDsn(QWidget* parent)
:QDialog(parent)
{
    setWindowTitle(tr("ODBC Data Source List"));

    QVBoxLayout*ml=new QVBoxLayout;
    setLayout(ml);
    QTabWidget*tab;
    ml->addWidget(tab=new QTabWidget,1);
    tab->addTab(mglbdsntable=new QTableView,tr("&Global DSNs"));
    mglbdsntable->setModel(mglbdsnmodel=new QStandardItemModel(this));
    mglbdsntable->setEditTriggers(QAbstractItemView::NoEditTriggers);
    tab->addTab(musrdsntable=new QTableView,tr("&User DSNs"));
    musrdsntable->setModel(musrdsnmodel=new QStandardItemModel(this));
    musrdsntable->setEditTriggers(QAbstractItemView::NoEditTriggers);

    QHBoxLayout*hl;
    ml->addLayout(hl=new QHBoxLayout,0);
    hl->addStretch(1);
    QPushButton*pb;
    hl->addWidget(pb=new QPushButton(tr("&Add DSN...")),0);
    connect(pb,&QPushButton::clicked,this,&OdbcDsn::addDsn);
    // connect(pb,&QPushButton::clicked,tab,std::bind(&QTabWidget::setCurrentWidget,tab,musrdsntable));
    hl->addSpacing(15);
    hl->addWidget(pb=new QPushButton(tr("&Close")),0);
    connect(pb,&QPushButton::clicked,this,&QDialog::accept);

    fillDSNs();
    mglbdsntable->resizeColumnsToContents();
    setSizeGripEnabled(true);

    restoreGeometry(QSettings().value("geo/OdbcDsn").toByteArray());
}

OdbcDsn::~OdbcDsn()
{
    QSettings().setValue("geo/OdbcDsn",saveGeometry());
}


void OdbcDsn::addDsn()
{
}

void OdbcDsn::fillDSNs()
{
    auto&odbc=ODBCInterface::instance();

    auto infos=odbc.dsnInfos(ODBCInterface::DSNclass::System);
    mglbdsnmodel->clear();
    if(mglbdsnmodel->columnCount()<2)
        mglbdsnmodel->insertColumns(0,2);
    mglbdsnmodel->setHorizontalHeaderLabels(QStringList()<<tr("DSN")<<tr("Description"));
    mglbdsnmodel->insertRows(0,infos.size());
    int row=0;
    for(const auto&info:infos){
        mglbdsnmodel->setData(mglbdsnmodel->index(row,0),info.dataSourceName());
        mglbdsnmodel->setData(mglbdsnmodel->index(row,1),info.description());
        row++;
    }
    mglbdsntable->resizeColumnsToContents();

    infos=odbc.dsnInfos(ODBCInterface::DSNclass::User);
    musrdsnmodel->clear();
    if(musrdsnmodel->columnCount()<2)
        musrdsnmodel->insertColumns(0,2);
    musrdsnmodel->setHorizontalHeaderLabels(QStringList()<<tr("DSN")<<tr("Description"));
    musrdsnmodel->insertRows(0,infos.size());
    row=0;
    for(const auto&info:infos){
        musrdsnmodel->setData(musrdsnmodel->index(row,0),info.dataSourceName());
        musrdsnmodel->setData(musrdsnmodel->index(row,1),info.description());
        row++;
    }
    musrdsntable->resizeColumnsToContents();
}

// ////////////////////////////
// DSN Create Wizard


// ////////////////////////////
// Driver List Window

static inline QString map2str(const QMap<QString,QString>&map)
{
    QString ret;
    for(const auto&key:map.keys())
        ret+=key + " = " + map[key] + "\n";
    return ret;
}

OdbcDriverList::OdbcDriverList(QWidget* parent)
:QDialog(parent)
{
    setWindowTitle(tr("ODBC Driver List"));
    auto infos=ODBCInterface::instance().driverInfos();
    QVBoxLayout*vl;
    setLayout(vl=new QVBoxLayout);
    auto*tab=new QTableView;
    auto*mod=new QStandardItemModel(infos.size(),2,this);
    vl->addWidget(tab,1);
    tab->setModel(mod);
    tab->setEditTriggers(QAbstractItemView::NoEditTriggers);
    tab->setSelectionMode(QAbstractItemView::NoSelection);
    int row=0;
    for(const auto&info:infos){
        mod->setData(mod->index(row,0),info.driverName());
        mod->setData(mod->index(row,1),info.description());
        mod->setData(mod->index(row,1),map2str(info.attributes()),Qt::UserRole);
        row++;
    }
    mod->setHorizontalHeaderLabels(QStringList()<<tr("Driver Name")<<tr("Description"));
    connect(tab,&QTableView::doubleClicked,tab,[=,this](const QModelIndex&cidx){
        const QString dn=mod->data(mod->index(cidx.row(),0)).toString();
        const QString attr=mod->data(mod->index(cidx.row(),1),Qt::UserRole).toString();
        QMessageBox::information(this,tr("Driver Info"),tr("Driver Name: %1\n\nAttributes:\n%2").arg(dn).arg(attr));
    });
    tab->resizeColumnsToContents();

    QHBoxLayout*hl;
    vl->addLayout(hl=new QHBoxLayout,0);
    hl->addStretch(1);
    QPushButton*pb;
    hl->addWidget(pb=new QPushButton(tr("&Close")));
    connect(pb,&QPushButton::clicked,this,&QDialog::accept);

    setSizeGripEnabled(true);

    restoreGeometry(QSettings().value("geo/OdbcDriverList").toByteArray());
}

OdbcDriverList::~OdbcDriverList()
{
    QSettings().setValue("geo/OdbcDriverList",saveGeometry());
}



// ////////////////////////////
// ODBC connection test window

OdbcConnectionTest::OdbcConnectionTest(const EBase::ODBC::ODBCconnectionResult&result, QWidget* parent)
{
    setWindowTitle(tr("ODBC Connection Test"));

    QVBoxLayout*vl;
    QHBoxLayout*hl;
    QTabWidget*tab;
    QPushButton*pb;
    QLabel*lab;
    QWidget*w;
    QTextBrowser*txbr;
    QTableView*tview;
    QStandardItemModel*model;

    setLayout(vl=new QVBoxLayout);
    vl->addWidget(tab=new QTabWidget,1);
    vl->addSpacing(10);
    vl->addLayout(hl=new QHBoxLayout,0);
    hl->addStretch(1);
    hl->addWidget(pb=new QPushButton(tr("&Close")),0);
    connect(pb,&QPushButton::clicked,this,&QDialog::accept);

    tab->addTab(w=new QWidget,tr("Result"));
    w->setLayout(vl=new QVBoxLayout);
    vl->addLayout(hl=new QHBoxLayout,0);
    hl->addWidget(lab=new QLabel,0);
    lab->setPixmap(Icons::iconByName(result.successfullyConnected() ? Icons::Names::dialogOkGreen : Icons::Names::dialogError) .pixmap(QSize(150,150)));
    hl->addWidget(lab=new QLabel,1);
    lab->setText(result.successfullyConnected()?
        tr("<html><h1 style='color: green'>Successfully Connected.</h1>"):
        tr("<html><h1 style='color: red'>%1</h1>").arg(returnCodeStr((int)result.returnCode())));

    if(result.hasErrors()){
        vl->addSpacing(10);
        vl->addWidget(new QLabel(tr("Errors:")),0);
        vl->addWidget(txbr=new QTextBrowser,1);
        QString s;
        for(const auto &e:result.errorRecords()){
            s+=e.toString()+"\n";
        }
        txbr->setPlainText(s);
    }else{
        vl->addStretch(1);
    }

    if(result.hasTables()){
        tab->addTab(tview=new QTableView,tr("Tables"));
        tview->setModel(model=new QStandardItemModel(tview));
        tview->setEditTriggers(QAbstractItemView::NoEditTriggers);
        tview->setSelectionMode(QAbstractItemView::NoSelection);
        model->insertColumns(0,5);
        model->setHorizontalHeaderLabels(QStringList()
            <<tr("Catalog")<<tr("Schema")<<tr("Table Name")<<tr("Table Type")<<tr("Remarks")
        );
        auto tables=result.tables();
        model->insertRows(0,tables.size());
        tview->verticalHeader()->hide();
        int row=0;
        for(const auto &t:tables){
            model->setData(model->index(row,0),t.catalog());
            model->setData(model->index(row,1),t.schema());
            model->setData(model->index(row,2),t.tableName());
            model->setData(model->index(row,3),t.tableType());
            model->setData(model->index(row,4),t.remarks());
            row++;
        }
    }

    if(result.hasViews()){
        tab->addTab(tview=new QTableView,tr("Views"));
        tview->setModel(model=new QStandardItemModel(tview));
        tview->setEditTriggers(QAbstractItemView::NoEditTriggers);
        tview->setSelectionMode(QAbstractItemView::NoSelection);
        model->insertColumns(0,5);
        model->setHorizontalHeaderLabels(QStringList()
            <<tr("Catalog")<<tr("Schema")<<tr("View Name")<<tr("View Type")<<tr("Remarks")
        );
        auto tables=result.views();
        model->insertRows(0,tables.size());
        tview->verticalHeader()->hide();
        int row=0;
        for(const auto &t:tables){
            model->setData(model->index(row,0),t.catalog());
            model->setData(model->index(row,1),t.schema());
            model->setData(model->index(row,2),t.tableName());
            model->setData(model->index(row,3),t.tableType());
            model->setData(model->index(row,4),t.remarks());
            row++;
        }
    }

    setSizeGripEnabled(true);

    restoreGeometry(QSettings().value("geo/OdbcConnectionTest").toByteArray());
}

OdbcConnectionTest::~OdbcConnectionTest()
{
    QSettings().setValue("geo/OdbcConnectionTest",saveGeometry());
}

QString OdbcConnectionTest::returnCodeStr(int rc)
{
    switch(EBase::ODBC::ODBCconnectionResult::ReturnCode(rc)){
        case ODBC::ODBCconnectionResult::ReturnCode::Successful:return tr("Success!");
        case ODBC::ODBCconnectionResult::ReturnCode::NotTriedYet:return tr("Not tried yet.");
        case ODBC::ODBCconnectionResult::ReturnCode::NoEnvironment:return tr("Unable: no ODBC Environment allocated.");
        case ODBC::ODBCconnectionResult::ReturnCode::InvalidSettings:return tr("Invalid ODBC Settings.");
        case ODBC::ODBCconnectionResult::ReturnCode::ConnectFailed:return tr("Connection Attempt Failed.");
    }
    Q_UNREACHABLE();
    qDebug()<<"Whoopsie. OdbcConnectionTest::returnCodeStr forgot to handle return code "<<rc<<"- need more sleep.";
    return QString::number(rc);
}
