// Electric Base App: Main Window - DB Tab
// (c) Konrad Rosenbaum, 2024
// protected under GNU GPL v.3 or at your option any newer

#pragma once

#include "mwin.h"

class QTreeView;
class QStandardItemModel;
class QTableView;
class QTextEdit;
class QSqlQueryModel;
class QLabel;

namespace EBase {
namespace GUI {

///SQL Query Tab of Main Application Window.
class MWSqlTab : public MainTab
{
    Q_OBJECT
public:
    ///Instantiates tab, creates widget
    MWSqlTab(MainWindow*,Project::Project*,DB::DBAccessor*);
    ///Frees all resources associated with this window. Saves contents.
    ~MWSqlTab();

    QString tabTitle() const override;
    QString tabToolTip() const override;
    void setGeometryFromConfig(QString settingsKey) override;
    void saveGeometryToConfig(QString settingsKey) override;
protected:
    void dbAboutToClose() override;
    void dbReload() override;
private slots:
    void execQuery();
    void clearQuery();
    void clearResult();
private:
    QTextEdit*msqltext;
    QTableView*mresulttable;
    QSqlQueryModel*mresultmodel;
    QLabel*mresultstate;
    bool misclear=false;
};

//END of namespace
}}
