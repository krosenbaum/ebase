// Electric Base App: Icons
// (c) Konrad Rosenbaum, 2024
// protected under GNU GPL v.3 or at your option any newer

#pragma once

#include <QIcon>
#include <QStringList>
#include <QPixmap>

///Namespace containing convenience functions and constants for common Icons.
namespace EBase::GUI::Icons {

    static const QStringList extensions{".svgz",".png"};
    static const QString baseDir{":/images/"};

    ///Convenience constants for icon file names. These can be used with \ref iconFileName , \ref iconByName and \ref pixmapByName.
    namespace Names {
        static const QString arrowDown{"arrow-down"};
        static const QString arrowLeft{"arrow-left"};
        static const QString arrowRight{"arrow-right"};
        static const QString arrowUp{"arrow-up"};
        static const QString connectDB{"connect-db"};
        static const QString database{"database"};
        static const QString dialogCancel{"dialog-cancel"};
        static const QString dialogError{"dialog-error"};
        static const QString dialogInformation{"dialog-information"};
        static const QString dialogOkBlue{"dialog-ok-blue"};
        static const QString dialogOkGreen{"dialog-ok-green"};
        static const QString editRename{"edit-rename"};
        static const QString edit{"edit"};
        static const QString fallback{"fallback"};
        static const QString listAdd{"list-add"};
        static const QString listRemove{"list-remove"};
        static const QString oxygen{"oxygen"};
        static const QString selectFromList{"select-from-list"};
        static const QString sqlClearQuery{"sql-clear-query"};
        static const QString sqlClearResult{"sql-clear-result"};
        static const QString sqlGo{"sql-go"};
        static const QString testODBC{"test-odbc"};
        static const QString visibleOff{"visible-off"};
        static const QString visibleOn{"visible-on"};
        static const QString visible{"visible"};
    }

    ///Variations that are attempted to be loaded
    enum class IconVariants {
        ///load only the normal icon, no variations
        Normal,
        ///load normal/disabled/active/selected mode icons
        Mode,
        ///load on and off state icons
        State
    };

    ///Attempt to find the icon file name (does not find variations).
    QString iconFileName(const QString&);
    ///Return an icon by its name (see \ref Names namespace).
    ///\param n name of the icon - use one of the Names::* constants
    ///\param v hint which variants should be loaded
    QIcon iconByName(const QString&n,IconVariants v=IconVariants::Normal);
    ///Return a pixmap by its name.
    QPixmap pixmapByName(const QString&n);


//END of namespace
}
