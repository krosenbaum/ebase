// Electric Base App: Main Window - DB Tab
// (c) Konrad Rosenbaum, 2024
// protected under GNU GPL v.3 or at your option any newer

#include <QBoxLayout>
#include <QButtonGroup>
#include <QFormLayout>
#include <QIcon>
#include <QInputDialog>
#include <QLabel>
#include <QLineEdit>
#include <QListView>
#include <QMessageBox>
#include <QPushButton>
#include <QRadioButton>
#include <QSettings>
#include <QSplitter>
#include <QSqlDatabase>
#include <QStandardItemModel>
#include <QTableView>

#include "mwlibrarytab.h"
#include "icons.h"
#include "project/project.h"
#include "db/dbaccess.h"
#include "db/dbdialect.h"

using namespace EBase::GUI;

MWLibraryTab::MWLibraryTab(MainWindow*mw, Project::Project*prj, DB::DBAccessor*dba)
:MainTab(mw,prj,dba)
{
    QSplitter *split=new QSplitter;
    QWidget*w;
    QHBoxLayout*hl;
    QVBoxLayout*vl;
    QFormLayout*fl;
    QPushButton*pb;
    QButtonGroup*bgrp;
    split->addWidget(w=new QWidget);
    w->setLayout(vl=new QVBoxLayout);
    vl->addWidget(mliblist=new QListView,1);
    mliblist->setModel(mlibmodel=new QStandardItemModel(this));
    mliblist->setModelColumn(0);
    mliblist->setEditTriggers(QAbstractItemView::NoEditTriggers);
    mliblist->setSelectionMode(QAbstractItemView::NoSelection);
    connect(mliblist,&QListView::doubleClicked,this,&MWLibraryTab::libraryRowSelected);
    vl->addLayout(hl=new QHBoxLayout,0);
    hl->addStretch(1);
    hl->addWidget(pb=new QPushButton(Icons::iconByName(Icons::Names::listAdd),tr("Add Library...")));
    connect(pb,&QPushButton::clicked,this,&MWLibraryTab::addLibrary);
    hl->addWidget(pb=new QPushButton(Icons::iconByName(Icons::Names::listRemove),tr("Remove Library")));
    connect(pb,&QPushButton::clicked,this,&MWLibraryTab::removeLibrary);

    split->addWidget(mright=w=new QWidget);
    w->setLayout(vl=new QVBoxLayout);
    vl->addLayout(fl=new QFormLayout,0);
    fl->addRow(tr("Library Name:"),hl=new QHBoxLayout);
    hl->addWidget(mlibname=new QLineEdit,1);
    mlibname->setReadOnly(true);
    hl->addWidget(pb=new QPushButton(Icons::iconByName(Icons::Names::editRename),tr("Rename...")),0);
    connect(pb,&QPushButton::clicked,this,&MWLibraryTab::renameLibrary);

    fl->addRow(tr("Table or View:"),hl=new QHBoxLayout);
    hl->addWidget(mtablename=new QLineEdit,1);
    auto selectIcon=Icons::iconByName(Icons::Names::selectFromList);
    hl->addWidget(pb=new QPushButton(selectIcon,tr("Select...")),0);
    connect(pb,&QPushButton::clicked,this,&MWLibraryTab::selectTable);

    fl->addRow(tr("Key/Part Column:"),hl=new QHBoxLayout);
    hl->addWidget(mkeycol=new QLineEdit,1);
    connect(mkeycol,&QLineEdit::textChanged,this,[this](const QString&t){mcurrent->setKeyColumn(t);});
    hl->addWidget(pb=new QPushButton(selectIcon,tr("Select...")),0);
    connect(pb,&QPushButton::clicked,this,std::bind(&MWLibraryTab::selectColumn,this,mkeycol,ColumnType::String,nullptr));

    fl->addRow(tr("Symbol Column:"),hl=new QHBoxLayout);
    hl->addWidget(msymcol=new QLineEdit,1);
    connect(msymcol,&QLineEdit::textChanged,this,[this](const QString&t){mcurrent->setSymbolColumn(t);});
    hl->addWidget(pb=new QPushButton(selectIcon,tr("Select...")),0);
    connect(pb,&QPushButton::clicked,this,std::bind(&MWLibraryTab::selectColumn,this,msymcol,ColumnType::String,nullptr));

    fl->addRow(tr("Footprint Column:"),hl=new QHBoxLayout);
    hl->addWidget(mfprintcol=new QLineEdit,1);
    connect(mfprintcol,&QLineEdit::textChanged,this,[this](const QString&t){mcurrent->setFootprintColumn(t);});
    hl->addWidget(pb=new QPushButton(selectIcon,tr("Select...")),0);
    connect(pb,&QPushButton::clicked,this,std::bind(&MWLibraryTab::selectColumn,this,mfprintcol,ColumnType::String,nullptr));

    fl->addRow(tr("Description Column:"),hl=new QHBoxLayout);
    bgrp=new QButtonGroup(hl);
    hl->addWidget(mdescroff=new QRadioButton(tr("Default: empty")),0);
    bgrp->addButton(mdescroff);
    hl->addWidget(mdescron=new QRadioButton(tr("Column:")),0);
    bgrp->addButton(mdescron);
    mdescroff->setChecked(true);
    hl->addWidget(mdescrcol=new QLineEdit,1);
    mdescrcol->setEnabled(false);
    connect(mdescrcol,&QLineEdit::textChanged,this,[this](const QString&t){mcurrent->setDescriptionColumn(t);});
    connect(mdescroff,&QRadioButton::toggled,this,[this](bool off){if(off)mcurrent->setDescriptionColumn("");});
    connect(mdescron,&QRadioButton::toggled,mdescrcol,&QWidget::setEnabled);
    hl->addWidget(pb=new QPushButton(selectIcon,tr("Select...")),0);
    connect(pb,&QPushButton::clicked,this,std::bind(&MWLibraryTab::selectColumn,this,mdescrcol,ColumnType::String,mdescron));

    fl->addRow(tr("Keywords Column:"),hl=new QHBoxLayout);
    bgrp=new QButtonGroup(hl);
    hl->addWidget(mkeywordoff=new QRadioButton(tr("Default: empty")),0);
    bgrp->addButton(mkeywordoff);
    hl->addWidget(mkeywordon=new QRadioButton(tr("Column:")),0);
    bgrp->addButton(mkeywordon);
    mkeywordoff->setChecked(true);
    hl->addWidget(mkeywordcol=new QLineEdit,1);
    mkeywordcol->setEnabled(false);
    connect(mkeywordcol,&QLineEdit::textChanged,this,[this](const QString&t){mcurrent->setKeywordsColumn(t);});
    connect(mkeywordoff,&QRadioButton::toggled,this,[this](bool off){if(off)mcurrent->setKeywordsColumn("");});
    connect(mkeywordon,&QRadioButton::toggled,mkeywordcol,&QWidget::setEnabled);
    hl->addWidget(pb=new QPushButton(selectIcon,tr("Select...")),0);
    connect(pb,&QPushButton::clicked,this,std::bind(&MWLibraryTab::selectColumn,this,mkeywordcol,ColumnType::String,mkeywordon));

    fl->addRow(tr("Exclude from BOM Column:"),hl=new QHBoxLayout);
    bgrp=new QButtonGroup(hl);
    hl->addWidget(mexclbomoff=new QRadioButton(tr("Default: include")),0);
    bgrp->addButton(mexclbomoff);
    hl->addWidget(mexclbomon=new QRadioButton(tr("Column:")),0);
    bgrp->addButton(mexclbomon);
    mexclbomoff->setChecked(true);
    hl->addWidget(mexclbomcol=new QLineEdit,1);
    mexclbomcol->setEnabled(false);
    connect(mexclbomcol,&QLineEdit::textChanged,this,[this](const QString&t){mcurrent->setExcludeBomColumn(t);});
    connect(mexclbomoff,&QRadioButton::toggled,this,[this](bool off){if(off)mcurrent->setExcludeBomColumn("");});
    connect(mexclbomon,&QRadioButton::toggled,mexclbomcol,&QWidget::setEnabled);
    hl->addWidget(pb=new QPushButton(selectIcon,tr("Select...")),0);
    connect(pb,&QPushButton::clicked,this,std::bind(&MWLibraryTab::selectColumn,this,mexclbomcol,ColumnType::String,mexclbomon));

    fl->addRow(tr("Exclude from Board Column:"),hl=new QHBoxLayout);
    bgrp=new QButtonGroup(hl);
    hl->addWidget(mexclboardoff=new QRadioButton(tr("Default: include")),0);
    bgrp->addButton(mexclboardoff);
    hl->addWidget(mexclboardon=new QRadioButton(tr("Column:")),0);
    bgrp->addButton(mexclboardon);
    mexclboardoff->setChecked(true);
    hl->addWidget(mexclboardcol=new QLineEdit,1);
    mexclboardcol->setEnabled(false);
    connect(mexclboardcol,&QLineEdit::textChanged,this,[this](const QString&t){mcurrent->setExcludeBoardColumn(t);});
    connect(mexclboardoff,&QRadioButton::toggled,this,[this](bool off){if(off)mcurrent->setExcludeBoardColumn("");});
    connect(mexclboardon,&QRadioButton::toggled,mexclboardcol,&QWidget::setEnabled);
    hl->addWidget(pb=new QPushButton(selectIcon,tr("Select...")),0);
    connect(pb,&QPushButton::clicked,this,std::bind(&MWLibraryTab::selectColumn,this,mexclboardcol,ColumnType::String,mexclboardon));

    vl->addSpacing(10);
    vl->addWidget(new QLabel(tr("Additional Fields:")),0);
    vl->addWidget(mfieldtable=new QTableView,1);
    mfieldtable->setModel(mfieldmodel=new QStandardItemModel(this));
    mfieldtable->setEditTriggers(QAbstractItemView::NoEditTriggers);
    mfieldmodel->insertColumns(0,6);
    mfieldmodel->setHorizontalHeaderLabels(QStringList()<<tr("Name")<<tr("Column")<<tr("Visible on Add")<<tr("Visible in Chooser")<<tr("Show Name")<<tr("Inherit Properties"));
    vl->addLayout(hl=new QHBoxLayout);
    hl->addStretch(1);
    hl->addWidget(pb=new QPushButton(Icons::iconByName(Icons::Names::listAdd),tr("Add Field...")),0);
    hl->addWidget(pb=new QPushButton(Icons::iconByName(Icons::Names::listRemove),tr("Remove Field")),0);

    mright->setEnabled(false);

    setWidget(split);

    refillLibraryModel();
}

void MWLibraryTab::saveGeometryToConfig(QString settingsKey)
{
    MainTab::saveGeometryToConfig(settingsKey);
    QSplitter*s=qobject_cast<QSplitter*>(widget());
    if(s)
        QSettings().setValue(settingsKey+"_split",s->saveState());
}

void MWLibraryTab::setGeometryFromConfig(QString settingsKey)
{
    MainTab::setGeometryFromConfig(settingsKey);
    QSplitter*s=qobject_cast<QSplitter*>(widget());
    if(s)
        s->restoreState(QSettings().value(settingsKey+"_split").toByteArray());
}


MWLibraryTab::~MWLibraryTab()
{
}

QString MWLibraryTab::tabTitle() const
{
    return tr("KiCAD &Libraries");
}

QString MWLibraryTab::tabToolTip() const
{
    return tr("KiCAD database library file setup");
}

void MWLibraryTab::addLibrary()
{
    //get a name
    QString libname;
    while(true){
        libname=QInputDialog::getText(widget(),tr("New Library"),tr("Please enter a name:"),QLineEdit::Normal,libname);
        if(libname.isEmpty())return;
        if(Project::LibraryDefinition::nameIsValid(libname))break;
        QMessageBox::warning(widget(),tr("Warning"),tr("Library name '%1' is not valid."));
    }
    //create lib
    auto lib=mproject->addLibrary();
    lib.setName(libname);
    //add to list
    int row=mlibmodel->rowCount();
    mlibmodel->insertRows(row,1);
    auto idx=mlibmodel->index(row,0);
    mlibmodel->setData(idx,libname);
    mlibmodel->setData(idx,lib.internalID(),Qt::UserRole);
    //display it
    selectLibrary(lib.internalID());
}

void MWLibraryTab::removeLibrary()
{
    //get current
    auto idx=mliblist->currentIndex();
    if(!idx.isValid())return;
    QUuid current=mlibmodel->data(idx,Qt::UserRole).toUuid();
    if(current.isNull())return;
    QString cname=mlibmodel->data(idx).toString();
    //ask for confirmation
    if(QMessageBox::question(widget(),tr("Really delete?"),tr("Really delete library '%1'?").arg(cname),QMessageBox::Yes,QMessageBox::No) != QMessageBox::Yes)return;
    //delete line
    mlibmodel->removeRows(idx.row(),1);
    //if on display, remove display
    if(current==mcurrent->internalID())
        selectLibrary(QUuid());
    //delete actual library
    mproject->removeLibrary(current);
}

void MWLibraryTab::renameLibrary()
{
    //get new name
    QString libname=mcurrent->name();
    do{
        libname=QInputDialog::getText(widget(),tr("Rename Library"),tr("Please enter a new name:"),QLineEdit::Normal,libname);
        if(libname.isEmpty())return;
    }while(!Project::LibraryDefinition::nameIsValid(libname));
    if(libname==mcurrent->name())return;
    //rename in project
    mcurrent->setName(libname);
    //change display
    mlibname->setText(libname);
    const auto id=mcurrent->internalID();
    for(int row=0;row<mlibmodel->rowCount();row++){
        auto idx=mlibmodel->index(row,0);
        if(mlibmodel->data(idx,Qt::UserRole).toUuid() == id)
            mlibmodel->setData(idx,libname);
    }
}

void MWLibraryTab::selectTable()
{
    //ask
    QSqlDatabase db=mdba->db();
    QStringList tables=db.tables(QSql::Tables)+db.tables(QSql::Views);
    QString table=mtablename->text();
    table=QInputDialog::getItem(widget(),tr("Select Table"),tr("Please select a table:"),tables,tables.indexOf(table),false);
    if(table.isEmpty())return;
    //set in project
    mcurrent->setTableName(table);
    //change display
    mtablename->setText(table);
}

void EBase::GUI::MWLibraryTab::selectColumn(QLineEdit*line, EBase::GUI::MWLibraryTab::ColumnType ct, QRadioButton* rbon)
{
    //FIXME: use columnType to just show good ones
    auto cols=mdba->sqlDialect().tableColumnNames(mdba->db(),mtablename->text());
    //show input dialog
    QString col=QInputDialog::getItem(widget(),tr("Select Column"),tr("Please select a column:"),cols,cols.indexOf(line->text()),false);
    if(col.isEmpty())return;
    //if ok: rbon->activate, set content
    if(rbon)rbon->setChecked(true);
    line->setText(col);
}

void MWLibraryTab::libraryRowSelected(const QModelIndex&idx)
{
    if(!idx.isValid())return;
    //get row/name
    QUuid libid=mlibmodel->data(idx,Qt::UserRole).toUuid();
    if(libid.isNull())return;
    //show
    selectLibrary(libid);
}

void MWLibraryTab::selectLibrary(const QUuid&libid)
{
    if(libid.isNull()){
        //no lib: kill display
        mlibname->setText(QString());
        mtablename->setText(QString());
        mkeycol->setText(QString());
        msymcol->setText(QString());
        mfprintcol->setText(QString());
        mdescrcol->setText(QString());
        mkeywordcol->setText(QString());
        mexclbomcol->setText(QString());
        mexclboardcol->setText(QString());
        mdescroff->setChecked(true);
        mkeywordoff->setChecked(true);
        mexclboardoff->setChecked(true);
        mexclbomoff->setChecked(true);
        mfieldmodel->removeRows(0,mfieldmodel->rowCount());
        mright->setEnabled(false);
    }else{
        //get object
        mcurrent.reset(new Project::LibraryDefinition(mproject->library(libid)));
        //display standard data
        mlibname->setText(mcurrent->name());
        mtablename->setText(mcurrent->tableName());
        mkeycol->setText(mcurrent->keyColumn());
        msymcol->setText(mcurrent->symbolColumn());
        mfprintcol->setText(mcurrent->footprintColumn());
        //display properties
        QString s;
        s=mcurrent->descriptionColumn();
        if(s.isEmpty())mdescroff->setChecked(true);
        else mdescron->setChecked(true);
        mdescrcol->setText(s);
        s=mcurrent->keywordsColumn();
        if(s.isEmpty())mkeywordoff->setChecked(true);
        else mkeywordon->setChecked(true);
        mkeywordcol->setText(s);
        s=mcurrent->excludeBomColumn();
        if(s.isEmpty())mexclbomoff->setChecked(true);
        else mexclbomon->setChecked(true);
        mexclbomcol->setText(s);
        s=mcurrent->excludeBoardColumn();
        if(s.isEmpty())mexclboardoff->setChecked(true);
        else mexclboardon->setChecked(true);
        mexclboardcol->setText(s);
        //display fields
        mfieldmodel->removeRows(0,mfieldmodel->rowCount());
        QStringList fields=mcurrent->fieldNames();
        mfieldmodel->insertRows(0,fields.size());
        int row=0;
        const auto yes=tr("Yes","bool");
        const auto no=tr("No","bool");
        for(const auto&field:fields){
            mfieldmodel->setData(mfieldmodel->index(row,0),field);
            mfieldmodel->setData(mfieldmodel->index(row,1),mcurrent->fieldColumn(field));
            mfieldmodel->setData(mfieldmodel->index(row,2),mcurrent->fieldVisibleOnAdd(field)?yes:no);
            mfieldmodel->setData(mfieldmodel->index(row,3),mcurrent->fieldVisibleInChooser(field)?yes:no);
            mfieldmodel->setData(mfieldmodel->index(row,4),mcurrent->fieldShowName(field)?yes:no);
            mfieldmodel->setData(mfieldmodel->index(row,5),mcurrent->fieldInheritProperties(field)?yes:no);
            row++;
        }
        mfieldtable->resizeColumnsToContents();
        //enable
        mright->setEnabled(true);
    }
}

void MWLibraryTab::refillLibraryModel()
{
    mlibmodel->clear();
    //basics?
    if(mlibmodel->columnCount()<1){
        mlibmodel->insertColumns(0,1);
        mlibmodel->setHorizontalHeaderLabels(QStringList()<<tr("DB Libraries"));
    }
    //fill
    auto idlist=mproject->libraryUids();
    mlibmodel->insertRows(0,idlist.size());
    int row=0;
    for(auto id:idlist){
        auto idx=mlibmodel->index(row++,0);
        mlibmodel->setData(idx,mproject->libraryName(id));
        mlibmodel->setData(idx,id,Qt::UserRole);
    }
}
