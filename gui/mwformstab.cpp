// Electric Base App: Main Window - DB Tab
// (c) Konrad Rosenbaum, 2024
// protected under GNU GPL v.3 or at your option any newer

#include "mwformstab.h"

using namespace EBase::GUI;

MWFormsTab::MWFormsTab(MainWindow*mw, Project::Project*prj, DB::DBAccessor*dba)
:MainTab(mw,prj,dba)
{
    setWidget(new QWidget);//TODO!!
}

MWFormsTab::~MWFormsTab()
{
}

QString MWFormsTab::tabTitle() const
{
    return tr("Views && &Forms");
}

QString MWFormsTab::tabToolTip() const
{
    return tr("Data viewer and data entry forms.");
}
