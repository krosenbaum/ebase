// Electric Base App: ODBC GUI
// (c) Konrad Rosenbaum, 2024
// protected under GNU GPL v.3 or at your option any newer

#pragma once

#include <QDialog>

class QTableView;
class QStandardItemModel;

namespace EBase {

namespace ODBC {
class ODBCconnectionResult;
}

namespace GUI {

///Shows available ODBC DSNs, allows to manipulate them.
class OdbcDsn : public QDialog
{
    Q_OBJECT
public:
    explicit OdbcDsn(QWidget*parent=nullptr);
    ~OdbcDsn();

private slots:
    void addDsn();

private:
    QTableView *mglbdsntable,*musrdsntable;
    QStandardItemModel *mglbdsnmodel,*musrdsnmodel;

    void fillDSNs();
};

///Shows available ODBC drivers.
class OdbcDriverList : public QDialog
{
    Q_OBJECT
public:
    explicit OdbcDriverList(QWidget*parent=nullptr);
    ~OdbcDriverList();
};

///Shows the results of an ODBC connection test.
class OdbcConnectionTest : public QDialog
{
    Q_OBJECT
public:
    explicit OdbcConnectionTest(const EBase::ODBC::ODBCconnectionResult&,QWidget*parent=nullptr);
    ~OdbcConnectionTest();
private:
    QString returnCodeStr(int);
};

//END of namespace
}}
