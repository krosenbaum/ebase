// Electric Base App: Main Window
// (c) Konrad Rosenbaum, 2024
// protected under GNU GPL v.3 or at your option any newer

#pragma once

#include <QMainWindow>
#include <QPointer>

class QCheckBox;
class QComboBox;
class QLabel;
class QLineEdit;
class QMenu;
class QSpinBox;
class QStandardItemModel;
class QSqlDatabase;

///Main Namespace for all of Electric Base.
namespace EBase {

namespace Project { class Project; }
namespace DB { class DBAccessor; }

///Namespace for all GUI classes (windows, widgets, dialogs, etc.).
namespace GUI {

class MWBasicsTab;
class MWDatabaseTab;
class MWLibraryTab;
class MWFormsTab;
class MWSqlTab;

///Main Application Window.
///Each instance represents one library project.
class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    ///Instantiates empty main window.
    ///\param prj if not NULL: the initial project for the window
    MainWindow(Project::Project*prj=nullptr);
    ///Frees all resources associated with this window. Saves contents.
    ~MainWindow();

private slots:
    //Project Menu
    ///create new project
    void newProject();
    ///open existing project
    void openProject();
    ///open project by file name, mostly used by "recent files" menu
    void openProjectFile(QString);
    ///"recent files" menu has changed
    void recentChanged();
    ///save all contents.
    void save();

    //GUI construction
    ///initialize GUI for project
    void initGui();
    ///update window title by inquiring project
    void updateTitle();

    //Help/About Menu
    ///shows help in browser (if it can be found)
    void help();
    ///shows about window for app
    void about();
    ///shows about window for icons
    void aboutOxygen();
    ///about ODBC
    void odbcVersion();
    ///shows what DB plugins/drivers are available
    void dbPluginInfo();

    //ODBC Menu
    ///shows what ODBC drivers are available
    void odbcDrvInfo();
    ///shows what ODBC DSNs are available
    void odbcDsnInfo();
    ///creates new DSN
    void odbcDsnWizard();

signals:
    ///emitted from the destructor as a last ditch effort to save stuff
    void aboutToClose();
    ///emitted when the DB is reloaded
    void dbReload();
    ///emitted when the DB is closed
    void dbClosed();
    ///emitted before the DB is closed
    void dbAboutToClose();

private:
    QPointer<Project::Project> mproject;
    QPointer<EBase::DB::DBAccessor> mdba;
    QMenu *mrecentfiles=nullptr;
    QPointer<MWBasicsTab>mbasetab;
    QPointer<MWDatabaseTab>mdbtab;
    QPointer<MWLibraryTab>mlibtab;
    QPointer<MWFormsTab>mformtab;
    QPointer<MWSqlTab>msqltab;

    ///helper: register a "recent file"
    void registerRecent();

    ///helper: open project by file name and mode (used by openProject* and newProject)
    void openProjectTool(QString fname,bool openNew);
};

///Represents the base class for any tab in the main window.
class MainTab : public QObject
{
    Q_OBJECT
    MainTab()=delete;
public:
    ///instantiate new tab, derived classes must call this constructor and create the full widget in their's
    MainTab(MainWindow*parent_,Project::Project*project,DB::DBAccessor*dbaccess);
    ///delete tab object, should not touch the actual widget - the main window will take care of that
    ~MainTab();

    ///returns the title that the tab should have, derived classes must implement this.
    virtual QString tabTitle()const=0;
    ///returns a tool tip string for the tab, per default the title is returned.
    virtual QString tabToolTip()const{return tabTitle();}

    ///gets the geometry from settings, pastes it onto the widget and remembers to save it on destruction
    virtual void setGeometryFromConfig(QString settingsKey);
    ///saves the geometry to settings (called during destruction - from aboutToClose)
    virtual void saveGeometryToConfig(QString settingsKey);

    ///returns the widget for the tab
    QWidget*widget(){return mwidget;}

    ///derived classes can override this to display data in the status bar or to remember the status bar for later
    virtual void configStatusBar(QStatusBar*){}

protected slots:
    ///called just before the main window self-destructs, derived classes may save some stuff from here and must call the base implementation
    virtual void aboutToClose();
    ///derived classes can override this to reload their view on a DB reload
    virtual void dbReload(){}
    ///derived classes can override this to change their display when the DB is closed
    virtual void dbClosed(){}
    ///derived classes can override this to change their display when the DB is about to be closed
    virtual void dbAboutToClose(){}

protected:
    ///pointer back to the main window
    QPointer<MainWindow>mmainwin;
    ///pointer to the open project
    QPointer<Project::Project>mproject;
    ///pointer to the DB access instance
    QPointer<DB::DBAccessor>mdba;

    ///must be called by derived classes' constructor to set the tab widget
    void setWidget(QWidget*w){mwidget=w;}
private:
    QString mgeokey;
    QPointer<QWidget>mwidget;
};

namespace Private {

///helper singleton for notifications that need to reach all MainWindows
class MainWindowNotifier : public QObject
{
    static MainWindowNotifier*inst;

    Q_OBJECT
    MainWindowNotifier();
    ~MainWindowNotifier();
public:
    static MainWindowNotifier* instance();
signals:
    ///informs windows that "recent files" menu has changed
    void recentChanged();
};

//END of Private namespace
}

//END of GUI namespaces
}}
