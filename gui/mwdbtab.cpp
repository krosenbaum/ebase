// Electric Base App: Main Window - DB Tab
// (c) Konrad Rosenbaum, 2024
// protected under GNU GPL v.3 or at your option any newer

#include "mwdbtab.h"
#include "db/dbaccess.h"

#include <QLabel>
#include <QSettings>
#include <QSplitter>
#include <QSqlDatabase>
#include <QStandardItemModel>
#include <QTreeView>

namespace EBase::GUI::Private {
    ///Used by \ref EBase::GUI::MWDatabaseTab internally to mark tree entries as referencing table or view.
    ///\relates EBase::GUI::MWDatabaseTab
    enum class TableType {
        ///Not a reference.
        None=0,
        ///References a Table.
        Table,
        ///References a View.
        View
    };
}
Q_DECLARE_METATYPE(EBase::GUI::Private::TableType);

using namespace EBase::GUI;
using namespace EBase::GUI::Private;

MWDatabaseTab::MWDatabaseTab(MainWindow*mw, Project::Project*prj, DB::DBAccessor*dba)
:MainTab(mw,prj,dba)
{
    auto split=new QSplitter;
    setWidget(split);
    //TODO!!
    split->addWidget(mdbstruct=new QTreeView);
    mdbstruct->setModel(mdbstructmodel=new QStandardItemModel(this));
    mdbstruct->setEditTriggers(QAbstractItemView::NoEditTriggers);
    split->addWidget(new QLabel("Here be Table or View design data..."));

    //initial load?
    if(dba->isOpen())dbReload();
}

MWDatabaseTab::~MWDatabaseTab()
{
}

QString MWDatabaseTab::tabTitle() const
{
    return tr("&Database Design");
}

QString MWDatabaseTab::tabToolTip() const
{
    return tr("Database Overview and Design");
}

void EBase::GUI::MWDatabaseTab::dbReload()
{
    //get tables/views
    mdbstructmodel->clear();
    if(mdbstructmodel->columnCount()<1){
        mdbstructmodel->insertColumn(0);
        mdbstructmodel->setHorizontalHeaderLabels(QStringList()<<tr("Tables & Views"));
    }
    mdbstructmodel->insertRows(0,2);
    QModelIndex tableroot=mdbstructmodel->index(0,0);
    QModelIndex viewroot =mdbstructmodel->index(1,0);
    mdbstructmodel->setData(tableroot,tr("Tables"));
    mdbstructmodel->setData(tableroot,QVariant::fromValue(TableType::None),Qt::UserRole);
    mdbstructmodel->setData(viewroot,tr("Views"));
    mdbstructmodel->setData(viewroot,QVariant::fromValue(TableType::None),Qt::UserRole);
    auto db=mdba->db();
    auto tables=db.tables(QSql::Tables);
    int row=0;
    mdbstructmodel->insertColumn(0,tableroot);
    mdbstructmodel->insertRows(0,tables.size(),tableroot);
    for(const auto&t:tables){
        auto idx=mdbstructmodel->index(row++,0,tableroot);
        mdbstructmodel->setData(idx,t);
        mdbstructmodel->setData(idx,QVariant::fromValue(TableType::Table),Qt::UserRole);
    }
    tables=db.tables(QSql::Views);
    row=0;
    mdbstructmodel->insertColumn(0,viewroot);
    mdbstructmodel->insertRows(0,tables.size(),viewroot);
    for(const auto&t:tables){
        auto idx=mdbstructmodel->index(row++,0,viewroot);
        mdbstructmodel->setData(idx,t);
        mdbstructmodel->setData(idx,QVariant::fromValue(TableType::View),Qt::UserRole);
    }
    mdbstruct->expandAll();
    //TODO: reload right side table display
}

void EBase::GUI::MWDatabaseTab::dbAboutToClose()
{
    //TODO: drop table display
    mdbstructmodel->clear();
}

void EBase::GUI::MWDatabaseTab::dbClosed()
{
}

void MWDatabaseTab::saveGeometryToConfig(QString settingsKey)
{
    MainTab::saveGeometryToConfig(settingsKey);
    QSplitter*s=qobject_cast<QSplitter*>(widget());
    if(s)
        QSettings().setValue(settingsKey+"_split",s->saveState());
}

void MWDatabaseTab::setGeometryFromConfig(QString settingsKey)
{
    MainTab::setGeometryFromConfig(settingsKey);
    QSplitter*s=qobject_cast<QSplitter*>(widget());
    if(s)
        s->restoreState(QSettings().value(settingsKey+"_split").toByteArray());
}
