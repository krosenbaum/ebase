// Electric Base App: Main Window - Basics Tab
// (c) Konrad Rosenbaum, 2024
// protected under GNU GPL v.3 or at your option any newer

#include <QApplication>
#include <QCheckBox>
#include <QComboBox>
#include <QFormLayout>
#include <QGroupBox>
#include <QInputDialog>
#include <QLabel>
#include <QLineEdit>
#include <QProgressDialog>
#include <QPushButton>
#include <QScrollArea>
#include <QSettings>
#include <QSpinBox>
#include <QStatusBar>
#include <QToolButton>
#include <QBoxLayout>

#include "mwbasetab.h"
#include "odbcgui.h"
#include "kicadpathdlg.h"
#include "icons.h"
#include "db/odbcdrv.h"
#include "db/dbaccess.h"
#include "db/dbdialect.h"
#include "project/project.h"

using namespace EBase;
using namespace EBase::GUI;



// ////////////////////////////
// Basic Tab construction

QString EBase::GUI::MWBasicsTab::tabTitle() const
{
    return tr("Project &Basics");
}

QString EBase::GUI::MWBasicsTab::tabToolTip() const
{
    return tr("Basic Project and Database Settings");
}


void EBase::GUI::MWBasicsTab::configStatusBar(QStatusBar*sb)
{
    sb->addPermanentWidget(mdbstatonbar=new QLabel(tr("DB not connected")),0);
    mdbstatonbar->setFrameShape(QFrame::Box);
    mdbstatonbar->setFrameShadow(QFrame::Sunken);
}

MWBasicsTab::MWBasicsTab(MainWindow*mw, Project::Project*prj, DB::DBAccessor*dba)
:MainTab(mw,prj,dba)
{
    QGroupBox*gb;
    QVBoxLayout*vl,*ml;
    QHBoxLayout*hl;
    QFormLayout*fl;
    QLineEdit*ledit,*ledit2;
    QPushButton*pb;
    QToolButton*tb;

    QWidget*w=new QWidget;

    w->setLayout(ml=new QVBoxLayout);

    ml->addWidget(gb=new QGroupBox(tr("KiCAD Library Basics")));
    gb->setLayout(fl=new QFormLayout);
    fl->addRow(tr("Project file name:"),new QLabel(mproject->projectFileName()));
    fl->addRow(tr("Library file name:"),new QLabel(mproject->libraryFileName()));
    fl->addRow(tr("Project name:"),ledit=new QLineEdit(mproject->name()));
    connect(ledit,&QLineEdit::textEdited,mproject,&Project::Project::setName);
    fl->addRow(tr("Project description:"),ledit=new QLineEdit(mproject->description()));
    connect(ledit,&QLineEdit::textEdited,mproject,&Project::Project::setDescription);
    fl->addRow(tr("Symbol Search Path:"),hl=new QHBoxLayout);
    hl->addWidget(ledit=new QLineEdit(mproject->symbolSearchPath().join("; ")),1);
    ledit->setReadOnly(true);
    hl->addWidget(pb=new QPushButton("..."),0);
    fl->addRow(tr("Footprint Search Path:"),hl=new QHBoxLayout);
    hl->addWidget(ledit2=new QLineEdit(mproject->footprintSearchPath().join("; ")),1);
    ledit2->setReadOnly(true);
    auto setpathes=[=,this](KicadPathDialog::ShowTab st)->void{
        KicadPathDialog pd(mproject.get(),st,widget());
        if(pd.exec()==QDialog::Accepted){
            pd.savePathes();
            ledit->setText(mproject->symbolSearchPath().join("; "));
            ledit2->setText(mproject->footprintSearchPath().join("; "));
        }
    };
    connect(pb,&QPushButton::clicked,this,std::bind(setpathes,KicadPathDialog::ShowSymbols));
    hl->addWidget(pb=new QPushButton("..."),0);
    connect(pb,&QPushButton::clicked,this,std::bind(setpathes,KicadPathDialog::ShowFootprints));

    ml->addWidget(gb=new QGroupBox(tr("KiCAD ODBC Settings")));
    gb->setLayout(fl=new QFormLayout);
    auto odbc=mproject->odbcSettings();
    fl->addRow(tr("DSN:","odbc"),hl=new QHBoxLayout);
    hl->addWidget(modbcdsn=new QLineEdit(odbc.dsn()),1);
    hl->addWidget(pb=new QPushButton("..."),0);
    connect(pb,&QPushButton::clicked,this,&MWBasicsTab::chooseDSN);
    fl->addRow(tr("Username:","odbc"),modbcusr=new QLineEdit(odbc.username()));
    fl->addRow(tr("Password:","odbc"),hl=new QHBoxLayout);
    hl->addWidget(modbcpass=new QLineEdit(odbc.password()),1);
    hl->addWidget(tb=new QToolButton,0);
    tb->setCheckable(true);
    tb->setChecked(false);
    QIcon visico=Icons::iconByName(Icons::Names::visible,Icons::IconVariants::State);
    tb->setIcon(visico);
    connect(tb,&QToolButton::toggled,this,[this](bool c){modbcpass->setEchoMode(c?QLineEdit::Normal:QLineEdit::Password);});
    fl->addRow(modbcusecs=new QCheckBox(tr("Use Connection String")));
    modbcpass->setEchoMode(QLineEdit::Password);
    const bool usecs=odbc.useConnectionString();
    modbcusecs->setChecked(usecs);
    modbcdsn->setEnabled(!usecs);connect(modbcusecs,&QCheckBox::toggled,modbcdsn,&QWidget::setDisabled);
    pb->setEnabled(!usecs);connect(modbcusecs,&QCheckBox::toggled,pb,&QWidget::setDisabled);
    tb->setEnabled(!usecs);connect(modbcusecs,&QCheckBox::toggled,tb,&QWidget::setDisabled);
    modbcusr->setEnabled(!usecs);connect(modbcusecs,&QCheckBox::toggled,modbcusr,&QWidget::setDisabled);
    modbcpass->setEnabled(!usecs);connect(modbcusecs,&QCheckBox::toggled,modbcpass,&QWidget::setDisabled);
    fl->addRow(tr("Connection String:","odbc"),modbccstr=new QLineEdit(odbc.connectionString()));
    modbccstr->setEnabled(usecs);connect(modbcusecs,&QCheckBox::toggled,modbccstr,&QWidget::setEnabled);
    connect(modbcdsn,&QLineEdit::textEdited,this,&MWBasicsTab::saveOdbc);
    connect(modbcusr,&QLineEdit::textEdited,this,&MWBasicsTab::saveOdbc);
    connect(modbcpass,&QLineEdit::textEdited,this,&MWBasicsTab::saveOdbc);
    connect(modbccstr,&QLineEdit::textEdited,this,&MWBasicsTab::saveOdbc);
    connect(modbcusecs,&QCheckBox::toggled,this,&MWBasicsTab::saveOdbc,Qt::QueuedConnection);
    fl->addRow(vl=new QVBoxLayout);
    vl->addSpacing(10);
    fl->addRow(tr("Connection Timeout:"),modbctime=new QSpinBox);
    modbctime->setRange(0,999);
    modbctime->setValue(odbc.timeout());
    connect(modbctime,&QSpinBox::valueChanged,this,[this](int i){mproject->odbcSettings().setTimeout(i);});

    ml->addWidget(gb=new QGroupBox(tr("Use Alternate DB Connection")));
    auto altdb=mproject->dbSettings();
    connect(gb,&QGroupBox::toggled,this,[this](bool on){mproject->dbSettings().setUseAlternate(on);},Qt::QueuedConnection);
    gb->setCheckable(true);
    gb->setChecked(altdb.useAlternate());
    gb->setLayout(fl=new QFormLayout);
    fl->addRow(tr("DB Type:"),madbtype=new QComboBox);
    fillDbTypes(altdb.driverName());
    madbtype->setEditable(false);
    connect(madbtype,&QComboBox::currentIndexChanged,this,[this](){
        mproject->dbSettings().setDriver(madbtype->currentData().toString());
    });
    fl->addRow(tr("Database name:"),madbname=new QLineEdit(altdb.databaseName()));
    connect(madbname,&QLineEdit::textEdited,this,[this](QString n){mproject->dbSettings().setDatabaseName(n);});
    fl->addRow(tr("Hostname:"),madbhost=new QLineEdit(altdb.hostName()));
    fl->addRow(tr("Port number:"),madbport=new QSpinBox);
    madbport->setRange(-1,0xffff);
    madbport->setValue(altdb.portNumber());
    auto sethost=[this]()->void{
        mproject->dbSettings().setHost(madbhost->text(),madbport->value());
    };
    connect(madbhost,&QLineEdit::textEdited,this,sethost);
    connect(madbport,&QSpinBox::valueChanged,this,sethost);
    fl->addRow(tr("User name:"),madbusr=new QLineEdit(altdb.username()));
    fl->addRow(tr("Password:"),hl=new QHBoxLayout);
    hl->addWidget(madbpass=new QLineEdit(altdb.password()),1);
    madbpass->setEchoMode(QLineEdit::Password);
    hl->addWidget(tb=new QToolButton,0);
    tb->setCheckable(true);
    tb->setChecked(false);
    tb->setIcon(visico);
    connect(tb,&QToolButton::toggled,this,[this](bool c){madbpass->setEchoMode(c?QLineEdit::Normal:QLineEdit::Password);});
    auto setlogin=[this]()->void{
        mproject->dbSettings().setLogin(madbusr->text(),madbpass->text());
    };
    connect(madbusr,&QLineEdit::textEdited,this,setlogin);
    connect(madbpass,&QLineEdit::textEdited,this,setlogin);
    fl->addRow(tr("Connect Options:"),madbopt=new QLineEdit(altdb.connectOptions()));
    connect(madbopt,&QLineEdit::textEdited,this,[this](QString o){mproject->dbSettings().setConnectOptions(o);});

    ml->addWidget(gb=new QGroupBox(tr("Database Functions")));
    gb->setLayout(fl=new QFormLayout);
    fl->addRow(tr("Database Status:"),mdbstatus=new QLabel(tr("<html>Not connected yet.")));
    fl->addRow(tr("Database Info:"),mdbinfo=new QLabel(""));
    fl->addRow(tr("SQL Dialect:"),mdbdialect=new QLabel(""));
    fl->addRow(hl=new QHBoxLayout);
    hl->addStretch(1);
    hl->addWidget(pb=new QPushButton(Icons::iconByName(Icons::Names::testODBC),tr("Test KiCAD ODBC")));
    connect(pb,&QPushButton::clicked,this,&MWBasicsTab::testOdbc);
    hl->addWidget(pb=new QPushButton(Icons::iconByName(Icons::Names::connectDB),tr("Connect to DB")));
    connect(pb,&QPushButton::clicked,this,std::bind(&MWBasicsTab::openDB,this,true));

    ml->addStretch(1);

    //wrap in scroll area
    QScrollArea*sa=new QScrollArea;
    sa->setAlignment(Qt::AlignTop|Qt::AlignLeft);
    sa->setWidgetResizable(true);
    sa->setWidget(w);

    //wrap in tab
    setWidget(sa);

    //inform parent about DB actions
    connect(this,&MWBasicsTab::dbAboutToCloseSig,mmainwin,&MainWindow::dbAboutToClose);
    connect(this,&MWBasicsTab::dbClosedSig,mmainwin,&MainWindow::dbClosed);
    connect(this,&MWBasicsTab::dbOpenedSig,mmainwin,&MainWindow::dbReload);
}

void MWBasicsTab::fillDbTypes(QString current)
{
    madbtype->clear();
    if(current.isEmpty())
        madbtype->addItem(tr("Please chose!"));
    else{
        madbtype->addItem(tr("Preset: %1 (%2)").arg(current).arg(DB::DBInfo(current).humanReadableType()), current);
    }
    for(const auto&drv:DB::DBInfo::allAvailable()){
        madbtype->addItem(QString("%1 (%2)").arg(drv.humanReadableType()).arg(drv.driver()), drv.driver());
    }
}

EBase::GUI::MWBasicsTab::~MWBasicsTab()
{
}



// ////////////////////////////
// Basic Tab

void MWBasicsTab::saveOdbc()
{
    if(mproject.isNull())return;
    if(modbcusecs->isChecked()){
        mproject->odbcSettings().setConnectionString(modbccstr->text());
    }else{
        mproject->odbcSettings().setDSN(
            modbcdsn->text(),
            modbcusr->text(),
            modbcpass->text()
        );
    }
}

void MWBasicsTab::testOdbc()
{
    //show "wait" window
    QProgressDialog pd(tr("Attempting to connect to ODBC database..."),QString(),0,10,widget());
    pd.show();
    qApp->processEvents();
    //run test
    auto result=ODBC::ODBCInterface::instance().testConnection(mproject->odbcSettings().toDriverSettings());
    //hide "wait" window
    pd.close();
    //show results
    OdbcConnectionTest ct(result,widget());
    ct.exec();
}

void MWBasicsTab::chooseDSN()
{
    QInputDialog d(widget());
    d.setComboBoxItems(ODBC::ODBCInterface::instance().dsNames());
    d.setComboBoxEditable(false);
    d.setTextValue(modbcdsn->text());
    d.setLabelText(tr("Choose DSN:"));
    d.setWindowTitle(tr("ODBC DSN"));
    if(d.exec()!=QDialog::Accepted)return;
    modbcdsn->setText(d.textValue());
    saveOdbc();
}

void MWBasicsTab::openDB(bool showError)
{
    //close if currently open
    closeDB();
    //init & open
    mdba->open(mproject,showError?widget():nullptr);
    if(mdba->isOpen())emit dbOpenedSig();
    //display status
    updateDbStatus();
}

void MWBasicsTab::closeDB()
{
    emit dbAboutToCloseSig();
    if(mdba->isOpen())mdba->close();
    updateDbStatus();
    emit dbClosedSig();
}

void MWBasicsTab::updateDbStatus()
{
    const bool isopen=mdba->isOpen();
    if(mdbstatus){
        mdbstatus->setText(isopen?tr("<html><font color='green'>Connected</font>"):tr("<html><font color='red'>Disconnected</font>"));
    }
    if(mdbinfo){
        mdbinfo->setText(mdba->dbInfo());
    }
    if(mdbdialect){
        mdbdialect->setText(mdba->sqlDialect().dialectName());
    }
    if(mdbstatonbar){
        mdbstatonbar->setText(isopen?tr("DB connected"):tr("DB not connected"));
    }
}



