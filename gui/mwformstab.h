// Electric Base App: Main Window - DB Tab
// (c) Konrad Rosenbaum, 2024
// protected under GNU GPL v.3 or at your option any newer

#pragma once

#include "mwin.h"


namespace EBase {
namespace GUI {

///DB/Form Data Entry Tab of Main Application Window.
class MWFormsTab : public MainTab
{
    Q_OBJECT
public:
    ///Instantiates tab.
    MWFormsTab(MainWindow*,Project::Project*,DB::DBAccessor*);
    ///Frees all resources associated with this window. Saves contents.
    ~MWFormsTab();

    QString tabTitle() const override;
    QString tabToolTip() const override;
    // void configStatusBar(QStatusBar * ) override;
};

//END of namespace
}}
