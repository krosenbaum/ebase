// Electric Base App: KiCAD search path dialog
// (c) Konrad Rosenbaum, 2024
// protected under GNU GPL v.3 or at your option any newer

#include "kicadpathdlg.h"
#include "project/project.h"
#include "project/kicadcache.h"

#include <QBoxLayout>
#include <QFileDialog>
#include <QFileInfo>
#include <QPushButton>
#include <QSettings>
#include <QStandardItemModel>
#include <QTabWidget>
#include <QTableView>

static const QString geoKey{"geo/KicadPathDialog"};

EBase::GUI::KicadPathDialog::KicadPathDialog(EBase::Project::Project*prj,ShowTab st, QWidget* p)
:QDialog(p),mproject(prj)
{
    setWindowTitle(tr("KiCAD Search Pathes"));

    //basic layout
    QVBoxLayout*vl;
    QHBoxLayout*hl;
    QTabWidget*tab;
    QPushButton*pb;
    QWidget*w;
    setLayout(vl=new QVBoxLayout);
    vl->addWidget(tab=new QTabWidget,1);
    vl->addSpacing(15);
    vl->addLayout(hl=new QHBoxLayout,0);
    hl->addStretch(1);
    hl->addWidget(pb=new QPushButton(tr("&Save")),0);
    connect(pb,&QPushButton::clicked,this,&QDialog::accept);
    hl->addWidget(pb=new QPushButton(tr("&Cancel")),0);
    connect(pb,&QPushButton::clicked,this,&QDialog::reject);

    //tabs
    const int symidx=tab->addTab(w=new QWidget,tr("S&ymbols"));
    w->setLayout(vl=new QVBoxLayout);
    vl->addWidget(msymtable=new QTableView,1);
    msymtable->setEditTriggers(QAbstractItemView::NoEditTriggers);
    msymtable->setSelectionMode(QAbstractItemView::SelectionMode::SingleSelection);
    msymtable->setSelectionBehavior(QAbstractItemView::SelectionBehavior::SelectRows);
    msymtable->setModel(msymmodel=new QStandardItemModel(this));
    vl->addLayout(hl=new QHBoxLayout,0);
    hl->addWidget(pb=new QPushButton(tr("Add File...")),0);
    connect(pb,&QPushButton::clicked,this,&KicadPathDialog::addSymbol);
    hl->addWidget(pb=new QPushButton(tr("Add Path...")),0);
    connect(pb,&QPushButton::clicked,this,&KicadPathDialog::addSymbolDir);
    hl->addWidget(pb=new QPushButton(tr("Remove")),0);
    connect(pb,&QPushButton::clicked,this,&KicadPathDialog::removeSymbol);
    hl->addStretch(1);

    const int fpridx=tab->addTab(w=new QWidget,tr("&Footprints"));
    w->setLayout(vl=new QVBoxLayout);
    vl->addWidget(mfprtable=new QTableView,1);
    mfprtable->setEditTriggers(QAbstractItemView::NoEditTriggers);
    mfprtable->setSelectionMode(QAbstractItemView::SelectionMode::SingleSelection);
    mfprtable->setSelectionBehavior(QAbstractItemView::SelectionBehavior::SelectRows);
    mfprtable->setModel(mfprmodel=new QStandardItemModel(this));
    vl->addLayout(hl=new QHBoxLayout,0);
    hl->addWidget(pb=new QPushButton(tr("Add Path...")),0);
    connect(pb,&QPushButton::clicked,this,&KicadPathDialog::addFootprint);
    hl->addWidget(pb=new QPushButton(tr("Remove")),0);
    connect(pb,&QPushButton::clicked,this,&KicadPathDialog::removeFootprint);
    hl->addStretch(1);

    //initialize
    mcache=new Project::KicadCache(this);
    mcache->loadLibraries(prj);
    initTables();

    //select initial tab
    if(st==ShowSymbols)tab->setCurrentIndex(symidx);
    else if(st==ShowFootprints)tab->setCurrentIndex(fpridx);

    //epilog
    setSizeGripEnabled(true);
    restoreGeometry(QSettings().value(geoKey).toByteArray());
}

EBase::GUI::KicadPathDialog::~KicadPathDialog()
{
    QSettings().setValue(geoKey,saveGeometry());
}

void EBase::GUI::KicadPathDialog::savePathes()
{
    QStringList lst;
    //Symbols
    int nr=msymmodel->rowCount();
    for(int row=0;row<nr;row++)
        lst.append(msymmodel->data(msymmodel->index(row,0)).toString());
    mproject->setSymbolSearchPath(lst);
    //Footprints
    lst.clear();
    nr=mfprmodel->rowCount();
    for(int row=0;row<nr;row++)
        lst.append(mfprmodel->data(mfprmodel->index(row,0)).toString());
    mproject->setFootprintSearchPath(lst);
}

void EBase::GUI::KicadPathDialog::initTables()
{
    msymmodel->insertColumns(0,3);
    msymmodel->setHorizontalHeaderLabels(QStringList()<<tr("Symbol Path")<<tr("Type")<<tr("# Symbols"));
    for(const auto&s:mproject->symbolSearchPath())
        addSymbolPath(s);
    mfprmodel->insertColumns(0,3);
    mfprmodel->setHorizontalHeaderLabels(QStringList()<<tr("Footprint Path")<<tr("Type")<<tr("# Footprints"));
    for(const auto&f:mproject->footprintSearchPath())
        addFootprintPath(f);
}

void EBase::GUI::KicadPathDialog::addSymbol()
{
    //get file
    auto path=QFileDialog::getOpenFileName(this,tr("Select KiCAD Symbol File"),QString(),tr("KiCAD Symbol (*.kicad_sym)"));
    //validate file exists
    if(path.isEmpty())return;
    QFileInfo fi(path);
    if(!fi.exists() || !fi.isFile() || !fi.isReadable())return;
    //add
    addSymbolPath(path);
}

void EBase::GUI::KicadPathDialog::addSymbolDir()
{
    //get dir
    auto path=QFileDialog::getExistingDirectory(this,tr("Select Directory with KiCAD Symbols"));
    //validate it exists
    if(path.isEmpty())return;
    QFileInfo fi(path);
    if(!fi.exists() || !fi.isDir())return;
    //add
    addSymbolPath(path);
}

void EBase::GUI::KicadPathDialog::addSymbolPath(QString sp)
{
    //scan
    mcache->loadSymbols(sp);
    auto info=mcache->pathInfo(Project::KicadCache::SymbolLibrary,sp);
    //store infos in table
    int row=msymmodel->rowCount();
    msymmodel->insertRows(row,1);
    msymmodel->setData(msymmodel->index(row,0),sp);
    msymmodel->setData(msymmodel->index(row,1),info.pathTypeString());
    msymmodel->setData(msymmodel->index(row,2),
            tr("%1 Libs, %2 Symbols")
            .arg(info.numLibraries())
            .arg(info.numItems())
        );
    //adjust size
    msymtable->resizeColumnsToContents();
}

void EBase::GUI::KicadPathDialog::removeSymbol()
{
    //get row
    auto idx=msymtable->selectionModel()->selectedIndexes();
    if(idx.size()<1)return;
    int row=idx[0].row();
    //delete row
    msymmodel->removeRows(row,1);
}

void EBase::GUI::KicadPathDialog::addFootprint()
{
    //get dir
    auto path=QFileDialog::getExistingDirectory(this,tr("Select Directory with KiCAD Footprints"));
    //validate it exists
    if(path.isEmpty())return;
    QFileInfo fi(path);
    if(!fi.exists() || !fi.isDir())return;
    //add
    addFootprintPath(path);
}

void EBase::GUI::KicadPathDialog::addFootprintPath(QString fp)
{
    //scan
    mcache->loadFootprints(fp);
    auto info=mcache->pathInfo(Project::KicadCache::FootprintLibrary,fp);
    //store in table
    int row=mfprmodel->rowCount();
    mfprmodel->insertRows(row,1);
    mfprmodel->setData(mfprmodel->index(row,0),fp);
    mfprmodel->setData(mfprmodel->index(row,1),info.pathTypeString());
    mfprmodel->setData(mfprmodel->index(row,2),
            tr("%1 Libs, %2 Footprints")
            .arg(info.numLibraries())
            .arg(info.numItems())
        );
    //adjust size
    mfprtable->resizeColumnsToContents();
}

void EBase::GUI::KicadPathDialog::removeFootprint()
{
    //get row
    auto idx=mfprtable->selectionModel()->selectedIndexes();
    if(idx.size()<1)return;
    int row=idx[0].row();
    //delete row
    mfprmodel->removeRows(row,1);
}

