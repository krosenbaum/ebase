// Electric Base App: KiCAD search path dialog
// (c) Konrad Rosenbaum, 2024
// protected under GNU GPL v.3 or at your option any newer

#pragma once

#include <QDialog>

class QTableView;
class QStandardItemModel;

namespace EBase::Project { class Project; class KicadCache; }

namespace EBase::GUI {

class KicadPathDialog : public QDialog
{
    Q_OBJECT
public:
    enum ShowTab { ShowSymbols, ShowFootprints };
    explicit KicadPathDialog(EBase::Project::Project*,ShowTab,QWidget*p=nullptr);
    ~KicadPathDialog();

public slots:
    void savePathes();

private slots:
    void addSymbol();
    void addSymbolDir();
    void addFootprint();
    void removeSymbol();
    void removeFootprint();

private:
    EBase::Project::Project*mproject;
    EBase::Project::KicadCache*mcache;
    QTableView*msymtable,*mfprtable;
    QStandardItemModel*msymmodel,*mfprmodel;

    void initTables();
    void addSymbolPath(QString);
    void addFootprintPath(QString);
};

//END of namespace
}
