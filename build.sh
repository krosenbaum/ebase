#!/bin/bash

SDIR="$(dirname $0)"

export Qt6_DIR=$(qmake -query QT_INSTALL_PREFIX)
export CMAKE_MODULE_PATH=$Qt6_DIR/lib/cmake/Qt6:$CMAKE_MODULE_PATH
export CMAKE_PREFIX_PATH=$Qt6_DIR:$CMAKE_PREFIX_PATH
cmake "$SDIR" "$@"
make
