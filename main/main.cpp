// Electric Base App: Main
// (c) Konrad Rosenbaum, 2024
// protected under GNU GPL v.3 or at your option any newer

#include <QApplication>
#include <QIcon>

#include "gui/mwin.h"


///main entry point
int main(int ac,char**av)
{
    QApplication::setOrganizationName("Wizzard.Industries");
    QApplication::setOrganizationDomain("wizzard.industries");
    QApplication::setApplicationDisplayName("Electric Base");
    QApplication::setApplicationName("ElectricBase");
    QApplication::setWindowIcon(QIcon(":/images/database.svgz"));

    QApplication app(ac,av);

    auto*mw = new EBase::GUI::MainWindow;
    mw->show();

    return app.exec();
}

/** \mainpage
 * ElectricBase is organized into multiple namespaces:
 * * EBase - main namespace that contains everything
 * * EBase::ODBC - ODBC utilities used to access ODBC system settings and testing whether KiCAD will be able to access the database
 * * EBase::DB - direct DB acces for EBase - based on QtSql
 * * EBase::GUI - all GUI related classes
 * * EBase::Project - KiCAD library and project file parsing and access classes
 *
 * Upon startup EBase sets a few basic settings and opens a EBase::GUI::MainWindow. Everything else is launched from there.
 */
