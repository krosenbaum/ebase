// Electric Base App: DB SQL Dialect
// (c) Konrad Rosenbaum, 2024
// protected under GNU GPL v.3 or at your option any newer

#pragma once

#include <memory>

#include <QStringList>

class QSqlDatabase;

namespace EBase::DB {

namespace Private { class DialectImpl; }

class DBDialect;

class ColumnInfo
{
public:
    ColumnInfo()=default;
    ColumnInfo(const ColumnInfo&)=default;
    ColumnInfo(ColumnInfo&&)=default;

    ColumnInfo& operator=(const ColumnInfo&)=default;
    ColumnInfo& operator=(ColumnInfo&&)=default;

    QString columnName()const{return mname;}
    QString columnType()const{return mtype;}
    bool isNotNull()const{return misnn;}
    bool isPrimary()const{return mispk;}
    QString defaultValue()const{return mdef;}
private:
    QString mname,mtype,mdef;
    bool misnn,mispk;

    friend class DBDialect;
    ColumnInfo(QString nm,QString tp,QString df,bool nn,bool pk):mname(nm),mtype(tp),mdef(df),misnn(nn),mispk(pk){}
};

class DBDialect
{
    Private::DialectImpl*mdimpl=nullptr;
public:
    enum class Dialect { NoSQL, StdSQL, MySQL, PgSQL, SQLite };
    explicit DBDialect(Dialect d=Dialect::StdSQL);
    DBDialect(const DBDialect&)=default;

    DBDialect& operator=(const DBDialect&)=default;

    static Dialect determineDialect(QSqlDatabase);

    QString dialectName()const;

    QString databaseName(QSqlDatabase)const;

    QStringList tableNames(QSqlDatabase)const;
    QStringList viewNames(QSqlDatabase)const;

    QList<ColumnInfo> tableColumns(QSqlDatabase,QString)const;
    QStringList tableColumnNames(QSqlDatabase,QString)const;
};

//END of namespace
}
