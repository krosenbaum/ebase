// Electric Base App: DB Access
// (c) Konrad Rosenbaum, 2024
// protected under GNU GPL v.3 or at your option any newer

#pragma once

#include <QObject>

class QWidget;
class QSqlDatabase;

namespace EBase {

namespace Project { class Project; }

namespace DB {

class DBDialect;

///Database Access Interface for EBase, unifies all access methods and handles SQL dialects.
class DBAccessor : public QObject
{
    Q_OBJECT
public:
    explicit DBAccessor(QObject*p);
    ~DBAccessor();

    ///returns the internal connection name used with QtSql (not human-readable)
    QString connectionName()const{return mdbcname;}
    ///Returns a (multi-line) string describing the current DB connection or empty string if there is no connection.
    QString dbInfo()const;

    ///Returns a reference to the current database (invalid if not open).
    QSqlDatabase db()const;
    ///Returns a reference to the SQL dialect being used.
    DBDialect sqlDialect()const;

    ///Open the DB connection
    ///\param project the project to take information from, if NULL: request gets ignored
    ///\param parentWindow if not NULL: display errors on screen atop parentWindow
    ///\note if there is currently a connection, it closes the connection before opening a fresh one
    void open(EBase::Project::Project*project,QWidget*parentWindow);
    ///True if the DB is currently open.
    bool isOpen()const;
    ///Close the database connection. Invalidates all open queries.
    void close();

private:
    ///Connection name (used by QtSql connection pool)
    QString mdbcname;
    ///Identified connection SQL dialect, see \ref sqlDialect
    mutable int mdbdialect=-1;

    ///Helper to log errors.
    ///Always logs to qDebug, whether something is shown to the user depends on arguments.
    ///\param pwid parent widget for the message box, if nullptr then no message box is shown
    ///\param errstr the error being logged and/or shown.
    void errorBox(QWidget*pwid,QString errstr)const;
};

//END of namespaces
}}
