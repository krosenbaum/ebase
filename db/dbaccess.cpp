// Electric Base App: DB Access
// (c) Konrad Rosenbaum, 2024
// protected under GNU GPL v.3 or at your option any newer

#include "dbaccess.h"
#include "dbinfo.h"
#include "dbdialect.h"
#include "project/project.h"

#include <QDebug>
#include <QMessageBox>
#include <QUuid>
#include <QSqlError>
#include <QSqlDatabase>

using namespace EBase::DB;
using namespace EBase::Project;

DBAccessor::DBAccessor(QObject*p)
:QObject(p),mdbcname("eDB/"+QUuid::createUuid().toString(QUuid::WithoutBraces))
{
    qDebug()<<"Created DB Accessor"<<mdbcname;
}

DBAccessor::~DBAccessor()
{
    QSqlDatabase::removeDatabase(mdbcname);
    qDebug()<<"Dropping DB Accessor"<<mdbcname;
}

void DBAccessor::errorBox(QWidget*pwid, QString err) const
{
    if(pwid){
        QMessageBox::warning(pwid,tr("Warning"),err);
    }
    qDebug()<<"Error while opening DB:"<<err;
}

void DBAccessor::open(EBase::Project::Project*prj,QWidget*pwid)
{
    if(prj==nullptr)return;
    //close old connection
    QSqlDatabase::removeDatabase(mdbcname);
    mdbdialect=-1;
    //ODBC or AltDB?
    auto dbset=prj->dbSettings();
    if(!dbset.isValid()){
        errorBox(pwid,tr("Invalid Database Settings. Unable to connect."));
        return;
    }
    if(dbset.useAlternate()){
        //try to get DB driver
        const QString drvName=dbset.driverName();
        if(drvName.isEmpty() || !QSqlDatabase::isDriverAvailable(drvName)){
            errorBox(pwid,tr("Database driver '%1' is not available. Unable to connect.").arg(drvName));
            return;
        }
        auto dbc=QSqlDatabase::addDatabase(drvName,mdbcname);
        //configure
        dbc.setDatabaseName(dbset.databaseName());
        // cfg: remote connection
        const auto host=dbset.hostName();
        const auto port=dbset.portNumber();
        if(!host.isEmpty())dbc.setHostName(host);
        if(port>0)dbc.setPort(port);
        // cfg: login
        const auto usr=dbset.username();
        const auto pwd=dbset.password();
        if(!usr.isEmpty())dbc.setUserName(usr);
        if(!pwd.isEmpty())dbc.setPassword(pwd);
        // cfg: options
        const auto opt=dbset.connectOptions();
        if(!opt.isEmpty())dbc.setConnectOptions(opt);
        //open
        if(dbc.open()){
            qDebug()<<"Opened DB"<<dbc.databaseName();
        }else{
            errorBox(pwid,tr("Unable to open database: %1").arg(dbc.lastError().text()));
        }
    }else{
        //using ODBC
        auto odbcset=prj->odbcSettings();
        if(!odbcset.isValid()){
            errorBox(pwid,tr("ODBC settings are not valid, cannot open ODBC connection."));
            return;
        }
        const QString odbcDrv=DBInfo::typeToDriver(DBInfo::DBType::ODBC);
        if(!QSqlDatabase::isDriverAvailable(odbcDrv)){
            errorBox(pwid,tr("ODBC driver is not available."));
            return;
        }
        //init
        auto dbc=QSqlDatabase::addDatabase(odbcDrv,mdbcname);
        if(odbcset.useDSN()){
            dbc.setDatabaseName(odbcset.dsn());
            const QString usr=odbcset.username();
            const QString pwd=odbcset.password();
            if(!usr.isEmpty())dbc.setUserName(usr);
            if(!pwd.isEmpty())dbc.setPassword(pwd);
        }else{
            dbc.setDatabaseName(odbcset.connectionString());
        }
        const auto tmout=odbcset.timeout();
        dbc.setConnectOptions(
            QString("SQL_ATTR_LOGIN_TIMEOUT=%1;SQL_ATTR_CONNECTION_TIMEOUT=%1;SQL_ATTR_ODBC_VERSION=SQL_OV_ODBC3;")
            .arg(tmout>0.0?tmout:10.)
        );
        //try to open
        if(dbc.open()){
            qDebug()<<"Opened ODBC DB"<<dbc.databaseName();
        }else{
            errorBox(pwid,tr("Unable to open ODBC database: %1").arg(dbc.lastError().text()));
        }
    }
}

DBDialect DBAccessor::sqlDialect() const
{
    //get dialect identified
    if(mdbdialect<0){
        auto d=db();
        mdbdialect=(int)DBDialect::determineDialect(d);
    }
    //return object
    return DBDialect(DBDialect::Dialect(mdbdialect));
}

bool DBAccessor::isOpen() const
{
    auto mdb=QSqlDatabase::database(mdbcname,false);
    return mdb.isValid() && mdb.isOpen();
}

void DBAccessor::close()
{
    QSqlDatabase::removeDatabase(mdbcname);
}

QSqlDatabase DBAccessor::db() const
{
    return QSqlDatabase::database(mdbcname);
}

QString DBAccessor::dbInfo() const
{
    auto dbc=QSqlDatabase::database(mdbcname);
    if(!dbc.isOpen())return QString();
    QString hspec;
    if(!dbc.hostName().isEmpty()){
        hspec=dbc.hostName();
        if(hspec.contains(':'))hspec="["+hspec+"]";//workaround for IPv6 addr
        if(dbc.port()>0)hspec+=":"+QString::number(dbc.port());
    }
    return tr("DB: %1 (%2)\nHost: %3\nUser: %4","1=db name, 2=driver, 3=host:port, 4=user")
        .arg(dbc.databaseName())
        .arg(dbc.driverName())
        .arg(hspec)
        .arg(dbc.userName());
}
