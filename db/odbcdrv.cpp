// Electric Base App: DB Basics
// (c) Konrad Rosenbaum, 2024
// protected under GNU GPL v.3 or at your option any newer

//Windows needs some more input for ODBC to work
#if defined(__MINGW32__) || defined(_MSC_VER)
#define UNICODE 1
#include <windows.h>
#endif
//ODBC libraries
#include <sql.h>
#include <sqlext.h>
#include <odbcinst.h>

//low level C
#include <string.h>

//myself
#include "odbcdrv.h"

//Qt
#include <QDebug>
#include <QSettings>

using namespace EBase;
using namespace EBase::ODBC;

// ////////////////////////////
// WChar helper class

namespace EBase { namespace ODBC {
///\internal \brief private ODBC internal stuff, not available outside ODBC
namespace Private {
///\internal \brief helper class to convert ODBC Unicode strings to QString and vice versa.
///\param asize size of the character buffer in 16bit characters
template<SQLSMALLINT asize>
class Utf16CharArray
{
    SQLWCHAR mdata[asize+1];
public:
    ///instantiate buffer array with all chars set to zero
    Utf16CharArray(){memset(mdata,0,sizeof(mdata));}
    ///instantiate buffer array and copy contents from string
    ///\param s start string
    ///\note if s is shorter than the array, then the remainder is filled with zeros, if it is longer then the contents are truncated
    Utf16CharArray(const QString&s){
        memset(mdata,0,sizeof(mdata));
        const auto sz=s.size();
        if(sz>0)
            memcpy(mdata,s.utf16(),sz<asize?sz:asize);
    }

    ///returns a pointer to the low level array for ODBC
    SQLWCHAR*data(){return mdata;}
    ///returns a pointer to the low level array for ODBC
    const SQLWCHAR*data()const{return mdata;}

    ///returns the (fixed) size of the array
    SQLSMALLINT size()const{return asize;}

    ///converts the array to a QString
    ///\param ssize the amount of chars to copy to the string, if less than 0 or omitted then up to the first NULL character, if it is larger than the array size() then only the array contents are copied and the string ends up short
    QString toString(int ssize=-1)const{
        static_assert(sizeof(char16_t)==sizeof(SQLWCHAR));
        if(ssize==0)return QString();
        if(ssize<0)return QString::fromUtf16((char16_t*)mdata);
        else return QString::fromUtf16((char16_t*)mdata,ssize<asize?ssize:asize);
    }

    ///resets the buffer to all zero
    void resetMem()
    {
        memset(mdata,0,sizeof(mdata));
    }
};
}}}
using namespace EBase::ODBC::Private;

// ////////////////////////////
// Interface class

static SQLHENV hdlEnv=0;
static bool haveEnv=false;

//default version
#ifdef SQL_OV_ODBC3_80
#define USE_ODBC_VERSION SQL_OV_ODBC3_80
#define USE_ODBC_VERSION_STR "3.80"
#else
#define USE_ODBC_VERSION SQL_OV_ODBC3
#define USE_ODBC_VERSION_STR "3.0"
#endif

//default buffer size
constexpr int odbcBufferSize = 1024;

QString ODBCInterface::odbcVersion()
{
    return QString("%1.%2").arg(ODBCVER>>8).arg(ODBCVER&0xFF,2,16,(QChar)'0');
}


ODBCInterface::ODBCInterface()
{
    if(!haveEnv){
        if(SQL_SUCCEEDED(SQLAllocHandle(SQL_HANDLE_ENV,SQL_NULL_HANDLE,&hdlEnv))){
            haveEnv=true;
            qDebug()<<"ODBC Environment allocated.";
            if(SQL_SUCCEEDED(SQLSetEnvAttr(hdlEnv,SQL_ATTR_ODBC_VERSION,(void*)USE_ODBC_VERSION,0)))
                qDebug()<<"Set ODBC Behavior to"<<USE_ODBC_VERSION_STR;
            else
                qDebug()<<"Setting ODBC Behavior may have failed.";
        }else{
            qDebug()<<"Unable to get ODBC Environment. ODBC will not work.";
        }
    }
}

ODBCInterface::~ODBCInterface()
{
    if(haveEnv){
        SQLFreeHandle(SQL_HANDLE_ENV,hdlEnv);
        haveEnv=false;
        qDebug()<<"ODBC Environment free'd.";
    }
}

ODBCInterface & ODBCInterface::instance()
{
    static ODBCInterface ifc;
    return ifc;
}

bool ODBCInterface::odbcAvailable() const
{
    return haveEnv;
}

QStringList ODBCInterface::driverNames() const
{
    if(!haveEnv)
        return QStringList();

    //get info from ODBC, we are only interested in names
    SQLUSMALLINT dir=SQL_FETCH_FIRST;
    SQLSMALLINT drvret;
    Utf16CharArray<odbcBufferSize> drvname;
    QStringList ret;
    while(SQL_SUCCEEDED(SQLDriversW(hdlEnv, dir,
        drvname.data(),drvname.size(),&drvret,
        nullptr,0,nullptr)))
    {
        dir=SQL_FETCH_NEXT;
        QString dn=drvname.toString(drvret);
        ret.append(dn);
    }
    //return list of names
    return ret;
}

DriverInfo ODBCInterface::driverInfo(QString find) const
{
    if(!haveEnv)
        return DriverInfo();

    SQLUSMALLINT dir=SQL_FETCH_FIRST;
    SQLSMALLINT attrret,drvret;
    Utf16CharArray<odbcBufferSize> drvname,drvattr;
    QStringList ret;
    while(SQL_SUCCEEDED(SQLDriversW(hdlEnv, dir,
        drvname.data(),drvname.size(),&drvret,
        drvattr.data(),drvattr.size(),&attrret)))
    {
        dir=SQL_FETCH_NEXT;
        QString dn=drvname.toString(drvret);
        if(dn==find)
            return DriverInfo(dn,drvattr.toString(attrret));
    }
    //not found: return empty instance
    return DriverInfo();
}

QList<DriverInfo> ODBCInterface::driverInfos() const
{
    if(!haveEnv)
        return QList<DriverInfo>();

    SQLUSMALLINT dir=SQL_FETCH_FIRST;
    SQLSMALLINT attrret,drvret;
    Utf16CharArray<odbcBufferSize> drvname,drvattr;
    QList<DriverInfo> ret;
    while(SQL_SUCCEEDED(SQLDriversW(hdlEnv, dir,
        drvname.data(),drvname.size(),&drvret,
        drvattr.data(),drvattr.size(),&attrret)))
    {
        dir=SQL_FETCH_NEXT;
        ret.append(DriverInfo(drvname.toString(drvret),drvattr.toString(attrret)));
    }
    //return data
    return ret;
}

static inline SQLUSMALLINT dsndir(ODBCInterface::DSNclass c)
{
    switch(c){
        case ODBCInterface::DSNclass::System: return SQL_FETCH_FIRST_SYSTEM;
        case ODBCInterface::DSNclass::User:   return SQL_FETCH_FIRST_USER;
        default:                              return SQL_FETCH_FIRST;
    }
}

DSNInfo ODBCInterface::dsnInfo(QString find) const
{
    if(!haveEnv || find.isNull())
        return DSNInfo();//TODO

    Utf16CharArray<odbcBufferSize> dsn,desc;
    SQLSMALLINT dsnret,descret;
    SQLUSMALLINT dir=dsndir(DSNclass::All);

    while(SQL_SUCCEEDED(SQLDataSourcesW(hdlEnv,dir,
        dsn.data(),dsn.size(),&dsnret,
        desc.data(),desc.size(),&descret)))
    {
        dir=SQL_FETCH_NEXT;
        const QString nm=dsn.toString(dsnret);
        if(nm==find)
            return DSNInfo(nm,desc.toString(descret));
    }

    //not found
    return DSNInfo();
}

QStringList ODBCInterface::dsNames(DSNclass dc) const
{
    QStringList ret;
    if(!haveEnv)return ret;

    Utf16CharArray<odbcBufferSize> dsn,desc;
    SQLSMALLINT dsnret,descret;
    SQLUSMALLINT dir=dsndir(dc);

    while(SQL_SUCCEEDED(SQLDataSourcesW(hdlEnv,dir,
        dsn.data(),dsn.size(),&dsnret,
        nullptr,0,nullptr)))
    {
        dir=SQL_FETCH_NEXT;
        ret.append(dsn.toString(dsnret));
    }

    return ret;
}

QList<DSNInfo> ODBCInterface::dsnInfos(DSNclass dc) const
{
    QList<DSNInfo>ret;
    if(!haveEnv)return ret;

    Utf16CharArray<odbcBufferSize> dsn,desc;
    SQLSMALLINT dsnret,descret;
    SQLUSMALLINT dir=dsndir(dc);

    while(SQL_SUCCEEDED(SQLDataSourcesW(hdlEnv,dir,
        dsn.data(),dsn.size(),&dsnret,
        desc.data(),desc.size(),&descret)))
    {
        dir=SQL_FETCH_NEXT;
        ret.append(DSNInfo(dsn.toString(dsnret),desc.toString(descret)));
    }

    return ret;
}

bool ODBCInterface::installUserDSN(QString dsn, QString driver, const QMap<QString, QString>& parameters)
{
    return false;//TODO
}

ODBCconnectionResult ODBCInterface::testConnection(const ODBCconnectionSettings&set)
{
    return ODBCconnectionResult(set);
}

static const QString loginTimeoutKey{"ODBC/logintimeout"};

int ODBCInterface::connectionLoginTimeout()
{
    return QSettings().value(loginTimeoutKey,(int)SQL_LOGIN_TIMEOUT_DEFAULT).toInt();
}

void ODBCInterface::setConnectionLoginTimeout(int t)
{
    QSettings().setValue(loginTimeoutKey,t>0?t:(int)SQL_LOGIN_TIMEOUT_DEFAULT);
}


ODBCconnectionResult::ODBCconnectionResult(const ODBCconnectionSettings& set)
:msettings(set)
{
    if(!haveEnv){
        qDebug()<<"ODBC testConnection called, but no ODBC Environment is allocated.";
        mcode=ODBCconnectionResult::ReturnCode::NoEnvironment;
        return;
    }
    if(set.connectionMode()==ODBCconnectionSettings::ConnectionMode::NoConnect){
        qDebug()<<"Cannot connect to a NoConnect DB spec.";
        mcode=ODBCconnectionResult::ReturnCode::InvalidSettings;
        return;
    }
    //try to connect
    SQLHDBC dbc;
    const SQLHWND hwnd=0;//see note below.
    const intptr_t logintimeout=ODBCInterface::connectionLoginTimeout();
    QString connstr;
    if(set.isConnectByDSN()){
        connstr = QString("DSN=%1;").arg(set.dsn());
        if(set.hasLoginData())
            connstr += QString("UID=%1;PWD=%2;").arg(set.username()).arg(set.password());
    }
    if(set.isConnectByString()){
        connstr = set.connectionString();
    }
    SQLAllocHandle(SQL_HANDLE_DBC, hdlEnv, &dbc);
    SQLSetConnectAttr(dbc,SQL_LOGIN_TIMEOUT,reinterpret_cast<SQLPOINTER>(logintimeout),0);
    auto ret=SQLDriverConnectW(dbc,hwnd,(SQLWCHAR*)connstr.utf16(),SQL_NTS, nullptr,0, nullptr, SQL_DRIVER_NOPROMPT);
    if(SQL_SUCCEEDED(ret)){
        mcode=ODBCconnectionResult::ReturnCode::Successful;
        getTables(dbc,"TABLE",mtables);
        getTables(dbc,"VIEW",mviews);
    }else{
        mcode=ODBCconnectionResult::ReturnCode::ConnectFailed;
        //get error records
        getErrorRecords(dbc,SQL_HANDLE_DBC);
    }
    SQLFreeHandle(SQL_HANDLE_DBC, dbc);
    //done.
}

//NOTE on hwnd and SQLDriverConnectW:
//This parameter decides whether the ODBC driver can show a popup window to ask for missing parameters.
//KiCAD uses NanODBC, which sets this to NULL - not allowing popups.
//If popups are to be allowed after all then this should be !=0 on Unix and GetDesktopWidget() on Win32.

void ODBCconnectionResult::getTables(void*dbc,QString ttype,QList<TableInfo>&list)
{
        //statement handle
        SQLHSTMT stmt;
        SQLAllocHandle(SQL_HANDLE_STMT, dbc, &stmt);

        //bind results
        Utf16CharArray<odbcBufferSize> rbuf[5];
        SQLLEN rlen[5]={0,0,0,0,0};
        for(int i=0;i<5;i++)
            SQLBindCol(stmt,i+1,SQL_C_WCHAR,rbuf[i].data(),rbuf[i].size(),rlen+i);
        //go on to get tables (any catalog, any schema, any table name)
        auto ret=SQLTablesW(stmt, nullptr,0, nullptr,0, nullptr,0, (SQLWCHAR*)ttype.utf16(),SQL_NTS);
        if(SQL_SUCCEEDED(ret)){
            while(SQL_SUCCEEDED(ret=SQLFetch(stmt))){
                //copy data to list
                list.append(TableInfo(
                    rlen[0]==SQL_NULL_DATA?TableInfo::nullValue():rbuf[0].toString(rlen[0]),
                    rlen[1]==SQL_NULL_DATA?TableInfo::nullValue():rbuf[1].toString(rlen[1]),
                    rlen[2]==SQL_NULL_DATA?TableInfo::nullValue():rbuf[2].toString(rlen[2]),
                    rlen[3]==SQL_NULL_DATA?TableInfo::nullValue():rbuf[3].toString(rlen[3]),
                    rlen[4]==SQL_NULL_DATA?TableInfo::nullValue():rbuf[4].toString(rlen[4])
                ));
                //reset for next
                for(int i=0;i<5;i++){
                    rlen[i]=0;
                    rbuf[i].resetMem();
                }
            }
        }else{
            qDebug()<<"Unable to get tables of type"<<ttype;
            getErrorRecords(stmt,SQL_HANDLE_STMT);
        }

        //done with statement
        SQLFreeHandle(SQL_HANDLE_STMT,stmt);
}

void ODBCconnectionResult::getErrorRecords(void*hdl, int hdltype)
{
    Utf16CharArray<odbcBufferSize>buffer;
    Utf16CharArray<8>state;
    SQLINTEGER naterr;
    SQLSMALLINT msgsize;
    SQLRETURN ret;
    int rnum=0;
    do{
        ret=SQLGetDiagRecW(hdltype, hdl, ++rnum, state.data(), &naterr, buffer.data(), buffer.size(), &msgsize);
        if(ret==SQL_SUCCESS){
            merrors.append(ErrorRecord(rnum,naterr,state.toString(5),buffer.toString(msgsize)));
        }else if(ret==SQL_SUCCESS_WITH_INFO){
            qDebug()<<"Oops. Lost error message! Needed"<<msgsize<<"characters, only have"<<odbcBufferSize;
            merrors.append(ErrorRecord(rnum,naterr,state.toString(5),"?"));
        }
    }while(SQL_SUCCEEDED(ret));
}


// ////////////////////////////
// Driver Info class

DriverInfo::DriverInfo(QString name, QString attr)
{
    mname=name;
    while(!attr.isEmpty()){
        //split out next one
        const auto idx=attr.indexOf((QChar)0);
        if(idx==0){attr=attr.mid(1);continue;}
        QString one=(idx<0) ? attr : attr.left(idx);
        if(idx>0)attr=attr.mid(idx+1);
        //split name/value
        const auto idx2=one.indexOf('=');
        if(idx2<0)mattr.insert(one,QString());
        else mattr.insert(one.left(idx2),one.mid(idx2+1));
    }
}

QString DriverInfo::description() const
{
    for(const auto&key:mattr.keys())
        if(key.compare("description",Qt::CaseInsensitive)==0)
            return mattr.value(key);
    //not found
    return QString();
}

// ////////////////////////////
// DSN Info class

// completely inline. See header.
