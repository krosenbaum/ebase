// Electric Base App: DB Basics
// (c) Konrad Rosenbaum, 2024
// protected under GNU GPL v.3 or at your option any newer

#include <QCoreApplication>
#include <QDebug>
#include <QSqlDatabase>
#include <QSqlDriver>
#include <QSqlError>

#include "dbinfo.h"

EBase::DB::DBInfo::DBType EBase::DB::DBInfo::driverToType(const QString&drv)
{
    if(drv=="QDB2")return IbmDB2;
    if(drv=="QIBASE")return InterBase;
    if(drv=="QMARIADB")return MariaDB;
    if(drv=="QMYSQL")return MySQL;
    if(drv=="QOCI")return OCI;
    if(drv=="QODBC")return ODBC;
    if(drv=="QPSQL")return PostgreSQL;
    if(drv=="QSQLITE")return SQLite;
    if(drv=="QMIMER")return Mimer;
    //unknown
    return UnknownDB;
}

QString EBase::DB::DBInfo::typeToDriver(EBase::DB::DBInfo::DBType dbt)
{
    switch(dbt){
        case UnknownDB:return QString();
        case IbmDB2:return "QDB2";
        case InterBase:return "QIBASE";
        case MySQL:return "QMYSQL";
        case MariaDB:return "QMARIADB";
        case OCI:return "QOCI";
        case ODBC:return "QODBC";
        case PostgreSQL:return "QPSQL";
        case SQLite:return "QSQLITE";
        case Mimer:return "QMIMER";
    }
    //unreachable
    Q_UNREACHABLE();
    qDebug()<<"OUCH! unknown DB type"<<(int)dbt;
    return QString();
}

QString EBase::DB::DBInfo::typeToHumanReadable(EBase::DB::DBInfo::DBType dbt)
{
    switch(dbt){
        case UnknownDB: return QCoreApplication::translate("DBInfo","Unknown DB");
        case IbmDB2:    return QCoreApplication::translate("DBInfo","IBM DB2");
        case InterBase: return QCoreApplication::translate("DBInfo","Borland InterBase");
        case MySQL:     return QCoreApplication::translate("DBInfo","MySQL");
        case MariaDB:   return QCoreApplication::translate("DBInfo","MariaDB");
        case OCI:       return QCoreApplication::translate("DBInfo","Oracle OCI");
        case ODBC:      return QCoreApplication::translate("DBInfo","ODBC");
        case PostgreSQL:return QCoreApplication::translate("DBInfo","PostgreSQL");
        case SQLite:    return QCoreApplication::translate("DBInfo","SQLite v.3");
        case Mimer:     return QCoreApplication::translate("DBInfo","Mimer SQL");
    }
    //unreachable
    Q_UNREACHABLE();
    qDebug()<<"OUCH! unknown DB type"<<(int)dbt;
    return QCoreApplication::translate("DBInfo","Unknown DB");
}

QList<EBase::DB::DBInfo> EBase::DB::DBInfo::allAvailable()
{
    QList<DBInfo>ret;
    for(const auto&d:QSqlDatabase::drivers())
        ret.append(DBInfo(d));
    return ret;
}

QList<EBase::DB::DBInfo> EBase::DB::DBInfo::allUsable()
{
    QList<DBInfo>ret;
    for(const auto&d:QSqlDatabase::drivers()){
        DBInfo dbi(d);
        if(dbi.isSupported())
            ret.append(dbi);
    }
    return ret;
}

bool EBase::DB::DBInfo::driverAvailable(QString d)
{
    return QSqlDatabase::drivers().contains(d);
}

bool EBase::DB::DBInfo::driverAvailable(EBase::DB::DBInfo::DBType t)
{
    return driverAvailable(typeToDriver(t));
}



bool EBase::DB::DBInfo::isAvailable() const
{
    return QSqlDatabase::drivers().contains(mdriver);
}

bool EBase::DB::DBInfo::isSupported() const
{
    switch(type()){
        case ODBC:
        case MariaDB:
        case MySQL:
        case PostgreSQL:
        case SQLite:
            return true;
        default:
            return false;
    }
}

