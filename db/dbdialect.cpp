// Electric Base App: DB SQL Dialect
// (c) Konrad Rosenbaum, 2024
// protected under GNU GPL v.3 or at your option any newer

#include "dbdialect.h"
#include "dbinfo.h"

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlField>
#include <QDebug>

namespace EBase::DB::Private {

///Base dialect: no SQL, just the API returning nothing - this forces the implementation to use Qt only
class DialectImpl
{
public:
    DialectImpl(){}
    virtual ~DialectImpl(){}

    virtual QString dName()const{return "Not SQL";}
    virtual QString getDatabase()const{return QString();}
};

static DialectImpl noSql;

///Standard SQL: assumes information_schema does exist and is compliant.
class StdSqlDI : public DialectImpl
{
public:
    virtual QString dName()const override{return "standard SQL";}
    virtual QString getDatabase()const override{return "SELECT database()";}
};

static StdSqlDI stdSql;

///MySQL dialect
class MySqlDI : public StdSqlDI
{
public:
    QString dName() const override{return "MySQL";}
};

static MySqlDI mySql;

///PostgreSQL dialect
class PgSqlDI : public StdSqlDI
{
public:
    QString dName() const override{return "PostgreSQL";}
    QString getDatabase()const override{return "SELECT current_database()";}
};

static PgSqlDI pgSql;

///SQLite dialect
class SqLiteDI : public StdSqlDI
{
public:
    QString dName() const override{return "SQLite";}
    QString getDatabase()const override{return QString();}
};

static SqLiteDI sqLite;

//END of Private
}


using namespace EBase::DB;

EBase::DB::DBDialect::DBDialect(Dialect d)
{
    switch(d){
        case Dialect::MySQL:  mdimpl= &Private::mySql; break;
        case Dialect::PgSQL:  mdimpl= &Private::pgSql; break;
        case Dialect::SQLite: mdimpl= &Private::sqLite; break;
        case Dialect::StdSQL: mdimpl= &Private::stdSql; break;
        default: mdimpl= &Private::noSql; break;
    }
}

EBase::DB::DBDialect::Dialect EBase::DB::DBDialect::determineDialect(QSqlDatabase db)
{
    //stage 0: ask driver
    switch(DBInfo::driverToType(db.driverName())){
        //MySQL/MariaDB
        case DBInfo::DBType::MariaDB:
        case DBInfo::DBType::MySQL:
            return Dialect::MySQL;
        //PostgreSQL
        case DBInfo::DBType::PostgreSQL:
            return Dialect::PgSQL;
        //SQLite
        case DBInfo::DBType::SQLite:
            return Dialect::SQLite;
        //ODBC and unknown: go below
        case DBInfo::DBType::ODBC:
        case DBInfo::DBType::UnknownDB:
            break;
        //All other supported drivers: go with standard SQL and hope for the best
        case DBInfo::DBType::IbmDB2:
        case DBInfo::DBType::InterBase:
        case DBInfo::DBType::OCI:
        case DBInfo::DBType::Mimer:
            return Dialect::StdSQL;
    }
    // no connection, no check
    if(!db.isOpen())return Dialect::NoSQL;

    //stage 1: get @@version variable -> MySQL and a few commercial servers
    {
        QSqlQuery q(db);
        if(q.exec("SELECT @@VERSION") && q.next()){
            const QString v=q.value(0).toString().toLower();
            //MySQL variants
            if(v.contains("mysql") || v.contains("mariadb")){
                qDebug()<<"identified MySQL dialect:"<<v;
                return Dialect::MySQL;
            }
            //MS SQL
            if(v.contains("microsoft")){
                qDebug()<<"yikes, identified MS SQL Server:"<<v;
                qDebug()<<"using standard SQL and hoping for the best...";
                return Dialect::StdSQL;
            }
        }
    }

    //stage 2: get version() -> PostgreSQL and MySQL, some others maybe?
    {
        QSqlQuery q(db);
        if(q.exec("SELECT VERSION()") && q.next()){
            const QString v=q.value(0).toString().toLower();
            //MySQL variants
            if(v.contains("mysql") || v.contains("mariadb")){
                qDebug()<<"identified MySQL dialect:"<<v;
                return Dialect::MySQL;
            }
            //PostgreSQL
            if(v.contains("postgres")){
                qDebug()<<"identified PostgreSQL dialect:"<<v;
                return Dialect::PgSQL;
            }
        }
    }

    //stage 3: get sqlite_version() -> SQLite only
    {
        QSqlQuery q(db);
        if(q.exec("SELECT sqlite_version()") && q.next()){
            const QString v=q.value(0).toString();
            if(!v.isEmpty()){
                qDebug()<<"identified SQLite dialect:"<<v;
                return Dialect::SQLite;
            }
        }
    }

    //stage 4: check for Oracle and complain
    {
        QSqlQuery q(db);
        if(q.exec("select banner from v$version")){
            while(q.next()){
                const QString b=q.value(0).toString();
                if(b.toLower().contains("oracle")){
                    qDebug()<<"yikes, Oracle detected:"<<b;
                    qDebug()<<"using standard SQL and hoping for the best";
                    return Dialect::StdSQL;
                }
            }
        }
    }

    //stage 5: check it is standard SQL
    {
        QSqlQuery q(db);
        if(q.exec("SELECT * FROM information_schema.tables")){
            qDebug()<<"information_schema exists, assuming standard SQL";
            return Dialect::StdSQL;
        }else{
            qDebug()<<"SQL like, but information_schema does not exist, giving up";
            return Dialect::NoSQL;
        }
    }
}

QString EBase::DB::DBDialect::dialectName() const
{
    return mdimpl->dName();
}

QString EBase::DB::DBDialect::databaseName(QSqlDatabase db) const
{
    //get query string
    QString qs=mdimpl->getDatabase();
    //no string: fall back
    if(qs.isEmpty())return db.databaseName();
    //ask
    QSqlQuery q(db);
    if(q.exec(qs) && q.next()){
        return q.value(0).toString();
    }
    //no response
    return db.databaseName();
}

QStringList EBase::DB::DBDialect::tableNames(QSqlDatabase db) const
{
    return db.tables(QSql::Tables);
}

QStringList EBase::DB::DBDialect::viewNames(QSqlDatabase db) const
{
    return db.tables(QSql::Views);
}

QList<EBase::DB::ColumnInfo> EBase::DB::DBDialect::tableColumns(QSqlDatabase db, QString table) const
{
    //TODO
    return QList<EBase::DB::ColumnInfo>();
}

QStringList EBase::DB::DBDialect::tableColumnNames(QSqlDatabase db, QString tname) const
{
    QStringList cols;
    //use a shameful shortcut...
    auto rec=db.record(tname);
    for(int i=0;i<rec.count();i++)
        cols.append(rec.fieldName(i));
    return cols;
}
