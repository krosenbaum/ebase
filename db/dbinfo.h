// Electric Base App: DB Basics
// (c) Konrad Rosenbaum, 2024
// protected under GNU GPL v.3 or at your option any newer

#pragma once

#include <QStringList>

namespace EBase {
///Generic Database access and driver stuff.
namespace DB {

///Encapsulates information about Qt Database drivers.
class DBInfo
{
public:
    ///Known DB types.
    enum DBType{
        ///DB is not known to this app. May or may not work. Also fallback value for "no DB driver found".
        UnknownDB,
        ///IBM DB2 (will probably never be fully supported).
        IbmDB2,
        ///Borland InterBase (rather old, unsupported by this app).
        InterBase,
        ///MySQL (supported as long as it stays compatible with MariaDB).
        MySQL,
        ///MariaDB (default for this app).
        MariaDB,
        ///Oracle database (not supported out of principle).
        OCI,
        ///Generic ODBC interface (can be used as fallback).
        ODBC,
        ///PostgreSQL database (will be supported).
        PostgreSQL,
        ///SQLite file database (will be supported).
        SQLite,
        ///Mimer Database (unsupported, commercial DBMS).
        Mimer
    };

    ///instantiates invalid info object
    DBInfo()=default;
    ///copy constructor
    DBInfo(const DBInfo&)=default;
    ///move constructor
    DBInfo(DBInfo&&)=default;
    ///instantiates info object by driver type
    DBInfo(DBType mytype):mdriver(typeToDriver(mytype)){}
    ///instantiates info object by driver name
    DBInfo(const QString&drivername):mdriver(drivername){}

    ///copy assignment operator
    DBInfo& operator=(const DBInfo&)=default;
    ///move assignment operator
    DBInfo& operator=(DBInfo&&)=default;

    ///returns true if these objects represent the same driver
    bool operator==(const DBInfo&)const=default;

    ///helper to convert Qt driver name to known DB type enum
    static DBType driverToType(const QString&);
    ///helper to convert DB type enum to Qt driver name
    static QString typeToDriver(DBType);
    ///helper to return human readable name for driver
    static QString typeToHumanReadable(DBType);

    ///returns true if Qt knows this driver name as a loadable plugin
    static bool driverAvailable(QString);
    ///returns true if Qt knows this driver type as a loadable plugin
    static bool driverAvailable(DBType);

    ///returns the name of the Qt driver, this is the name used with Qt SQL to make a connection
    QString driver()const{return mdriver;}
    ///returns the DB type of the driver, or UnknownDB if the driver is unknown
    DBType type()const{return driverToType(mdriver);}
    ///returns a human readable name for the driver
    QString humanReadableType()const{return typeToHumanReadable(type());}

    ///returns true if the info object probably represents a valid driver (non-empty)
    bool isValid()const{return !mdriver.isEmpty();}

    ///returns true if the plugin is available
    bool isAvailable()const;
    ///returns true if the program fully supports it
    bool isSupported()const;
    ///returns true if this DB type can actually be used in this session
    bool canBeUsed()const{return isAvailable()&&isSupported();}
    ///returns true if this DB driver is ODBC
    bool isOdbc()const{return type()==ODBC;}

    ///returns info about all available DB drivers in Qt (all plugins that can be loaded)
    static QList<DBInfo> allAvailable();
    ///returns all drivers that are fully supported and available as plugins
    static QList<DBInfo> allUsable();
private:
    ///Qt6 plugin driver name that can be used with Qt SQL
    QString mdriver;
};

//END of namespaces
}}
