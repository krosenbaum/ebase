// Electric Base App: DB Basics
// (c) Konrad Rosenbaum, 2024
// protected under GNU GPL v.3 or at your option any newer

#pragma once

#include <QStringList>
#include <QMap>

namespace EBase {
///ODBC Database Driver classes.
///These classes use the ODBC or UnixODBC driver directly without involving QtSql.
///Used for testing KiCAD connections and setting up DSNs in the system.
///Direct DB access for table structure and content is routed through QtSql, even when using ODBC.
namespace ODBC {

class ODBCInterface;

///Encapsulates info about an ODBC driver.
class DriverInfo
{
    friend class ODBCInterface;
    ///used internally
    DriverInfo(QString name,QString attr);

    QString mname;
    QMap<QString,QString> mattr;
public:
    ///instantiates invalid driver info
    DriverInfo(){}
    ///copy constructor
    DriverInfo(const DriverInfo&)=default;
    ///move constructor
    DriverInfo(DriverInfo&&)=default;

    ///copy assignment
    DriverInfo& operator=(const DriverInfo&)=default;
    ///move assignment
    DriverInfo& operator=(DriverInfo&&)=default;

    ///returns true if the objects represent the same driver entry in ODBC
    bool operator==(const DriverInfo&)const = default;

    ///returns the name of the driver (as referenced from DSNs)
    QString driverName()const{return mname;}
    ///returns the description of the driver (as returned by ODBC)
    QString description()const;
    ///returns all configuration key-value pairs of the driver
    QMap<QString,QString> attributes()const{return mattr;}
};

///Encapsulates info about a DSN.
class DSNInfo
{
    friend class ODBCInterface;
    QString mname,mdescr;
    ///used internally
    DSNInfo(QString n,QString d):mname(n),mdescr(d){}
public:
    ///instantiates invalid info object
    DSNInfo(){}
    ///copy constructor
    DSNInfo(const DSNInfo&)=default;
    ///move constructor
    DSNInfo(DSNInfo&&)=default;

    ///copy assignment
    DSNInfo& operator=(const DSNInfo&)=default;
    ///move assignment
    DSNInfo& operator=(DSNInfo&&)=default;

    ///returns true if the objects represent the same DSN
    bool operator==(const DSNInfo&)const =default;

    ///return the DSN name by which the connection can be referenced
    QString dataSourceName()const{return mname;}
    ///return the description returned by ODBC
    QString description()const{return mdescr;}
};

///Settings for a test connection.
class ODBCconnectionSettings
{
public:
    ///How to connect (emulates KiCAD behavior).
    enum class ConnectionMode {
        ///Invalid settings - no connection.
        NoConnect,
        ///Connect by DSN with optional User name and Password.
        ByDSN,
        ///Connect by connection string.
        ByConnectionString,
    };
    ///Instantiate invalid settings object.
    ODBCconnectionSettings()=default;
    ///Instantiate normal settings object.
    ///See also \ref EBase::Project::OdbcSettings::toDriverSettings
    ODBCconnectionSettings(ConnectionMode mode, QString dsnOrConnStr, QString uname=QString(), QString passwd=QString())
        :mmode(mode),
         mdsn(mode==ConnectionMode::ByDSN?dsnOrConnStr:QString()),
         musr(uname),mpass(passwd), //TODO: is this useful only for DSN mode? If yes: limit setting
         mconnstr(mode==ConnectionMode::ByConnectionString?dsnOrConnStr:QString())
        {}
    ///Copy constructor
    ODBCconnectionSettings(const ODBCconnectionSettings&)=default;

    ///Assignment operator
    ODBCconnectionSettings& operator=(const ODBCconnectionSettings&)=default;

    ///Returns true if the settings appear to be valid.
    bool isValid()const{return mmode!=ConnectionMode::NoConnect;}

    ///Returns the connection mode.
    ConnectionMode connectionMode()const{return mmode;}
    ///True if this is a connect by DSN setting.
    bool isConnectByDSN()const{return mmode==ConnectionMode::ByDSN;}
    ///True if this is a connect by string setting.
    bool isConnectByString()const{return mmode==ConnectionMode::ByConnectionString;}

    ///True if these settings contain username and/or password.
    bool hasLoginData()const{return !musr.isEmpty() || !mpass.isEmpty();}

    ///Return the DSN (for connect by DSN settings).
    QString dsn()const{return mdsn;}
    ///Return the user name or empty string.
    QString username()const{return musr;}
    ///Return the password or empty string.
    QString password()const{return mpass;}
    ///Return the connection string (for connect by string settings).
    QString connectionString()const{return mconnstr;}

private:
    ConnectionMode mmode=ConnectionMode::NoConnect;
    QString mdsn,musr,mpass,mconnstr;
};

///Result of an ODBC connection test.
class ODBCconnectionResult
{
public:
    ///copy constructor
    ODBCconnectionResult(const ODBCconnectionResult&)=default;
    ///move constructor
    ODBCconnectionResult(ODBCconnectionResult&&)=default;

    ///copy assignment
    ODBCconnectionResult& operator=(const ODBCconnectionResult&)=default;
    ///move assignment
    ODBCconnectionResult& operator=(ODBCconnectionResult&&)=default;

    ///returns the original settings that lead to this result
    ODBCconnectionSettings settings()const{return msettings;}

    ///Overall result of the connection test.
    enum class ReturnCode{
        ///Test was successful: ODBC could connect to the database.
        Successful,
        ///Test not run yet.
        NotTriedYet,
        ///ODBC environment is missing - initialization failed.
        NoEnvironment,
        ///The settings were invalid. Not attempted.
        InvalidSettings,
        ///Connection attempt failed.
        ConnectFailed,
    };

    ///True if the attempt was successful.
    bool successfullyConnected()const{return mcode==ReturnCode::Successful;}
    ///Returns the overall result of the connection attempt.
    ReturnCode returnCode()const{return mcode;}

    ///Info about a table or view in the ODBC database.
    class TableInfo{
    public:
        ///default constructor: creates an empty info object
        TableInfo()=default;
        ///copy constructor
        TableInfo(const TableInfo&)=default;

        ///assignment operator
        TableInfo& operator=(const TableInfo&)=default;

        ///returns the string used for a DB NULL value
        static QString nullValue(){return "(NULL)";}

        ///the catalog in which the table exists
        QString catalog()const{return mcat;}
        ///the schema in which the table exists, if the DB has schemas
        QString schema()const{return msch;}
        ///the name of the table or view
        QString tableName()const{return mtable;}
        ///the type of the table or view (usually "TABLE" or "VIEW")
        QString tableType()const{return mtype;}
        ///additional remarks about the table, if the DB supports that
        QString remarks()const{return mrem;}

        ///True if the catalog was returned as NULL
        bool catalogIsNull()const  {return mcat  == nullValue();}
        ///True if the schema was returned as NULL
        bool schemaIsNull()const   {return msch  == nullValue();}
        ///True if the table name was returned as NULL
        bool tableNameIsNull()const{return mtable== nullValue();}
        ///True if the table type was returned as NULL
        bool tableTypeIsNull()const{return mtype == nullValue();}
        ///True if the remarks were returned as NULL
        bool remarksIsNull()const  {return mrem  == nullValue();}
    private:
        QString mcat,msch,mtable,mtype,mrem;
        ///\internal used by parent class to instantiate table info
        TableInfo(QString c,QString s,QString ta,QString ty,QString r):mcat(c),msch(s),mtable(ta),mtype(ty),mrem(r){}
        friend class ODBCconnectionResult;
    };

    ///returns all tables that were reported by the ODBC connection
    QList<TableInfo> tables()const{return mtables;}
    ///returns all views that were reported by the ODBC connection
    QList<TableInfo> views()const{return mviews;}

    ///true if the database has tables
    bool hasTables()const{return mtables.size()>0;}
    ///true if the database has views
    bool hasViews()const{return mviews.size()>0;}

    ///Helper class to store errors during the ODBC test.
    class ErrorRecord {
    public:
        ///default constructor: empty record
        ErrorRecord()=default;
        ///copy constructor
        ErrorRecord(const ErrorRecord&)=default;

        ///copy assignment
        ErrorRecord& operator=(const ErrorRecord&)=default;

        ///ODBC state - this is a fairly cryptic 5-char code that may or may not be documented somewhere
        QString sqlStateCode()const{return mstate;}
        ///the (more or less) human readable error message
        QString errorMessage()const{return mmsg;}
        ///the native error code returned by the DB driver
        int nativeError()const{return mnaterr;}
        ///the record number in case there are multiple error records (starts at 1)
        int recordNumber()const{return mrecnr;}

        ///returns a consolidated string with all information that can be logged or displayed
        QString toString()const{return QString("[Rec=%3 State=%1 Native Error=%2] %4").arg(mstate).arg(mnaterr).arg(mrecnr).arg(mmsg);}
    private:
        QString mstate,mmsg;
        int mnaterr=0,mrecnr=0;

        ///\internal used by parent class to instantiate record
        ErrorRecord(int rnr,int ne,QString sta,QString msg):mstate(sta),mmsg(msg),mnaterr(ne),mrecnr(rnr){}
        friend class ODBCconnectionResult;
    };

    ///returns all recorded errors of the connection attempt
    QList<ErrorRecord> errorRecords()const{return merrors;}
    ///returns true if there are errors
    bool hasErrors()const{return merrors.size()>0;}
private:
    ODBCconnectionSettings msettings;
    QList<TableInfo> mtables,mviews;
    ReturnCode mcode=ReturnCode::NotTriedYet;
    QList<ErrorRecord>merrors;

    friend class ODBCInterface;

    ODBCconnectionResult()=delete;
    ///\internal used by ODBCInterface to instantiate a result object, executes the test
    ODBCconnectionResult(const ODBCconnectionSettings&s);
    ///\internal helper to get errors from ODBC
    void getErrorRecords(void*,int);
    ///\internal helper to list tables/views
    void getTables(void*dbc,QString ttype,QList<TableInfo>&list);
};

///ODBC API Interface Class (singleton).
///There is exactly one instance providing the interface.
///All queries into ODBC start at that instance.
class ODBCInterface
{
    ODBCInterface();
    ~ODBCInterface();

    ODBCInterface(const ODBCInterface&)=delete;
    ODBCInterface(ODBCInterface&&)=delete;
public:
    ///returns a reference to the singleton instance.
    static ODBCInterface& instance();

    ///returns true if ODBC has been initialized and can be used
    bool odbcAvailable()const;

    ///returns the names of all installed ODBC drivers
    QStringList driverNames()const;
    ///returns info for a specific installed ODBC driver
    DriverInfo driverInfo(QString)const;
    ///returns info for all installed ODBC drivers
    QList<DriverInfo> driverInfos()const;

    ///DSN types that can be queried.
    enum class DSNclass {
        ///Query all DSNs (system and user)
        All,
        ///Query only System DSNs
        System,
        ///Query only user DSNs
        User
    };
    ///Returns all Data Source Names
    ///\param c select whether to return system or user DSNs (per default: both)
    QStringList dsNames(DSNclass c=DSNclass::All)const;
    ///Returns info about a specific DSN
    DSNInfo dsnInfo(QString)const;
    ///Returns info about all DSNs.
    ///\param c select whether to return system or user DSNs (per default: both)
    QList<DSNInfo> dsnInfos(DSNclass c=DSNclass::All)const;

    ///Install a user DSN.
    bool installUserDSN(QString dsn,QString driver,const QMap<QString,QString>&parameters);

    ///Returns the version of ODBC library this app has been linked to.
    static QString odbcVersion();

    ///Attempts to test a connection.
    ODBCconnectionResult testConnection(const ODBCconnectionSettings&);

    ///Returns the currently set login timeout for the \ref testConnection method.
    static int connectionLoginTimeout();
    ///Sets the login timeout for the \ref testConnection method. A value of <=0 will reset it to ODBC default.
    static void setConnectionLoginTimeout(int);
};


//END of namespaces
}}
